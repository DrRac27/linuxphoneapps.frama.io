+++
title = "Apps to be added"
description = "Apps that need to be evaluated and added."
date = 2023-01-08T07:00:00+00:00
draft = false

[extra]
lead = 'Apps that need to be evaluated.'
images = []
+++

## Purpose

This list contains apps that have not been added as a proper listing. This list can be useful if

- the app list does not contain what you need,
- you want to help adding apps (see [Contributing](@/docs/contributing/_index.md) for how to do this),
- or you want to see if someone has already started developing an app in order to join an existing project.

### Ready to be added (holdup) 
#### GTK3
* GNOME Terminal (fine except for settings, but .., add after GTK4 rewrite)

### Needs help testing
* keyring https://git.sr.ht/~martijnbraam/keyring (needs Himitsu user https://himitsustore.org/ / packaging of himitsu/hare) 
* rokugtk https://github.com/cliftonts/rokugtk (a Roku remote app, requires a Roku device to test)
* AugVK-Messenger https://github.com/Augmeneco/AugVK-Messenger (requires a VKontakte account)
* AugSteam https://github.com/Augmeneco/AugSteam (if you use Steam's mobile authenticator app, please help out!)
* Kazv https://lily.kazv.moe/kazv (can't even successfully build libkazv)
* Libre Invoice https://codeberg.org/matiaslavik/LibreInvoice Looks like LimeReport might need to be rebuild, but since I know nothing about Qt Android apps, that might not be all.
* Open Mandriva Camera app https://github.com/OpenMandrivaSoftware/om-camera (can't get this to work on other distributions)
* GNOME Network Displays https://gitlab.gnome.org/GNOME/gnome-network-displays (Flathub: https://flathub.org/apps/details/org.gnome.NetworkDisplays; AUR: gnome-network-displays(-git), requires Miracast Display to test

### Needs testing

#### Kirigami
* Clip2DeepL https://codeberg.org/yasht/clip2deepl (works when started on terminal on Phosh, needs testing on Plasma Mobile, last commit 2021-03-02)
* Papper https://gitlab.com/GunnarGrop/papper (reddit client, last commit 2020-07-01)
* Francis https://invent.kde.org/utilities/francis 

#### QtWidget
* opensnitch https://github.com/evilsocket/opensnitch/discussions/415
* Qt-Audiobook-Player https://github.com/rushic24/Qt-Audiobook-Player/

#### QtQuick
* Grep Tool QML https://github.com/presire/GrepToolQML
* Barcelona Trees https://github.com/pedrolcl/barcelona-trees
* Toxer Desktop https://gitlab.com/Toxer/toxer-desktop
* Orion https://github.com/drac69/orion twitch client, 4 (last commit 2022-11-10)
* Telephant https://github.com/muesli/telephant mastodon client
* Qomp https://github.com/qomp/qomp music player
* Vrenemar https://github.com/ntadej/Vremenar weather app

#### GTK3 
* Desktop Files Creator (https://github.com/alexkdeveloper/desktop-files-creator)
* Abbadon (Discord client) https://github.com/uowuo/abaddon
* PineCast https://github.com/Jeremiah-Roise/PineCast
* xournalpp https://github.com/xournalpp/xournalpp
* keylight-control https://github.com/mschneider82/keylight-control builds, but quits on restart
* Coffee https://github.com/nick92/coffee (Settings don't work well (janky), rest is okay)
* umtp-responder-gui https://github.com/JollyDevelopment/umtp-responder-gui
* Astroid https://github.com/astroidmail/astroid/
* Tunes (MPD Client) https://git.sr.ht/~jakob/tunes
* Argos https://github.com/orontee/argos (likely ironically not mobile frienly)
* Camera https://gitlab.gnome.org/jwestman/camera (archived in favor of Snapshot (see below) 

##### GTK3 + Granite
* Agenda https://github.com/dahenson/agenda  
* Bitguarden https://github.com/DenisPalchuk/bitguarden (last commit 2020-04-29)
* Gifup https://github.com/BharatKalluri/Gifup (last commit 2020-03-03)

#### GTK4
* Lorem	https://gitlab.gnome.org/World/design/lorem (at best a 4, and.. do we need Lorem Ipsum on a Phone?)
* Dot Matrix (https://github.com/lainsce/dot-matrix/, needs scale-to-fit io.github.lainsce.DotMatrix on)
* Kooha https://github.com/SeaDve/Kooha
* Share Preview https://github.com/rafaelmardojai/share-preview
* Webfont Kit Generator https://github.com/rafaelmardojai/webfont-kit-generator
* Icon Library https://gitlab.gnome.org/World/design/icon-library
* Mirdorph (Discord client) https://gitlab.gnome.org/ranchester/mirdorph - last commit 2021-09-05
* Tracks https://codeberg.org/som/Tracks
* SharMaVid (Invidious client) https://gitlab.gnome.org/ranfdev/sharmavid
* Summarizer https://github.com/Nalsai/summarizer
* Pala https://gitlab.com/gabmus/pala
* Ghostshot https://gitlab.gnome.org/GabMus/ghostshot
* MD Writer https://gitlab.com/gabmus/mdwriter
* Shards (Torrent client) https://github.com/TheCactusVert/shards
* Noteworthy https://github.com/SeaDve/Noteworthy (builds now, not yet tested on mobile)
* Pods https://github.com/marhkb/pods (Podman client)
* Citations https://gitlab.gnome.org/World/citations
* unlockR https://github.com/jkotra/unlockr
* SNCF https://sr.ht/~yukikoo/sncf/
* Snapshot https://gitlab.gnome.org/Incubator/snapshot upcoming GNOME camera app


#### Flutter
* AppImage Pool (https://github.com/prateekmedia/appimagepool)
* Nextcloud Password Client https://gitlab.com/j0chn/nextcloud_password_client, once https://github.com/flathub/com.gitlab.j0chn.nextcloud_password_client/issues/1 is fixed.
* Spotube https://github.com/KRTirtho/spotube/
* AuthPass (Keepass client) https://github.com/authpass/authpass (Did not work in linmob's attempt to build it (white window after startup), supposedly there's an ARM64 snap build, but it returns an architecture warning.)
* Kaiteki (mastodon client) https://github.com/Kaiteki-Fedi/Kaiteki
* LocalSend https://localsend.org/ (did not build for me on Arch Linux ARM), related issue https://github.com/localsend/localsend/issues/101
* More here: https://github.com/leanflutter/awesome-flutter-desktop#open-source-apps

#### Fyne
* itd-gui https://gitea.arsenm.dev/Arsen6331/itd#itgui

#### Ubuntu Components
* Cooking calc https://codeberg.org/WammKD/Cooking-Calc

### Downstream forks
#### GTK3
* Evince (Purism)

### WIP (needs more work, early and pre-release) and needs testing

#### GTK3
* Camcam https://github.com/JNissi/camcam WIP Camera app written in Rust (last commit 2021-10-16)
* Fastcal https://github.com/JNissi/fastcal WIP Calendar app written in Rust (last commit 2021-03-21)
* hn https://github.com/DavidVentura/hn Python hacker news client (last commit 2021-04-12)
* Hydro Bot https://github.com/ripxorip/hydro_bot ("Pinephone app that keeps you hydrated", too much WIP to add on 2022-04-12)
* wifiscanner https://github.com/Cyborgscode/wifiscanner (last commit 2020-12-31)
* GTK Hangouts Client https://github.com/do-sch/hangouts-gtk
* pKPR https://github.com/juliendecharentenay/pKPR
* Pygenda https://github.com/semiprime/pygenda

#### GTK4
* Replay https://github.com/ReplayDev/Replay (YouTube client)
* Sharit https://gitlab.com/LyesSaadi/sharit (YouTube client)
* Katana https://github.com/gavr123456789/Katana (GTK Tree Structure Browser)
* Chert https://gitlab.com/high-creek-software/chert (Note taking app)
* covidpass https://github.com/pentamassiv/covidpass
* Bazar https://codeberg.org/fsf.poland/bazar (Vala SMS/MMS client)

#### Kirigami
* PowerPlant https://invent.kde.org/mbruchert/powerplant
* Nextcloud Talk https://github.com/CarlSchwan/talk-desktop  (Kirigami-fication of a SailfishOS app) https://codeberg.org/blizzz/harbour-nextcloud-talk/pulls/37, see also: https://carlschwan.eu/2021/12/18/more-kde-apps/
* Pelikan https://invent.kde.org/carlschwan/quickmail (now called "Pelikan", see https://carlschwan.eu/2021/12/18/more-kde-apps/)
* mailmodel https://invent.kde.org/vandenoever/mailmodel (last commit 2021-04-04)
* Solio https://invent.kde.org/devinlin/solio Soundcloud client (last commit 2021-05-21)
* Booth https://invent.kde.org/maui/booth (convergent camera app)
* Comlink https://codeberg.org/xaviers/Comlink
* LurkyMcLurkFace https://gitlab.com/armen138/lurkymclurkface (reddit client, last commit 2020-12-16)

#### QtQuick
* Project Trackball https://github.com/NthElse/trackball (Todo.txt desktop apps, scales fine on 20210930, but adding tasks does not seem to work yet, last commit 20211005)

#### Cairo etc
* Visurf https://sr.ht/~sircmpwn/visurf/

### Needs initial testing 
* Personal Voice Assistant https://github.com/Cyborgscode/Personal-Voice-Assistent (Currently German only, requires Java. See also https://marius.bloggt-in-braunschweig.de/2021/07/15/hallo-computer-bist-du-da/)
* Pine Connect https://github.com/timaios/PineConnect (early state of development, work on the app has not started yet (but the daemon is being worked on)) - last commit 2021-07-27 

## From other applists
* https://cubocore.org/ apps, see also https://wiki.postmarketos.org/wiki/CoreApps


### old Linux Phone apps:
* Navit
* FoxtrotGPS
* Maep

## unordered stuff copied over from another to do list

- https://github.com/grelltrier/keyboard Fingerboard


### unoptimized desktop apps that have been run on the Librem 5 or PinePhone successfully (only add if they are at least somewhat usable and have an advantage compared to more mobile-ready apps):
* Gnome Photos
* Gthumb
* Shotwell // apps from here on where spotted on a screenshot
* Claws Mail
* D-Feet
* Gpredict
* KiCad
* Shadowsocks-Qt5
* Lyrebird https://github.com/constcharptr/lyrebird
* https://github.com/linuxmint/webapp-manager
* Verbiste https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/7
* Simple scan https://gitlab.gnome.org/GNOME/simple-scan (newer versions are only fine after "scale-to-fit simple-scan on")
* Marble (https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/27)
* Orage (https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/28)
* Solar System (https://flathub.org/apps/details/org.sugarlabs.SolarSystem; https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/37)
* AusweisApp 2 (https://flathub.org/apps/details/de.bund.ausweisapp.ausweisapp2; https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/51)
* Nixwriter https://flathub.org/apps/details/com.gitlab.adnan338.Nixwriter; https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/69
* SongRec https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/91
* Bleachbit https://forums.puri.sm/t/list-of-apps-that-fit-and-function-well-post-them-here/11361/101
* Argos Translate https://github.com/argosopentech/argos-translate

### Needs help testing (low priority, inactive)
#### GTK3
* Moody (Moodle client, GTK3/libhandy) https://gitlab.gnugen.ch/afontain/moodle (no commit in two years)
* Venom https://github.com/naxuroqa/venom (A modern Tox client for the Linux desktop, not mobile friendly, last commit 2020-04-23)

### Projects that seem to have been abandoned or are archived
* Wallet https://gitlab.gnome.org/bilelmoussaoui/wallet
* Kodimore https://github.com/scandinave/kodimote (WIP, No commits in 16 months, but..)
* l5_shoppinglist https://source.puri.sm/fphemeral/l5_shoppinglist,https://puri.sm/posts/an-interview-with-fphemeral-librem-5-early-adopter
* librem5_utils https://source.puri.sm/fphemeral/librem5_utils,https://puri.sm/posts/an-interview-with-fphemeral-librem-5-early-adopthttps://stackoverflow.com/questions/1260748/how-do-i-remove-a-submodule/36593218#36593218er/
* Syncthing GTK https://github.com/kozec/syncthing-gtk (last release 2019, python 2)
* Flashcards https://github.com/SeaDve/Flashcards (only UI, non-functional, archived)
* PinePhone modem firmware updater UI: https://git.sr.ht/~martijnbraam/pinephone-modemfw (solved with GNOME Firmware, still listed for historic purposes)
* camera-rs https://gitlab.gnome.org/bilelmoussaoui/camera-rs (last commit 2020-12-25)
* qrcode-generator https://gitlab.gnome.org/bilelmoussaoui/paintable-rs (last commit 2020-12-23)
* Interpret https://codeberg.org/xaviers/Interpret (GTK3 deepl based translation app, archived)
* Tox Client (Electron) https://github.com/TheToxProject/client
* Simple RSS Reader using KDE Kirigami https://gitlab.com/SiriBeta/simple-rss-reader-using-kde-kirigami (last commit 2020-06-13)

### Projects that seem to be gone
* Musik https://invent.kde.org/openwebdesigngermany/musik 404
* clan https://web.archive.org/web/20201201114016/https://github.com/mauikit/clan (Seems to have been a launcher)

### Not an app
* Retiled (Windows Phone style launcher) https://github.com/DrewNaylor/Retiled (are launchers in scope?)
* PUI https://github.com/nbdy/pui (launcher?)

### No GUI
* Mactrack https://gitlab.com/Aresesi/mactrack 


## Remains of other-apps.csv
Check, then sort or remove the following:

### This is a loose and non-formal collection of apps which are not added to the main list yet, because they are not in a usable state, are still in planning stage or wait to be transfered to the other list"
* MedOS-kirigami,https://github.com/milohr/medOS-kirigami
* Liri Screencast,https://github.com/lirios/screencast
* Liri Screenshot,https://github.com/lirios/screenshot
* Liri music,https://github.com/lirios/music
* Liri Settings,https://github.com/lirios/settings
* Liri NetworkManager,https://github.com/lirios/networkmanager
* OwnCloud Sync,https://open-store.io/app/owncloud-sync
* Music,https://wiki.gnome.org/Apps/Music,https://tchncs.de/_matrix/media/v1/download/talk.puri.sm/wDbVJsNtmaLUljbuxVVzRRhf
* GadgetBridge https://github.com/Freeyourgadget/Gadgetbridge (afaik Android only, so: why?)
* Linphone (not mobile compatible, Ubuntu Touch version does not exist for 64bit yet)
* https://doc.qt.io/qt-5/qtquick-codesamples.html

### In planning stage:
* Compass https://phabricator.kde.org/T8905
* Konversation 2.0

#### Apps where the sources are not shared yet:
* Translator by Avi Wadhwa in community/librem-5-apps announced

### Sources for apps to be included:
* https://binary-factory.kde.org/view/Android/,28 apps (02.03.2020)
* https://www.youtube.com/channel/UCIVaIdjtr6aNPdTm-JNbFAg/videos
* https://www.youtube.com/watch?v=K4OfNFis--g
* https://puri.sm/posts/what-is-mobile-pureos/
* https://source.puri.sm/Librem5/community-wiki/-/wikis/List-of-Apps-in-Development
* https://nxos.org/maui/maui-apps-1-2-1-released-and-more/ (check whether everything is already on the list)
