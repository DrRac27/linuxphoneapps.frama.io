+++
title = "MauiKit"
description = "The MauiKit UI Framework."
date = 2021-08-15T08:50:45+00:00
updated = 2021-08-15T08:50:45+00:00
draft = false
+++


MauiKit is a "a free and modular front-end framework for developing user experiences" based on [Kirigami](@/frameworks/kirigami.md).

* [MauiKit.org](https://mauikit.org/)

