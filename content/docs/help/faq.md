+++
title = "FAQ"
description = "Answers to frequently asked questions."
date = 2021-08-14T18:30:00+02:00
updated = 2022-04-07T19:30:00+02:00
draft = false
weight = 30
sort_by = "weight"
template = "docs/page.html"

[extra]
lead = "Answers to frequently asked questions."
toc = true
top = false
+++

## What is LinuxPhoneApps?

LinuxPhoneApps.org is the continuation of [LINMOBapps](https://framagit.org/linmobapps/linmobapps.frama.io) (and thus also [MGLapps](https://mglapps.frama.io)). It lists and documents apps for Linux Phones like the PinePhone or the Librem 5 that don't have a centralized app store (yet) and run distributions such as Arch Linux ARM, Fedora Mobility, Mobian, openSUSE, postmarketOS, PureOS or similar which use Phosh, Plasma Mobile or Sxmo for their UI.

If you don't you have questions about the contents of our app listings or have suggestions for improvements (especially regarding visuals and wording), please [get in touch](#join-the-discussion-and-stay-informed) so that we can make things better and easier to grasp.

### Can I install these apps on Ubuntu Touch?

Some, maybe. Generally, Ubuntu Touch is not in scope for this project, as it has an [app store](https://open-store.io/). 

### Can I install these apps on Sailfish OS?

Some, maybe. Generally, Sailfish OS is not in the scope of this project as it has more than one app store already:
* the official Jolla App Store (no website),
* [OpenRepos.net](https://openrepos.net/) and
* [SailfishOS:Chum community repository](https://build.sailfishos.org/project/show/sailfishos:chum).

## Join the effort

The various parts of this project live under the [LinuxPhoneApps Organization on Framagit](https://framagit.org/linuxphoneapps/).

### Repositories

Currently, these are: 
* the repository for the [main page, including docs, apps and games](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io),
* the old LINMOBapps repo [where our csv files of LINMOBapps are still available, but no longer archive](https://framagit.org/linmobapps/linmobapps.frama.io),
* a seperate repo for the [Python code that can turn the individual markdown files into csv or bulk editing and vice versa](https://framagit.org/linuxphoneapps/appscsv2tomlmd).

### Bugtrackers
If you want to work on known issues, you'll find
* [all content (documentation, landing page, feeds) and all Zola related website design (listing and app overview design) issues in the LinuxPhoneApps repo](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues),
* [markdown -> csv -> markdown converter centric issues (Python) issues in its repo](https://framagit.org/linuxphoneapps/appscsv2tomlmd/-/issues).

Creating new issues is a very valuable contribution too!


### Adding or editing app listings

If you want to add an app, you can do so by forking the LINMOBapps repo and creating a merge request or sending an email. The following pages have more info on this:
* [Submit an app by email](@/docs/contributing/submit-app-by-email.md),
* [Submit an app on GitLab](@/docs/contributing/submit-app-on-gitlab.md),

In order to understand the TOML-frontmatter of our Markdown files, check out [Listings explained](@/docs/contributing/listings-explained.md).

If just want to report that something is wrong with a listing, click the link at the bottom of the apps page to send an email, or better yet, edit the listing on framagit by clicking the edit link (also at the bottom at the page).

### Reuse the data

If you respect the license and credit the project properly, feel free to reuse apps.csv data! Up to this point, the following projects are known to reuse .csv data:

* [LINMOBappsBrowser](https://github.com/Speiburger/LINMOBappsBrowser)
* [Simple Applist](https://framagit.org/1peter10/simple-applist)

If you have a project that reuses the data, please make sure to list your project here, too!

<mark>Since April 14th, 2023, the .csv list is no longer the source of truth. You can, however, still generate a .csv listing using [tomlmd2csv.py](https://framagit.org/linuxphoneapps/appscsv2tomlmd/-/blob/main/tomlmd2csv.py).</mark>

The above projects will need adjustments to work with such a generated csv files, as the structure of the .csv has changed a lot since these projects were last updated.


## Join the discussion and stay informed

Join our [Matrix room](https://matrix.to/#/#linuxphoneapps:matrix.org)!

Or, subscribe to or join the discussion one of our mailing lists:
* [Discuss](https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-discuss) list for the discussion of apps and general topics,
* [Devel](https://lists.sr.ht/~linuxphoneapps/linuxphoneapps.org-devel) list for discussion regarding further development of the LinuxPhoneApps app list.

You can also subscribe to a number of [Atom](https://en.wikipedia.org/wiki/Atom_%28Web_standard%29) feeds to follow along:
* [Apps feed](https://linuxphoneapps.org/apps/atom.xml) to learn about newly added apps,
* [Games feed](https://linuxphoneapps.org/games/atom.xml) to learn about newly added games,
* [Blog feed](https://linuxphoneapps.org/blog/atom.xml) to learn about new blog posts.


## Keyboard shortcuts for search? 

<mark>Focus and Close are currently not working (removed too much JavaScript ;-)), will be restored</mark>
- focus: `/`
- select: `↓` and `↑`
- open: `Enter`
- close: `Esc`

