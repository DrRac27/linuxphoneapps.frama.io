+++
title = "Rabbit Escape"
description = "Android and PC game inspired by Lemmings and Pingus"
aliases = []
date = 2019-02-01

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = []

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://github.com/andybalaam/rabbit-escape"
homepage = "https://artificialworlds.net/rabbit-escape/"
more_information = []
summary_source_url = "https://github.com/andybalaam/rabbit-escape"
screenshots = [ "artificialworlds.net/rabbit-escape",]
screenshots_img = []
app_id = "net.artificialworlds.rabbitescape"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "rabbit-escape",]
appstream_xml_url = ""
+++

