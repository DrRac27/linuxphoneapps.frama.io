+++
title = "Zandronum"
description = "Multiplayer oriented port, based off Skulltag, for Doom and Doom II by id Software."
aliases = []
date = 2021-03-14

[taxonomies]
project_licenses = [ "Sleepycat AND BSD-3-Clause AND",]
categories = [ "game",]
mobile_compatibility = [ "1",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_22_11", "nix_unstable",]

[extra]
reported_by = "-Euso-"
verified = "❎"
repository = "https://osdn.net/projects/zandronum/"
homepage = "https://zandronum.com/"
more_information = []
summary_source_url = "https://osdn.net/projects/zandronum/scm/hg/zandronum-stable/"
screenshots = []
screenshots_img = []
app_id = "com.zandronum.Zandronum"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.zandronum.Zandronum"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "zandronum",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/com.zandronum.Zandronum/master/com.zandronum.Zandronum.appdata.xml"
+++


