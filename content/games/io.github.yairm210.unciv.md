+++
title = "Unciv"
description = "An open-source reimplementation of the most famous civilization-building game ever."
aliases = []
date = 2021-03-17

[taxonomies]
project_licenses = [ "MPL-2.0",]
categories = [ "game",]
mobile_compatibility = [ "4",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_22_11", "nix_unstable",]

[extra]
reported_by = "Nob0dy73"
verified = "❎"
repository = "https://github.com/yairm210/UnCiv"
homepage = "https://yairm210.itch.io/unciv"
more_information = []
summary_source_url = "no quotation"
screenshots = []
screenshots_img = []
app_id = "io.github.yairm210.unciv"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.yairm210.unciv"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "unciv",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/io.github.yairm210.unciv/master/io.github.yairm210.unciv.appdata.xml"
+++





### Notice

The resolution is pretty messed up with the text bordering on unreadable, but otherwise running fine
