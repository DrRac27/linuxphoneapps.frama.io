+++
title = "GNOME Sudoku"
description = "Test your logic skills in this number grid puzzle"
aliases = []
date = 2023-02-07

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "5",]
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]

[extra]
reported_by = "linmob"
verified = "✅"
repository = "https://gitlab.gnome.org/GNOME/gnome-sudoku"
homepage = "https://gitlab.gnome.org/GNOME/gnome-sudoku"
more_information = []
summary_source_url = "https://flathub.org/apps/org.gnome.Sudoku"
screenshots = []
screenshots_img = []
app_id = "org.gnome.Sudoku"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Sudoku"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-sudoku",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-sudoku/-/raw/master/data/org.gnome.Sudoku.appdata.xml.in"
+++


