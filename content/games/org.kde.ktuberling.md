+++
title = "KTuberling"
description = "KTuberling is a simple constructor game suitable for children and adults alike"
aliases = []
date = 2019-02-01

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
frameworks = [ "QtWidgets",]
backends = []
services = []
packaged_in = [ "archlinuxarm_aarch64", "archlinuxarm_armv7h", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "opensuse_tumbleweed", "pureos_landing",]

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://invent.kde.org/games/ktuberling"
homepage = ""
more_information = []
summary_source_url = "https://invent.kde.org/games/ktuberling"
screenshots = [ "https://play.google.com/store/apps/details?id=org.kde.ktuberling",]
screenshots_img = []
app_id = "org.kde.ktuberling"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.ktuberling"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "ktuberling",]
appstream_xml_url = "https://invent.kde.org/games/ktuberling/-/raw/master/org.kde.ktuberling.appdata.xml"
+++


