+++
title = "Magarena"
description = "Single-player fantasy card game played against a computer opponent."
aliases = []
date = 2021-03-14

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
categories = [ "game",]
mobile_compatibility = [ "1",]
frameworks = []
backends = []
services = []
packaged_in = [ "aur",]

[extra]
reported_by = "-Euso-"
verified = "❎"
repository = "https://github.com/magarena/magarena"
homepage = "https://magarena.github.io/"
more_information = []
summary_source_url = "https://github.com/magarena/magarena"
screenshots = []
screenshots_img = []
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "magarena",]
appstream_xml_url = ""
+++

