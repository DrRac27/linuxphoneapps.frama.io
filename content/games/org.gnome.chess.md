+++
title = "GNOME Chess"
description = "Play the classic two-player board game of chess"
aliases = []
date = 2021-03-14

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "5",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]

[extra]
reported_by = "Moxvallix"
verified = "✅"
repository = "https://gitlab.gnome.org/GNOME/gnome-chess"
homepage = "https://wiki.gnome.org/Apps/Chess"
more_information = []
summary_source_url = "https://flathub.org/apps/org.gnome.Chess"
screenshots = []
screenshots_img = []
app_id = "org.gnome.Chess"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Chess"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-chess",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-chess/-/raw/master/data/org.gnome.Chess.appdata.xml.in"
+++


