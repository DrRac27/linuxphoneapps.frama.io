+++
title = "SuperTux"
description = "SuperTux is a jump'n'run game"
aliases = []
date = 2019-02-01

[taxonomies]
project_licenses = [ "GPL-3.0-or-later AND CC-BY-SA-2.0",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "snapcraft",]

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://github.com/SuperTux/supertux"
homepage = "https://www.supertux.org/"
more_information = []
summary_source_url = "https://github.com/SuperTux/supertux"
screenshots = [ "https://www.supertux.org/screenshots.html",]
screenshots_img = []
app_id = "org.supertuxproject.SuperTux"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.supertuxproject.SuperTux"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = "https://snapcraft.io/supertux"
snap_link = ""
snap_recipe = ""
repology = [ "supertux",]
appstream_xml_url = "https://raw.githubusercontent.com/SuperTux/supertux/master/supertux2.appdata.xml"
+++




### Description

SuperTux is a jump'n'run game with strong inspiration from the Super Mario Bros. games for the various Nintendo platforms.  Run and jump through multiple worlds, fighting off enemies by jumping on them, bumping them from below or tossing objects at them, grabbing power-ups and other stuff on the way. [Source](https://github.com/SuperTux/supertux)
