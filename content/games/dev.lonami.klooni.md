+++
title = "1010! Klooni"
description = "libGDX game based on the original 1010!"
aliases = []
date = 2019-02-01

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "5",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = [ "aur",]

[extra]
reported_by = "cahfofpai"
verified = "✅"
repository = "https://github.com/moxvallix/LinMobGames"
homepage = "https://lonami.dev/klooni/"
more_information = []
summary_source_url = "https://github.com/LonamiWebs/Klooni1010"
screenshots = [ "https://f-droid.org/en/packages/dev.lonami.klooni/",]
screenshots_img = []
app_id = "dev.lonami.klooni"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "klooni1010",]
appstream_xml_url = ""
+++





### Notice

Patched to run by Moxvallix.
