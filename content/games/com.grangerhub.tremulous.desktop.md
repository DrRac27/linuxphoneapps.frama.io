+++
title = "Tremulous"
description = "Asymmetric team-based first-person shooter with real-time strategy elements."
aliases = []
date = 2021-03-14

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
categories = [ "game",]
mobile_compatibility = [ "1",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "aur", "fedora_38", "fedora_rawhide", "flathub",]

[extra]
reported_by = "-Euso-"
verified = "❎"
repository = "https://github.com/darklegion/tremulous"
homepage = "https://tremulous.net/"
more_information = []
summary_source_url = "https://tremulous.net/"
screenshots = []
screenshots_img = []
app_id = "com.grangerhub.Tremulous.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.grangerhub.Tremulous"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "tremulous",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/com.grangerhub.Tremulous/master/com.grangerhub.Tremulous.appdata.xml"
+++


