+++
title = "Parolottero"
description = "Word game similar to Boggle and Ruzzle"
aliases = []
date = 2023-01-08

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
categories = [ "game",]
mobile_compatibility = [ "5",]
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = [ "debian_12", "debian_unstable",]

[extra]
reported_by = "linmob"
verified = "✅"
repository = "https://github.com/ltworf/parolottero"
homepage = ""
more_information = [ "https://www.youtube.com/watch?v=NEwD4Rn_nPQ",]
summary_source_url = "https://github.com/ltworf/parolottero"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/noappid.ltworf.parolottero/1.png", "https://img.linuxphoneapps.org/noappid.ltworf.parolottero/2.png", "https://img.linuxphoneapps.org/noappid.ltworf.parolottero/3.png", "https://img.linuxphoneapps.org/noappid.ltworf.parolottero/4.png",]
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "parolottero",]
appstream_xml_url = ""
+++





### Notice

Language files are at https://github.com/ltworf/parolottero-languages