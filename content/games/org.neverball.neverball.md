+++
title = "Neverball"
description = "Tilt the floor to roll a ball through an obstacle course within the given time. If the ball falls or time expires, a ball is lost."
aliases = []
date = 2019-02-01

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "nix_stable_22_11", "nix_unstable", "pureos_landing",]

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://github.com/pseuudonym404/neverball-touch"
homepage = ""
more_information = []
summary_source_url = "https://github.com/pseuudonym404/neverball-touch"
screenshots = []
screenshots_img = []
app_id = "org.neverball.Neverball"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.neverball.Neverball"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "neverball",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/org.neverball.Neverball/master/org.neverball.Neverball.appdata.xml"
+++


