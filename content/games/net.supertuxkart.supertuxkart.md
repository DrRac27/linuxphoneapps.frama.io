+++
title = "SuperTuxKart"
description = "SuperTuxKart is a free kart racing game. It focuses on fun and not on realistic kart physics."
aliases = []
date = 2019-02-01

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing", "snapcraft",]

[extra]
reported_by = "cahfofpai"
verified = "✅"
repository = "https://github.com/supertuxkart/stk-code"
homepage = "https://supertuxkart.net/Main_Page"
more_information = []
summary_source_url = "https://github.com/supertuxkart/stk-code"
screenshots = [ "https://play.google.com/store/apps/details?id=org.supertuxkart.stk",]
screenshots_img = []
app_id = "net.supertuxkart.SuperTuxKart"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.supertuxkart.SuperTuxKart"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = "https://snapcraft.io/supertuxkart"
snap_link = ""
snap_recipe = ""
repology = [ "supertuxkart",]
appstream_xml_url = "https://raw.githubusercontent.com/supertuxkart/stk-code/master/data/supertuxkart.appdata.xml"
+++


