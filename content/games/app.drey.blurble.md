+++
title = "Blurble"
description = "Word guessing game"
aliases = []
date = 2022-07-16

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "5",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]

[extra]
reported_by = "linmob"
verified = "✅"
repository = "https://gitlab.gnome.org/World/Blurble"
homepage = "https://gitlab.gnome.org/World/Blurble"
more_information = []
summary_source_url = "https://gitlab.gnome.org/World/Blurble"
screenshots = []
screenshots_img = []
app_id = "app.drey.Blurble"
scale_to_fit = ""
flathub = "https://flathub.org/apps/app.drey.Blurble"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "blurble",]
appstream_xml_url = "https://gitlab.gnome.org/World/Blurble/-/raw/main/data/blurble.appdata.xml.in.in"
+++




### Description

Solve the riddle until you run out of guesses! The game is a clone of Wordle and made with localization in mind. [Source](https://gitlab.gnome.org/pervoj/Blurble)
