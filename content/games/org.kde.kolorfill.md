+++
title = "Kolorfill"
description = "It is a simple flood filling game written using the amazing Kirigami framework."
aliases = []
date = 2019-02-01

[taxonomies]
project_licenses = [ "MIT/X11",]
categories = [ "game",]
mobile_compatibility = [ "5",]
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = []

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://invent.kde.org/games/kolorfill"
homepage = ""
more_information = [ "https://pusling.com/blog/?p=505",]
summary_source_url = "https://pusling.com/blog/?p=555"
screenshots = [ "https://i.imgur.com/iUCpI9I.png",]
screenshots_img = []
app_id = "org.kde.kolorfill"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "kolorfill",]
appstream_xml_url = ""
+++

