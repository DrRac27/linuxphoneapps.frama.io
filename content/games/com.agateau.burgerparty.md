+++
title = "BurgerParty"
description = "A time management game for Android where you play a fast-food owner who must put together the burgers ordered by her customers before time runs out."
aliases = []
date = 2019-02-01

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
frameworks = [ "libGDX",]
backends = []
services = []
packaged_in = []

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://github.com/agateau/burgerparty"
homepage = "https://agateau.com/projects/burgerparty/"
more_information = []
summary_source_url = "https://github.com/agateau/burgerparty"
screenshots = [ "https://agateau.com/projects/burgerparty/",]
screenshots_img = []
app_id = "com.agateau.burgerparty"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""

+++
