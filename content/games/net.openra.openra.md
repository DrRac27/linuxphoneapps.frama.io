+++
title = "OpenRA"
description = "OpenRA is a project that recreates and modernizes the classic Command & Conquer real time strategy games."
aliases = []
date = 2021-03-14

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "1",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable",]

[extra]
reported_by = "-Euso-"
verified = "❎"
repository = "https://github.com/OpenRA/OpenRA"
homepage = "https://www.openra.net/"
more_information = []
summary_source_url = ""
screenshots = []
screenshots_img = []
app_id = "net.openra.OpenRA"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.openra.OpenRA"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "openra",]
appstream_xml_url = "https://raw.githubusercontent.com/OpenRA/OpenRA/bleed/packaging/linux/openra.metainfo.xml.in"
+++


