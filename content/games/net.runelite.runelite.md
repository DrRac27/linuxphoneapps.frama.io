+++
title = "RuneLite"
description = "RuneLite is a free, open source OldSchool RuneScape client."
aliases = []
date = 2021-01-26

[taxonomies]
project_licenses = [ "BSD 2-Clause License",]
categories = [ "game",]
mobile_compatibility = [ "1",]
frameworks = []
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_22_11", "nix_unstable",]

[extra]
reported_by = "immychan"
verified = "❎"
repository = "https://github.com/runelite/runelite"
homepage = "https://runelite.net/"
more_information = []
summary_source_url = "https://github.com/runelite/runelite"
screenshots = []
screenshots_img = []
app_id = "net.runelite.RuneLite"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.runelite.RuneLite"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "runelite",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/net.runelite.RuneLite/master/net.runelite.RuneLite.appdata.xml"
+++




### Description

RuneLite is a free, open source OldSchool RuneScape client. [Source](https://github.com/runelite/runelite)
