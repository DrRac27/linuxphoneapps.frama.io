+++
title = "TwoFun"
description = "Simple touch based game for two player"
aliases = []
date = 2019-02-01

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
categories = [ "game",]
mobile_compatibility = [ "5",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "Flathub",]

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://gitlab.com/tabos/twofun"
homepage = "https://tabos.gitlab.io/projects/twofun/"
more_information = []
summary_source_url = "https://tabos.gitlab.io/projects/twofun/"
screenshots = [ "https://tabos.gitlab.io/projects/twofun/images/twofun1_hu872057eecb6739c1f15a2b0ba491a7a8_14711_360x0_resize_box_3.png https://tabos.gitlab.io/projects/twofun/images/twofun2_hu8803da43a856921af908977f9315421d_12991_360x0_resize_box_3.png https://tabos.gitlab.io/projects/twofun/images/twofun3_huab24be4c92908d74db0159a87bbb815c_19028_360x0_resize_box_3.png  https://tabos.gitlab.io/projects/twofun/images/twofun4_huc90de91a26c438b218d6cdb8f99b621d_27954_360x0_resize_box_3.png https://tabos.gitlab.io/projects/twofun/images/twofun5_hu7e98226585a740c5effb9922f554ed98_19128_360x0_resize_box_3.png",]
screenshots_img = []
app_id = "org.tabos.twofun"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.tabos.twofun"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://gitlab.com/tabos/twofun/-/raw/master/data/org.tabos.twofun.appdata.xml.in"

+++
