+++
title = "PrBoom"
description = "PrBoom is the culmination of years of work by various people and projects on the Doom source code."
aliases = []
date = 2021-03-14

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "1",]
frameworks = [ "SDL",]
backends = []
services = []
packaged_in = [ "aur", "fedora_38", "fedora_rawhide",]

[extra]
reported_by = "-Euso-"
verified = "❎"
repository = "https://sourceforge.net/projects/prboom/"
homepage = "https://prboom.sourceforge.net/"
more_information = []
summary_source_url = "https://prboom.sourceforge.net/about.html"
screenshots = []
screenshots_img = []
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "prboom",]
appstream_xml_url = ""
+++

