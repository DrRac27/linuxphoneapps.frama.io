+++
title = "Minetest"
description = "Minetest, an open source infinite-world block sandbox game engine with support for survival and crafting."
aliases = []
date = 2019-02-01

[taxonomies]
project_licenses = [ "LGPL-2.1-or-later AND CC-BY-SA-3.0 AND MIT AND Apache-2.0",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
frameworks = []
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://github.com/minetest/minetest"
homepage = "https://www.minetest.net/"
more_information = []
summary_source_url = "https://github.com/minetest/minetest#compiling"
screenshots = []
screenshots_img = []
app_id = "net.minetest.Minetest"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.minetest.Minetest"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "minetest",]
appstream_xml_url = "https://raw.githubusercontent.com/minetest/minetest/master/misc/net.minetest.minetest.appdata.xml"
+++





### Notice

Once this pull request is merged, it will have touch screen support. https://github.com/minetest/minetest/pull/10729
