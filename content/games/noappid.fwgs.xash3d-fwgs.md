+++
title = "Xash3D"
description = "Custom Gold Source engine rewritten from scratch."
aliases = []
date = 2021-03-14

[taxonomies]
project_licenses = [ "not specified",]
categories = [ "game engine",]
mobile_compatibility = [ "1",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "aur",]

[extra]
reported_by = "-Euso-"
verified = "❎"
repository = "https://github.com/FWGS/xash3d-fwgs"
homepage = "https://xash.su/xash3d.html"
more_information = []
summary_source_url = "https://github.com/FWGS/xash3d-fwgs"
screenshots = []
screenshots_img = []
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "xash3d",]
appstream_xml_url = ""
+++

