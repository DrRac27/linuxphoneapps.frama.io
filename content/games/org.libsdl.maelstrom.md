+++
title = "Maelstrom"
description = "You pilot your ship through the dreaded \"Maelstrom\" asteroid belt -- suddenly your best friend thrusts towards you and fires, directly at your cockpit. You raise your shields just in time, and the battle is joined."
aliases = []
date = 2021-05-06

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "1",]
frameworks = [ "SDL",]
backends = []
services = []
packaged_in = [ "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]

[extra]
reported_by = "cornelius1968"
verified = "❎"
repository = "https://www.libsdl.org/projects/Maelstrom/source.html"
homepage = "https://www.libsdl.org/projects/Maelstrom/"
more_information = []
summary_source_url = "https://www.libsdl.org/projects/Maelstrom/"
screenshots = []
screenshots_img = []
app_id = "org.libsdl.Maelstrom"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.libsdl.Maelstrom"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "maelstrom",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/org.libsdl.Maelstrom/master/org.libsdl.Maelstrom.appdata.xml"
+++


