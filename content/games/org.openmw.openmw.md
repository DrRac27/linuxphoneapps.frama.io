+++
title = "OpenMW"
description = "Open source Morrowind implementation."
aliases = []
date = 2021-03-14

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "1",]
frameworks = [ "SDL2",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]

[extra]
reported_by = "preflex"
verified = "❎"
repository = "https://gitlab.com/OpenMW/openmw"
homepage = "https://openmw.org/en/"
more_information = []
summary_source_url = ""
screenshots = []
screenshots_img = []
app_id = "org.openmw.OpenMW"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.openmw.OpenMW"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "openmw",]
appstream_xml_url = "https://gitlab.com/OpenMW/openmw/-/raw/master/files/openmw.appdata.xml"
+++


