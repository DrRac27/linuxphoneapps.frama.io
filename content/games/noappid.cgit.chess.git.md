+++
title = "GNU Chess"
description = "GNU Chess is a chess-playing program."
aliases = []
date = 2021-03-14

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
categories = [ "game",]
mobile_compatibility = [ "5",]
frameworks = []
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]

[extra]
reported_by = "Based_Commgnunism"
verified = "✅"
repository = "https://git.savannah.gnu.org/cgit/chess.git"
homepage = "https://www.gnu.org/software/chess/"
more_information = []
summary_source_url = "https://www.gnu.org/software/chess/chess.html"
screenshots = []
screenshots_img = []
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnuchess",]
appstream_xml_url = ""
+++




### Description

GNU Chess is a chess-playing program. It can be used to play chess against the computer on a terminal or, more commonly, as a chess engine for graphical chess frontends such as Xboard. [Source](https://www.gnu.org/software/chess/chess.html)