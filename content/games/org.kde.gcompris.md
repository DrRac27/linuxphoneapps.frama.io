+++
title = "Gcompris"
description = ""
aliases = []
date = 2021-03-15

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
categories = [ "educational game",]
mobile_compatibility = [ "5",]
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]

[extra]
reported_by = "skyrrd"
verified = "❎"
repository = "https://github.com/gcompris/GCompris-qt"
homepage = "https://www.gcompris.net"
more_information = []
summary_source_url = ""
screenshots = []
screenshots_img = []
app_id = "org.kde.gcompris"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.gcompris"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gcompris",]
appstream_xml_url = "https://raw.githubusercontent.com/gcompris/GCompris-qt/master/org.kde.gcompris.appdata.xml"
+++


