+++
title = "H-Craft"
description = "H-Craft Championship is a fun to play scifi-racer."
aliases = []
date = 2019-02-01

[taxonomies]
project_licenses = [ "z-lib and others", "graphics and fonts are not free licensed!",]
categories = [ "game",]
mobile_compatibility = [ "needs testing",]
frameworks = [ "SDL",]
backends = []
services = []
packaged_in = [ "aur",]

[extra]
reported_by = "cahfofpai"
verified = "❎"
repository = "https://github.com/mzeilfelder/hc1"
homepage = "http://www.irrgheist.com/games.htm"
more_information = []
summary_source_url = "http://www.irrgheist.com/games.htm"
screenshots = [ "http://www.irrgheist.com/hcraftscreenshots.htm https://play.google.com/store/apps/details?id=com.irrgheist.hcraft_championship",]
screenshots_img = []
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "hcraft",]
appstream_xml_url = ""
+++

