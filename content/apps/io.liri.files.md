+++
title = "Liri Files"
description = "Files is a tool for managing files"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Liri",]
categories = [ "file management",]
mobile_compatibility = [ "3",]
status = []
frameworks = [ "QtQuick", "fluid",]
backends = []
services = []
packaged_in = [ "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "Qt", "System", "FileTools", "FileManager",]
programming_languages = [ "Cpp", "C", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/lirios/files"
homepage = ""
bugtracker = "https://github.com/lirios/files/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://liri.io/apps/"
screenshots = [ "https://liri.io/apps/",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.liri.Files"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "liri-files",]
appstream_xml_url = "https://raw.githubusercontent.com/lirios/files/develop/src/app/io.liri.Files.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++


