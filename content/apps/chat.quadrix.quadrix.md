+++
title = "Quadrix"
description = "Minimal, simple, multi-platform chat client for the Matrix protocol"
aliases = []
date = 2022-07-20
updated = 2023-04-23

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Jean-François Alarie",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "ReactXP", "Electron",]
backends = []
services = [ "Matrix",]
packaged_in = [ "Flathub",]
freedesktop_categories = [ "Network", "InstantMessaging", "Chat", "VideoConference",]
programming_languages = [ "TypeScript",]
build_systems = [ "npm",]

[extra]
repository = "https://github.com/alariej/quadrix"
homepage = "https://quadrix.chat/"
bugtracker = "https://github.com/alariej/quadrix/issues/"
donations = "https://github.com/sponsors/alariej"
translations = "https://github.com/alariej/quadrix/blob/dev/src/translations.ts"
more_information = [ "https://github.com/alariej/quadrix/blob/dev/README.md",]
summary_source_url = "https://raw.githubusercontent.com/flathub/chat.quadrix.Quadrix/master/chat.quadrix.Quadrix.metainfo.xml"
screenshots = [ "https://quadrix.chat/appstores/flathub/screenshots/01C.png", "https://quadrix.chat/appstores/flathub/screenshots/02C.png", "https://quadrix.chat/appstores/flathub/screenshots/03C.png", "https://quadrix.chat/appstores/flathub/screenshots/04C.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/chat.quadrix.quadrix/01-main_view.png", "https://img.linuxphoneapps.org/chat.quadrix.quadrix/02-create_room.png", "https://img.linuxphoneapps.org/chat.quadrix.quadrix/03-room_search.png", "https://img.linuxphoneapps.org/chat.quadrix.quadrix/04-direct_message.png", "https://img.linuxphoneapps.org/chat.quadrix.quadrix/05-group_history.png", "https://img.linuxphoneapps.org/chat.quadrix.quadrix/06-group_typing.png", "https://img.linuxphoneapps.org/chat.quadrix.quadrix/07-room_list.png", "https://img.linuxphoneapps.org/chat.quadrix.quadrix/08-impressions.png", "https://img.linuxphoneapps.org/chat.quadrix.quadrix/09-audio_conference.png", "https://img.linuxphoneapps.org/chat.quadrix.quadrix/10-settings.png",]
all_features_touch = 1
intended_for_mobile = 1
app_id = "chat.quadrix.Quadrix"
scale_to_fit = ""
flathub = "https://flathub.org/apps/chat.quadrix.Quadrix"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = "https://snapcraft.io/quadrix"
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/flathub/chat.quadrix.Quadrix/master/chat.quadrix.Quadrix.metainfo.xml"
reported_by = "alpabrz"
updated_by = "linmob"

+++


### Description

No data collection - Completely free, no ads - Does not support End-to-End Encryption - Video-conferencing (uses Jitsi Meet SDK) - Unique top-down messaging feed - Ideally used with a private Matrix homeserver [Author's note: abbreviated from original source] [Source](https://github.com/flathub/chat.quadrix.Quadrix/raw/master/chat.quadrix.Quadrix.metainfo.xml)


### Notice

In its Flatpak version, Quadrix currently only defaults on Phosh to its Wayland version. You may force it in your graphical shell by adding `--enable-features=UseOzonePlatform --ozone-platform=wayland` as arguments. Also, you might have problems with input as in other Electron apps (e.g. wrong input on Phosh, OSK doesn't pop up on Plasma Mobile etc.). There is an official sample web deployment [here](https://app.quadrix.chat/), but it uses the unstable branch and is only intended for test purposes. Regarding its included conferencing features, audio works fine, however, for video your Linux phone's camera probably won't be detected.
