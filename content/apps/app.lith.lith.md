+++
title = "Lith"
description = "Multiplatform WeeChat relay client"
aliases = []
date = 2021-04-18
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "lithapp",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick",]
backends = [ "WeeChat",]
services = [ "IRC",]
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "IRCClient",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "qmake",]

[extra]
repository = "https://github.com/LithApp/Lith"
homepage = "https://lith.app/"
bugtracker = "https://github.com/LithApp/Lith/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/LithApp/Lith"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "app.lith.Lith"
scale_to_fit = ""
flathub = "https://flathub.org/apps/app.lith.Lith"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "lith",]
appstream_xml_url = "https://raw.githubusercontent.com/LithApp/Lith/master/dist/linux/app.lith.Lith.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Lith is a multiplatform WeeChat Relay client, allowing you to connect to your running WeeChat instance from anywhere, be it your phone or your desktop computer. [Source](https://lith.app/)


### Notice

WeeChat client – you will need to set up and configure WeeChat to use this.
