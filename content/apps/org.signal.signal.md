+++
title = "Signal Desktop"
description = "Signal – Private Messenger for Windows, Mac, and Linux"
aliases = []
date = 2022-04-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Signal Foundation",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Electron",]
backends = []
services = [ "Signal",]
packaged_in = [ "alpine_edge", "aur", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "Network", "Chat", "InstantMessaging",]
programming_languages = [ "TypeScript", "JavaScript",]
build_systems = [ "yarn",]

[extra]
repository = "https://github.com/signalapp/Signal-Desktop"
homepage = "https://signal.org"
bugtracker = "https://github.com/signalapp/Signal-Desktop/issues/"
donations = "https://signal.org/donate/"
translations = ""
more_information = [ "https://wiki.mobian.org/doku.php?id=signaldesktop", "https://wiki.mobian.org/doku.php?id=signal", "https://github.com/signalapp/Signal-Desktop/issues/3904#issuecomment-585861062",]
summary_source_url = "https://github.com/signalapp/Signal-Desktop"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/org.signal.signal/1.png", "https://img.linuxphoneapps.org/org.signal.signal/2.png", "https://img.linuxphoneapps.org/org.signal.signal/3.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.signal.Signal"
scale_to_fit = ""
flathub = ""
flatpak_link = "https://elagost.com/flatpak/"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "signal-desktop",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/org.signal.Signal/master/org.signal.Signal.metainfo.xml"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Signal Desktop links with Signal on Android or iOS and lets you message from your Windows, macOS, and Linux computers. [Source](https://github.com/signalapp/Signal-Desktop)


### Notice

Official Signal app for desktops, works pretty well - except for suffering from the common Electron issues: Blurriness, or non-working virtual keyboards (partially on Phosh, totally on Plasma Mobile) when enabling the ozone wayland backend. Sadly, Signal does not provide official ARM64/aarch64 binaries, so that unofficial builds must do: Builds for Debian are listed on the [Mobian Wiki](https://wiki.mobian.org/doku.php?id=signaldesktop), builds for Arch are available on [privacy-shark](https://privacyshark.zero-credibility.net/#howto) and there's also a distribution-independent [flatpak build](https://elagost.com/flatpak/) that should work on every distro. Be sure to examine these before use if your thread model requires it.