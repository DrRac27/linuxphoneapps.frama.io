+++
title = "Qwertone"
description = "Turns your PC into music instrument"
aliases = []
date = 2021-02-03
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Andrii Zymohliad",]
categories = [ "musical tool",]
mobile_compatibility = [ "3",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "aur", "debian_11", "debian_unstable", "devuan_4_0", "flathub", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Education", "Music",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]

[extra]
repository = "https://gitlab.com/azymohliad/qwertone"
homepage = ""
bugtracker = "https://gitlab.com/azymohliad/qwertone/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/azymohliad/qwertone"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.gitlab.azymohliad.Qwertone"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.gitlab.azymohliad.Qwertone"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "qwertone",]
appstream_xml_url = "https://gitlab.com/azymohliad/qwertone/-/raw/master/res/com.gitlab.azymohliad.Qwertone.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++





### Notice

Scales well, but needs the virtual keyboard to make sounds because it's not really made for touchscreens.
