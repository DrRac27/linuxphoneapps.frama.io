+++
title = "YOGA Image Optimizer"
description = "YOGA Image Optimizer is a graphical user interface for YOGA Image that converts and optimizes the size of JPEGs, PNGs and WebP image files."
aliases = []
date = 2021-07-22
updated = 2023-03-25

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Fabien LOISON",]
categories = [ "utilities",]
mobile_compatibility = [ "3",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "Utility", "Graphics",]
programming_languages = [ "Python",]
build_systems = [ "custom",]

[extra]
repository = "https://github.com/flozz/yoga-image-optimizer"
homepage = "https://yoga.flozz.org/"
bugtracker = "https://github.com/flozz/yoga-image-optimizer/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/flozz/yoga-image-optimizer"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.flozz.yoga-image-optimizer"
scale_to_fit = "org.flozz.yoga-image-optimizer"
flathub = "https://flathub.org/apps/org.flozz.yoga-image-optimizer"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "yoga-image-optimizer",]
appstream_xml_url = "https://raw.githubusercontent.com/flozz/yoga-image-optimizer/master/linuxpkg/org.flozz.yoga-image-optimizer.metainfo.xml"
reported_by = "linmob"
updated_by = "linmob"
+++


