+++
title = "Ear Tag"
description = "Edit audio file tags"
aliases = []
date = 2022-09-08
updated = 2023-04-23

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "knuxify",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_edge", "aur", "flathub", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "AudioVideo",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.com/knuxify/eartag"
homepage = "https://gitlab.gnome.org/knuxify/eartag"
bugtracker = "https://gitlab.gnome.org/knuxify/eartag/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/knuxify/eartag/-/raw/main/data/app.drey.EarTag.metainfo.xml.in"
screenshots = [ "https://gitlab.gnome.org/knuxify/eartag/-/raw/main/data/screenshot-scaled.png", "https://gitlab.gnome.org/knuxify/eartag/-/raw/main/data/screenshot-mobile.png", "https://gitlab.gnome.org/knuxify/eartag/-/raw/main/data/screenshot-advanced.png", "https://gitlab.gnome.org/knuxify/eartag/-/raw/main/data/screenshot-empty.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = 1
app_id = "app.drey.EarTag"
scale_to_fit = ""
flathub = "https://flathub.org/apps/app.drey.EarTag"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "eartag",]
appstream_xml_url = "https://gitlab.gnome.org/knuxify/eartag/-/raw/main/data/app.drey.EarTag.metainfo.xml.in"
reported_by = "linmob"
updated_by = "linmob"
+++



### Description

Ear Tag is a simple audio file tag editor. It is primarily geared towards making quick edits or bulk-editing tracks in albums/EPs. Unlike other tagging programs, Ear Tag does not require the user to set up a music library folder. [Source](https://gitlab.gnome.org/knuxify/eartag/-/raw/main/data/app.drey.EarTag.metainfo.xml.in)
