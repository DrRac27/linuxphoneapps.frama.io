+++
title = "Obfuscate"
description = "Censor private information."
aliases = []
date = 2021-07-15
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Bilal Elmoussaoui",]
categories = [ "privacy", "utilities",]
mobile_compatibility = [ "3",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "flathub", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/World/obfuscate/"
homepage = ""
bugtracker = "https://gitlab.gnome.org/World/obfuscate/-/issues/"
donations = ""
translations = ""
more_information = [ "https://apps.gnome.org/app/com.belmoussaoui.Obfuscate/",]
summary_source_url = "https://gitlab.gnome.org/World/obfuscate/"
screenshots = [ "https://twitter.com/linuxphoneapps/status/1415707389429522438",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.belmoussaoui.Obfuscate"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.belmoussaoui.Obfuscate"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-obfuscate",]
appstream_xml_url = "https://gitlab.gnome.org/World/obfuscate/-/raw/master/data/com.belmoussaoui.Obfuscate.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Obfuscate lets you redact your private information from any image. [Source](https://flathub.org/apps/com.belmoussaoui.Obfuscate)


### Notice

Sadly, mobile compatibility got worse in between 0.0.4 and 0.0.7. Barely usable with scale-to-fit.
