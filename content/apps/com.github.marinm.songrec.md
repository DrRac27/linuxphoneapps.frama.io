+++
title = "Songrec"
description = "An open-source, unofficial Shazam client for Linux, written in Rust."
aliases = []
date = 2022-06-06
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Marin",]
categories = [ "multimedia",]
mobile_compatibility = [ "4",]
status = [ "maturing",]
frameworks = [ "GTK3",]
backends = []
services = [ "shazam",]
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Audio",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]

[extra]
repository = "https://github.com/marin-m/SongRec"
homepage = ""
bugtracker = "https://github.com/marin-m/SongRec/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/com.github.marinm.songrec"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/com.github.marinm.songrec/1.png", "https://img.linuxphoneapps.org/com.github.marinm.songrec/2.png", "https://img.linuxphoneapps.org/com.github.marinm.songrec/3.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.marinm.songrec"
scale_to_fit = "songrec"
flathub = "https://flathub.org/apps/com.github.marinm.songrec"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "songrec",]
appstream_xml_url = "https://raw.githubusercontent.com/marin-m/SongRec/master/packaging/rootfs/usr/share/metainfo/com.github.marinm.songrec.metainfo.xml"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Features:  Recognize audio from an arbitrary audio file. Recognize audio from the microphone. Usage from both GUI and command line (for the file recognition part). Provide an history of the recognized songs on the GUI, exportable to CSV. Continuous song detection from the microphone, with the ability to choose your input device. Ability to recognize songs from your speakers rather than your microphone (on compatible PulseAudio setups). Generate a lure from a song that, when played, will fool Shazam into thinking that it is the concerned song. [Source](https://github.com/marin-m/SongRec)


### Notice

Flatpak requires scale-to-fit, native build on Arch Linux ARM somehow does not.
