+++
title = "PDF Arranger"
description = "Simple App to split, rotate, rearrange PDFs"
aliases = []
date = 2020-10-15
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "pdfarranger",]
categories = [ "office",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Office", "Viewer",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]

[extra]
repository = "https://github.com/pdfarranger/pdfarranger"
homepage = ""
bugtracker = "https://github.com/pdfarranger/pdfarranger/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "no quotation"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.jeromerobert.pdfarranger"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.jeromerobert.pdfarranger"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "pdfarranger",]
appstream_xml_url = "https://raw.githubusercontent.com/pdfarranger/pdfarranger/main/data/com.github.jeromerobert.pdfarranger.metainfo.xml"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Small python-gtk application, which helps the user to merge or split pdf documents and rotate, crop and rearrange their pages using an interactive and intuitive graphical interface. [Source](https://github.com/pdfarranger/pdfarranger)
