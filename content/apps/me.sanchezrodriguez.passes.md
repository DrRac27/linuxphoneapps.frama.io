+++
title = "Passes"
description = "A digital pass manager"
aliases = []
date = 2022-03-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Pablo Sánchez Rodríguez",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "debian_12", "debian_unstable", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/pablo-s/passes"
homepage = ""
bugtracker = "https://github.com/pablo-s/passes/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/pablo-s/passes"
screenshots = [ "https://raw.githubusercontent.com/pablo-s/passes/main/data/screenshots/passes.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "me.sanchezrodriguez.passes"
scale_to_fit = ""
flathub = "https://flathub.org/apps/me.sanchezrodriguez.passes"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "passes",]
appstream_xml_url = "https://raw.githubusercontent.com/pablo-s/passes/main/data/me.sanchezrodriguez.passes.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Passes is a simple tool that allows you to import and take your digital passes with you. [Source](https://flathub.org/apps/me.sanchezrodriguez.passes)


### Notice

This app opens and displays .pkpass files
