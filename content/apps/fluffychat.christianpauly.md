+++
title = "FluffyChat (QtQuick)"
description = "FluffyChat is a chat client for Matrix, originally created for Ubuntu Touch"
aliases = []
date = 2019-09-30
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "krillefear",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick",]
backends = []
services = [ "Matrix",]
packaged_in = [ "aur", "nix_stable_22_11",]
freedesktop_categories = [ "Qt", "Network", "Chat", "InstantMessaging",]
programming_languages = [ "QML", "JavaScript", "Cpp",]
build_systems = [ "cmake", "clickable",]

[extra]
repository = "https://gitlab.com/KrilleFear/fluffychat"
homepage = ""
bugtracker = "https://gitlab.com/KrilleFear/fluffychat/-/issues/"
donations = ""
translations = ""
more_information = [ "https://ubports.com/de_DE/blog/ubports-blog-1/post/fluffychat-188", "https://snapcraft.io/fluffychat",]
summary_source_url = "no quotation"
screenshots = [ "https://invidious.kavin.rocks/watch?v=ZKMIdr6bqg4", "https://open-store.io/app/fluffychat.christianpauly",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "fluffychat.christianpauly"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "fluffychat",]
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "script"
+++





### Notice

This project is no longer in active development, replaced by a Flutter based app that has the same name. Merge Requests are still welcome, as this project is still in use on Ubuntu Touch.
