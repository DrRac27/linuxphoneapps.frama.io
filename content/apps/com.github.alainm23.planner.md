+++
title = "Planner"
description = "Never worry about forgetting things again"
aliases = []
date = 2020-08-26
updated = 2023-04-23

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Alain M.",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK3", "Granite",]
backends = []
services = [ "Todoist", "CalDAV",]
packaged_in = [ "flathub", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "Office", "Project Management",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/alainm23/planner"
homepage = "https://useplanner.com/"
bugtracker = "https://github.com/alainm23/planner/issues"
donations = "https://www.patreon.com/alainm23"
translations = "https://github.com/alainm23/planner/tree/master/po#readme"
more_information = []
summary_source_url = "https://planner-todo.web.app/"
screenshots = [ "https://raw.githubusercontent.com/alainm23/planner/master/data/screenshot/screenshot-01.png", "https://raw.githubusercontent.com/alainm23/planner/master/data/screenshot/screenshot-02.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.alainm23.planner"
scale_to_fit = "com.github.alainm23.planner"
flathub = "https://flathub.org/apps/com.github.alainm23.planner"
flatpak_link = "https://appcenter.elementary.io/com.github.alainm23.planner/"
flatpak_recipe = "https://raw.githubusercontent.com/flathub/com.github.alainm23.planner/master/com.github.alainm23.planner.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "elementary-planner",]
appstream_xml_url = "https://raw.githubusercontent.com/alainm23/planner/master/data/com.github.alainm23.task-planner.appdata.xml.in"
reported_by = "linmob"
updated_by = "linmob"
+++



### Notice

2023-04-23: Release 3.0.10 from Flathub fits the screen well; GTK4/libadwaita port in progress (renamed to Task Planner).
