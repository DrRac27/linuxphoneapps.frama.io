+++
title = "Index"
description = "Multi-platform file manager"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "maui",]
categories = [ "file management",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "MauiKit", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "Qt", "System", "FileManager",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/maui/index-fm"
homepage = "https://mauikit.org/apps/index/"
bugtracker = "https://invent.kde.org/maui/index-fm/-/issues/"
donations = ""
translations = ""
more_information = [ "https://medium.com/@temisclopeolimac/index-overview-b2ddcb16534f",]
summary_source_url = "https://invent.kde.org/maui/index-fm"
screenshots = [ "https://medium.com/@temisclopeolimac/index-overview-b2ddcb16534f", "https://medium.com/nitrux/maui-apps-apk-packages-5c966f185f0c", "https://medium.com/nitrux/maui-kde-fcdc920138e2", "https://medium.com/nitrux/maui-plasma-mobile-sprint-2019-c20031700b3b",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.index"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.index"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "index-fm",]
appstream_xml_url = "https://invent.kde.org/maui/index-fm/-/raw/master/org.kde.index.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++




### Description

Index is a file manager that works on desktops, Android and Plasma Mobile. Index lets you browse your system files and applications and preview your music, text, image and video files and share them with external applications. [Source](https://invent.kde.org/maui/index-fm)
