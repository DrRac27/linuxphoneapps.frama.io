+++
title = "Plano"
description = "Calculate slope and midpoint for a Plane"
aliases = []
date = 2021-09-28
updated = 2023-04-23

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Diego Iván",]
categories = [ "calculator",]
mobile_compatibility = [ "5",]
status = [ "inactive",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Calculator",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/Diego-Ivan/plano-rewritten"
homepage = "https://github.com/diegoivanme/plano-rewritten"
bugtracker = "https://github.com/Diego-Ivan/plano-rewritten/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/Diego-Ivan/plano-rewritten"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.diegoivanme.plano.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "plano",]
appstream_xml_url = "https://raw.githubusercontent.com/Diego-Ivan/plano-rewritten/main/data/com.github.diegoivanme.plano.appdata.xml.in"
reported_by = "linmob"
updated_by = "linmob"
+++



### Notice

Project is inactive, last commit on 2021-11-06. 
