+++
title = "Squeekboard"
description = "The final Librem5 keyboard"
aliases = []
date = 2020-09-06
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "GNOME Developers",]
categories = [ "virtual keyboard",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Rust", "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/World/Phosh/squeekboard"
homepage = ""
bugtracker = "https://gitlab.gnome.org/World/Phosh/squeekboard/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://source.puri.sm/Librem5/squeekboard"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "sm.puri.Squeekboard"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "squeekboard",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"
+++




### Description

Squeekboard is a virtual keyboard supporting Wayland, built primarily for the Librem 5 phone. It squeaks because some Rust got inside. [Source](https://gitlab.gnome.org/World/Phosh/squeekboard)


### Notice

replacement for virtboard