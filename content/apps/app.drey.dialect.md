+++
title = "Dialect"
description = "A translation app for GNOME based on Google Translate."
aliases = [ "apps/com.github.gi_lom.dialect/",]
date = 2020-10-16
updated = 2023-03-25

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The Dialect Authors",]
categories = [ "translation tool",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Google Translate", "Libre Translate",]
packaged_in = [ "aur", "debian_12", "debian_unstable", "fedora_38", "fedora_rawhide", "flathub", "gnuguix", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "TextTools", "Dictionary",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/dialect-app/dialect"
homepage = ""
bugtracker = "https://github.com/dialect-app/dialect/issues/"
donations = ""
translations = "https://hosted.weblate.org/engage/dialect/"
more_information = [ "https://apps.gnome.org/app/app.drey.Dialect/",]
summary_source_url = "https://github.com/dialect-app/dialect"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "app.drey.Dialect"
scale_to_fit = ""
flathub = "https://flathub.org/apps/app.drey.Dialect"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "dialect",]
appstream_xml_url = "https://raw.githubusercontent.com/dialect-app/dialect/main/data/app.drey.Dialect.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "linmob"
+++




### Description

Features: - Translation based on Google Translate, - Translation based on the LibreTranslate API, allowing you to use any public instance, - Translation history, - Automatic language detection, - Text to speech, - Clipboard buttons [Source](https://github.com/dialect-app/dialect/)


### Notice

Supports Libre Translate since release 1.2.0, GTK4/libadwaita since release 2.0.
