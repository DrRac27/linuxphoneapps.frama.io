+++
title = "Authenticator"
description = "Generate Two-Factor Codes."
aliases = []
date = 2019-02-16
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Bilal Elmoussaoui",]
categories = [ "multi-factor authentication",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "debian_11", "debian_unstable", "devuan_4_0", "flathub", "gnuguix", "nix_stable_22_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/World/Authenticator"
homepage = ""
bugtracker = "https://gitlab.gnome.org/World/Authenticator/-/issues/"
donations = "https://liberapay.com/bielmoussaoui"
translations = "https://l10n.gnome.org/module/authenticator/"
more_information = [ "https://apps.gnome.org/app/com.belmoussaoui.Authenticator/",]
summary_source_url = "https://gitlab.gnome.org/World/Authenticator/"
screenshots = [ "https://gitlab.gnome.org/World/Authenticator/", "https://matrix.to/#/!BSqRHgvCtIsGittkBG:talk.puri.sm/$1547610383565VYxvY:disroot.org?via=talk.puri.sm&via=matrix.org&via=disroot.org", "https://www.youtube.com/watch?v=troGmVpQCF0",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.bilelmoussaoui.Authenticator"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.belmoussaoui.Authenticator"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-authenticator",]
appstream_xml_url = "https://gitlab.gnome.org/World/Authenticator/-/raw/master/data/com.belmoussaoui.Authenticator.metainfo.xml.in.in"
reported_by = "cahfofpai"
updated_by = "script"
+++





### Notice

Previously, before release 4.*, this program was using GTK3/libhandy and written in Python, e.g. Debian still ship that release.
