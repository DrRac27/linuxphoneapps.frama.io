+++
title = "Nota"
description = "Nota is a simple text editor for desktop and mobile computers."
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "LGPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "maui",]
categories = [ "text editor",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "MauiKit", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "Qt", "KDE", "Utility", "TextEditor",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/maui/nota"
homepage = "https://mauikit.org/apps/nota/"
bugtracker = "https://invent.kde.org/maui/nota/-/issues/"
donations = "https://opencollective.com/nitrux"
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/maui/nota/-/blob/master/org.kde.nota.metainfo.xml"
screenshots = [ "https://apps.kde.org/nota/", "https://mauikit.org/wp-content/uploads/2021/08/nota-1-1536x1016.png", "https://medium.com/@temisclopeolimac/mauis-monthly-c39bbb2de9d0", "https://medium.com/nitrux/maui-kde-fcdc920138e2",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.nota"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "maui-nota",]
appstream_xml_url = "https://invent.kde.org/maui/nota/-/raw/master/org.kde.nota.metainfo.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++




### Description

The Nota text editor features a simple interface by default. It features syntax highlighting, dynamic word wrap, an embedded console, and some preliminary plugin support. [Source](https://mauikit.org/apps/nota/)


### Notice

Some buttons, e.g. delete and info, don't work and there's a warning displayed when browsing files (at least for Version 2.1.2 on pmOS and Manjaro).
