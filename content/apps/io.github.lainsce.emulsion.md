+++
title = "Emulsion"
description = "Stock up on colors"
aliases = []
date = 2021-05-19
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Lains",]
categories = [ "development",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Graphics", "Publishing",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/lainsce/emulsion"
homepage = ""
bugtracker = "https://github.com/lainsce/emulsion/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/lainsce/emulsion/"
screenshots = [ "https://github.com/lainsce/emulsion/",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.github.lainsce.Emulsion"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.lainsce.Emulsion"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "emulsion",]
appstream_xml_url = "https://raw.githubusercontent.com/lainsce/emulsion/main/data/io.github.lainsce.Emulsion.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Store your palettes in an easy way, and edit them if needed.  [Source](https://github.com/lainsce/emulsion/)
