+++
title = "GNOME Notes"
description = "Note editor designed to remain simple to use"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later", "LGPL-2.0-or-later", "LGPL-3.0-only",]
metadata_licenses = []
app_author = [ "GNOME Developers",]
categories = [ "note taking",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = [ "gnome-settings-daemon",]
services = []
packaged_in = [ "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "TextTools",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-notes"
homepage = "https://wiki.gnome.org/Apps/Notes"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-notes/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-notes"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Notes"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Notes"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-notes",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-notes/-/raw/master/data/appdata/org.gnome.Notes.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "script"
+++


