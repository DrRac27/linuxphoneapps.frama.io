+++
title = "Goodvibes"
description = "A Lightweight Radio Player"
aliases = []
date = 2020-11-07
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "goodvibes",]
categories = [ "internet radio",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Audio", "Player",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/goodvibes/goodvibes"
homepage = ""
bugtracker = "https://gitlab.com/goodvibes/goodvibes/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/goodvibes/goodvibes"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.gitlab.Goodvibes"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.gitlab.Goodvibes"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "goodvibes",]
appstream_xml_url = "https://gitlab.com/goodvibes/goodvibes/-/raw/master/data/io.gitlab.Goodvibes.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++





### Notice

Scales perfectly, has proven to be crashy on Arch Linux ARM
