+++
title = "Compass"
description = "This is a proof of concept of a simple compass app for Mobile Linux."
aliases = []
date = 2021-06-23
updated = 2023-04-14

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "lgtrombetta",]
categories = [ "compass",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/lgtrombetta/compass"
homepage = ""
bugtracker = "https://gitlab.com/lgtrombetta/compass/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/lgtrombetta/compass"
screenshots = [ "https://gitlab.com/lgtrombetta/compass/-/raw/main/data/screenshot.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.gitlab.lgtrombetta.Compass"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "pinephone-compass",]
appstream_xml_url = "https://gitlab.com/lgtrombetta/compass/-/raw/main/data/com.gitlab.lgtrombetta.Compass.appdata.xml.in"
reported_by = "linmob"
updated_by = "lgtrombetta"
+++




### Description

Currently supported devices:

- Pine64 Pinephone v1.0, v1.1, v1.2 (LIS3MDL magnetometer)
- Pine64 Pinephone v1.2b (AF8133J magnetometer)
- Pine64 Pinephone Pro (AF8133J magnetometer, requires Megi's kernel>=6.3 for the correct mount-matrix)
- Purism Librem 5 (LSM9DS1 magnetometer)

Known issues:

- AF8133J is not currently recognized in pmOS

[Source](https://gitlab.com/lgtrombetta/compass)


### Notice

Works great after calibration!
