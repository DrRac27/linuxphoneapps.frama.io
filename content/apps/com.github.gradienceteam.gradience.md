+++
title = "Gradience"
description = "Change the look of Adwaita, with ease"
aliases = []
date = 2022-09-17
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Gradience Team",]
categories = [ "utilities",]
mobile_compatibility = [ "4",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/GradienceTeam/Gradience"
homepage = "https://gradience.atrophaneura.tech/"
bugtracker = "https://github.com/GradienceTeam/Gradience/issues/"
donations = ""
translations = "https://hosted.weblate.org/projects/GradienceTeam/gradience/"
more_information = [ "https://github.com/orgs/GradienceTeam/discussions",]
summary_source_url = "https://github.com/GradienceTeam/Gradience"
screenshots = [ "https://raw.githubusercontent.com/GradienceTeam/Design/main/Screenshots/advanced_purple.png", "https://raw.githubusercontent.com/GradienceTeam/Design/main/Screenshots/colors_purple.png", "https://raw.githubusercontent.com/GradienceTeam/Design/main/Screenshots/monet_purple.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/com.github.gradienceteam.gradience/1.png", "https://img.linuxphoneapps.org/com.github.gradienceteam.gradience/2.png", "https://img.linuxphoneapps.org/com.github.gradienceteam.gradience/3.png", "https://img.linuxphoneapps.org/com.github.gradienceteam.gradience/4.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.GradienceTeam.Gradience"
scale_to_fit = "com.github.GradienceTeam.Gradience gradience"
flathub = "https://flathub.org/apps/com.github.GradienceTeam.Gradience"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gradience",]
appstream_xml_url = "https://raw.githubusercontent.com/GradienceTeam/Gradience/main/data/com.github.GradienceTeam.Gradience.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Gradience is a tool for customizing Libadwaita applications and the adw-gtk3 theme. [Source](https://github.com/GradienceTeam/Gradience)


### Notice

Release 0.2.2 is just a few pixels to wide to fit the screen and is usable without adjustments. To fit the color picker, make sure to run scale-to-fit gradience true.
