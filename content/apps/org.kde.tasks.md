+++
title = "Tasks"
description = "Simple task list"
aliases = []
date = 2022-04-26
updated = 2022-12-23

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community", "Felipe Kinoshita",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/utilities/toad"
homepage = ""
bugtracker = "https://invent.kde.org/utilities/toad/-/issues"
donations = ""
translations = ""
more_information = [ "http://web.archive.org/web/20220422210752/https://fhek.gitlab.io/en/tasks/", "http://web.archive.org/web/20220602083557/https://fhek.gitlab.io/en/my-week-in-kde-improvements-to-tasks/",]
summary_source_url = "https://invent.kde.org/utilities/toad/-/raw/master/org.kde.tasks.metainfo.xml"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/org.kde.tasks/1.png", "https://img.linuxphoneapps.org/org.kde.tasks/2.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.tasks"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://invent.kde.org/utilities/toad/-/raw/master/org.kde.tasks.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

Tasks is a simple task list application, you can edit tasks on the fly and it saves all your work automatically. [Source](https://invent.kde.org/utilities/toad/-/raw/master/org.kde.tasks.metainfo.xml)
