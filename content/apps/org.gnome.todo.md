+++
title = "Endeavour"
description = "Personal task manager for GNOME"
aliases = []
date = 2022-10-04
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Jamie Murphy",]
categories = [ "productivity",]
mobile_compatibility = [ "4",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Office",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/World/Endeavour"
homepage = "https://wiki.gnome.org/Apps/Todo"
bugtracker = "https://gitlab.gnome.org/World/Endeavour/-/issues/"
donations = ""
translations = ""
more_information = [ "https://feaneron.com/2022/06/21/giving-up-on-gnome-to-do/",]
summary_source_url = "https://gitlab.gnome.org/World/Endeavour"
screenshots = [ "https://gitlab.gnome.org/World/Endeavour/raw/main/data/appdata/no-tasks.png", "https://gitlab.gnome.org/World/Endeavour/raw/main/data/appdata/task-list.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.todo/1.png", "https://img.linuxphoneapps.org/org.gnome.todo/2.png", "https://img.linuxphoneapps.org/org.gnome.todo/3.png", "https://img.linuxphoneapps.org/org.gnome.todo/4.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Todo"
scale_to_fit = "org.gnome.todo"
flathub = "https://flathub.org/apps/org.gnome.Todo"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-todo", "endeavour",]
appstream_xml_url = "https://gitlab.gnome.org/World/Endeavour/-/raw/main/data/appdata/org.gnome.Todo.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Endeavour is an intuitive and powerful application to manage your personal tasks. It uses GNOME technologies and has complete integration with the GNOME desktop environment. [Source](https://gitlab.gnome.org/World/Endeavour)


### Notice

Previously known as GNOME To Do. Endeavour upstream is not yet adaptive, but Mobian has been shipping an [adaptive build](https://salsa.debian.org/Mobian-team/packages/gnome-todo) for a long time, with a [patchset](https://salsa.debian.org/Mobian-team/packages/gnome-todo/-/tree/mobian/debian/patches) maintained up to 41 release.
