+++
title = "Peruse"
description = "A comic book viewer based on Frameworks 5, for use on multiple form factors"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "KDE Community",]
categories = [ "document viewer",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Graphics", "Viewer",]
programming_languages = [ "Cpp", "QML", "C",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/graphics/peruse"
homepage = "https://peruse.kde.org/"
bugtracker = "https://invent.kde.org/graphics/peruse/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/graphics/peruse"
screenshots = [ "https://peruse.kde.org/",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.peruse"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "peruse",]
appstream_xml_url = "https://invent.kde.org/graphics/peruse/-/raw/master/src/app/org.kde.peruse.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++


