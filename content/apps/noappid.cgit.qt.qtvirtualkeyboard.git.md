+++
title = "Qt Virtual Keyboard"
description = "The Qt Virtual Keyboard project provides an input framework and reference keyboard frontend for Qt 5 on Linux Desktop/X11, Windows Desktop, and Boot2Qt targets."
aliases = []
date = 2019-02-16

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Qt Company",]
categories = [ "virtual keyboard",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge",]
freedesktop_categories = [ "Qt", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://code.qt.io/cgit/qt/qtvirtualkeyboard.git"
homepage = "https://doc.qt.io/qt-5/qtvirtualkeyboard-index.html"
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://doc.qt.io/qt-5/qtvirtualkeyboard-index.html"
screenshots = [ "https://doc.qt.io/qt-5/qtvirtualkeyboard-basic-example.html",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "qtvirtualkeyboard-plasma",]
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = ""
+++


