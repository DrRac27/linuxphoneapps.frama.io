+++
title = "Tootle"
description = "GTK-based Mastodon client for Linux"
aliases = []
date = 2020-08-25
updated = 2023-04-22

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "bleak_grey",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "Mastodon",]
packaged_in = [ "alpine_3_17", "aur", "debian_11", "devuan_4_0", "gnuguix", "nix_stable_22_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Feed",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/bleakgrey/tootle"
homepage = ""
bugtracker = "https://github.com/bleakgrey/tootle/issues/"
donations = "https://liberapay.com/bleakgrey/donate"
translations = ""
more_information = []
summary_source_url = "https://github.com/bleakgrey/tootle"
screenshots = [ "https://wiki.mobian-project.org/doku.php?id=tootle",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.bleakgrey.tootle"
scale_to_fit = "com.github.bleakgrey.tootle"
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/bleakgrey/tootle/master/com.github.bleakgrey.tootle.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "tootle",]
appstream_xml_url = "https://raw.githubusercontent.com/bleakgrey/tootle/master/data/com.github.bleakgrey.tootle.appdata.xml.in"
reported_by = "linmob"
updated_by = "linmob"
+++




### Description

Simple Mastodon client for Linux [Source](https://github.com/bleakgrey/tootle)


### Notice

Archived. Version 2.0 was going to be GTK4 and libadwaita, its development has been continued as [Tuba](https://linuxphoneapps.org/apps/dev.geopjr.tuba/).
