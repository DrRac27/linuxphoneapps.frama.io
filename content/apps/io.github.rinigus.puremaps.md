+++
title = "Pure Maps"
description = "Maps and navigation"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "rinigus",]
categories = [ "maps and navigation",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick", "Kirigami", "Silica",]
backends = []
services = [ "openstreetmap",]
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "flathub", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "Qt", "Utility", "Maps",]
programming_languages = [ "QML", "Python", "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/rinigus/pure-maps"
homepage = ""
bugtracker = "https://github.com/rinigus/pure-maps/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/rinigus/pure-maps"
screenshots = [ "https://flathub.org/apps/io.github.rinigus.PureMaps",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.github.rinigus.PureMaps"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.rinigus.PureMaps"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "pure-maps",]
appstream_xml_url = "https://raw.githubusercontent.com/rinigus/pure-maps/91e155d4ce580d0f0dd8bc08cbdf010925ddf4ac/packaging/pure-maps.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++




### Description

Pure Maps is an application for Linux and Sailfish OS to display vector and raster maps, places, routes, and provide navigation instructions with a flexible selection of data and service providers. [Source](https://github.com/rinigus/pure-maps)
