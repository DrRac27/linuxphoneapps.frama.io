+++
title = "File Shredder"
description = "Securely delete your files"
aliases = []
date = 2022-09-17
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Alan Beveridge",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "shred",]
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/ADBeveridge/raider"
homepage = ""
bugtracker = "https://github.com/ADBeveridge/raider/issues/"
donations = ""
translations = "https://github.com/ADBeveridge/raider/tree/develop/po"
more_information = [ "https://apps.gnome.org/app/com.github.ADBeveridge.Raider/",]
summary_source_url = "https://github.com/ADBeveridge/raider"
screenshots = [ "https://raw.githubusercontent.com/ADBeveridge/raider/main/data/raider-screenshot1.png", "https://raw.githubusercontent.com/ADBeveridge/raider/main/data/raider-screenshot2.png", "https://raw.githubusercontent.com/ADBeveridge/raider/main/data/raider-screenshot3.png", "https://raw.githubusercontent.com/ADBeveridge/raider/main/data/raider-screenshot4.png", "https://raw.githubusercontent.com/ADBeveridge/raider/main/data/raider-screenshot5.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/com.github.adbeveridge.raider/1.png", "https://img.linuxphoneapps.org/com.github.adbeveridge.raider/2.png", "https://img.linuxphoneapps.org/com.github.adbeveridge.raider/3.png", "https://img.linuxphoneapps.org/com.github.adbeveridge.raider/4.png", "https://img.linuxphoneapps.org/com.github.adbeveridge.raider/5.png", "https://img.linuxphoneapps.org/com.github.adbeveridge.raider/6.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.ADBeveridge.Raider"
scale_to_fit = "com.github.ADBeveridge.Raider.Help"
flathub = "https://flathub.org/apps/com.github.ADBeveridge.Raider"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "raider-file-shredder",]
appstream_xml_url = "https://raw.githubusercontent.com/ADBeveridge/raider/develop/data/com.github.ADBeveridge.Raider.metainfo.xml"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Raider is a simple shredding program built for the GNOME desktop. Also known as File Shredder, it uses a program from the GNU Core Utilities package, included on every Linux distribution, called shred. Raider supports all the options that shred supports. [Source](https://github.com/ADBeveridge/raider)
