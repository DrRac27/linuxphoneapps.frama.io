+++
title = "Phonic"
description = "Phonic is a GTK audiobook player targeted at mobile Linux."
aliases = []
date = 2020-09-06
updated = 2022-12-23

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "hamner",]
categories = [ "audiobook player",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "GNOME", "Audio", "Player",]
programming_languages = [ "Python",]
build_systems = [ "none",]

[extra]
repository = "https://sr.ht/~hamner/Phonic/"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.sr.ht/~hamner/Phonic"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "phonic",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"
+++

