+++
title = "caerbannog"
description = "Frontend for password-store"
aliases = []
date = 2021-02-06
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "craftyguy",]
categories = [ "password manager",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = [ "pass",]
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://git.sr.ht/~craftyguy/caerbannog"
homepage = ""
bugtracker = "https://todo.sr.ht/~craftyguy/caerbannog"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.sr.ht/~craftyguy/caerbannog"
screenshots = [ "https://fosstodon.org/@linmob/105686222789395866",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "caerbannog",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"
+++

