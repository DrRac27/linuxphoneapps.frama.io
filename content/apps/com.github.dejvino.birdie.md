+++
title = "Birdie"
description = "Wakeup Alarm App for a Linux Phone"
aliases = []
date = 2021-04-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = []
app_author = [ "dejvino",]
categories = [ "alarm clock",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "Utility", "Clock",]
programming_languages = [ "Python", "C",]
build_systems = [ "make",]

[extra]
repository = "https://github.com/Dejvino/birdie"
homepage = ""
bugtracker = "https://github.com/Dejvino/birdie/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/Dejvino/birdie"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.dejvino.birdie"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "birdie",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"
+++





### Notice

An improvement on wake-mobile