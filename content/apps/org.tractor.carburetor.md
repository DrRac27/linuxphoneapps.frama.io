+++
title = "Carburetor"
description = "GTK and Libawaita app for connecting to Tor network"
aliases = []
date = 2023-03-17

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-SA-4.0",]
app_author = [ "Danial Behzadi",]
categories = [ "network",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "tractor",]
services = [ "Tor",]
packaged_in = [ "debian_12", "debian_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Network",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]

[extra]
repository = "https://framagit.org/tractor/carburetor"
homepage = "https://framagit.org/tractor/"
bugtracker = "https://framagit.org/tractor/carburetor/-/issues"
donations = ""
translations = "https://hosted.weblate.org/engage/carburetor"
more_information = [ "https://thisweek.gnome.org/posts/2023/01/twig-77/#third-party-projects",]
summary_source_url = "no quotation"
screenshots = [ "https://thisweek.gnome.org/posts/2023/01/twig-77/aad1dfd5929eb03d4e027330c5a43dd8b49a3f99.mp4",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.tractor.carburetor"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "carburetor",]
appstream_xml_url = "https://framagit.org/tractor/carburetor/-/raw/main/carburetor/desktop/org.tractor.carburetor.metainfo.xml"
reported_by = "danialbehzadi"
updated_by = ""
+++




### Description

Carburetor is a graphical settings app for tractor which is a package uses Python stem library to provide a connection through the onion proxy and sets up proxy in user session, so you don't have to mess up with TOR on your system anymore. [Source](https://framagit.org/tractor/carburetor/-/raw/main/carburetor/desktop/org.tractor.carburetor.metainfo.xml)