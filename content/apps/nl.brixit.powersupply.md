+++
title = "Powersupply"
description = "Linux power subsystem debugger"
aliases = [ "apps/noappid.martijnbraam.powersupply/",]
date = 2022-03-25
updated = 2023-06-04

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Martijn Braam",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "flathub", "pureos_landing",]
freedesktop_categories = [ "GTK", "System",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]

[extra]
repository = "https://gitlab.com/MartijnBraam/powersupply"
homepage = "https://gitlab.com/MartijnBraam/powersupply"
bugtracker = "https://gitlab.com/MartijnBraam/powersupply/-/issues"
donations = ""
translations = ""
more_information = [ "https://wiki.postmarketos.org/wiki/Applications_by_category#System",]
summary_source_url = "https://gitlab.com/MartijnBraam/powersupply"
screenshots = [ "http://brixitcdn.net/metainfo/powersupply.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = 1
app_id = "nl.brixit.powersupply"
scale_to_fit = ""
flathub = "https://flathub.org/apps/nl.brixit.powersupply"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "powersupply",]
appstream_xml_url = "https://gitlab.com/MartijnBraam/powersupply/-/raw/master/data/nl.brixit.powersupply.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++


### Description

GTK3 app to display power status of phones [Source](https://gitlab.com/MartijnBraam/powersupply)
