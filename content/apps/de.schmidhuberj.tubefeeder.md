+++
title = "Tubefeeder"
description = "A YouTube, LBRY and PeerTube client"
aliases = []
date = 2021-03-31
updated = "2023-04-19"

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "Julian Schmidhuber",]
categories = [ "multimedia",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "yt-dlp",]
services = [ "YouTube", "lbry", "PeerTube",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Video", "Player",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]

[extra]
repository = "https://github.com/Tubefeeder/Tubefeeder"
homepage = "https://www.tubefeeder.de/"
bugtracker = "https://github.com/Tubefeeder/Tubefeeder/issues"
donations = "https://www.tubefeeder.de/donate"
translations = ""
more_information = [ "https://www.tubefeeder.de/wiki/different-player.html",]
summary_source_url = "https://github.com/Tubefeeder/Tubefeeder"
screenshots = [ "https://raw.githubusercontent.com/Tubefeeder/Tubefeeder/master/data/screenshots/tubefeeder_screenshot_feed.png", "https://raw.githubusercontent.com/Tubefeeder/Tubefeeder/master/data/screenshots/tubefeeder_screenshot_watch_later.png", "https://raw.githubusercontent.com/Tubefeeder/Tubefeeder/master/data/screenshots/tubefeeder_screenshot_filters.png", "https://raw.githubusercontent.com/Tubefeeder/Tubefeeder/master/data/screenshots/tubefeeder_screenshot_subscriptions.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/de.schmidhuberj.tubefeeder/1.png", "https://img.linuxphoneapps.org/de.schmidhuberj.tubefeeder/2.png", "https://img.linuxphoneapps.org/de.schmidhuberj.tubefeeder/3.png", "https://img.linuxphoneapps.org/de.schmidhuberj.tubefeeder/4.png", "https://img.linuxphoneapps.org/de.schmidhuberj.tubefeeder/5.png",]
all_features_touch = ""
intended_for_mobile = 1
app_id = "de.schmidhuberj.tubefeeder"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.schmidhuberj.tubefeeder"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "tubefeeder",]
appstream_xml_url = "https://raw.githubusercontent.com/Tubefeeder/Tubefeeder/master/data/de.schmidhuberj.tubefeeder.metainfo.xml"
reported_by = "linmob"
updated_by = "linmob"
+++



### Description

Tubefeeder lets you watch and download videos from YouTube, LBRY and PeerTube, all without needing to navigate through different websites.

Tubefeeder comes with several features:

*   Subscribe to channels
*   Play videos with MPV (or any other video player)
*   Filter out unwanted videos in the feed
*   Import data from NewPipe

[Source](https://raw.githubusercontent.com/Tubefeeder/Tubefeeder/master/data/de.schmidhuberj.tubefeeder.metainfo.xml)


### Notice

Make sure to read https://github.com/Schmiddiii/Tubefeeder/wiki. Ported to GTK4/libadwaita with release 1.6.0.
