+++
title = "Quickddit"
description = "A free and open source Reddit client for mobile phones."
aliases = []
date = 2020-12-22
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "accumulator",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick",]
backends = []
services = [ "reddit",]
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "Network", "News",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "qmake",]

[extra]
repository = "https://github.com/accumulator/Quickddit"
homepage = ""
bugtracker = "https://github.com/accumulator/Quickddit/issues/"
donations = ""
translations = ""
more_information = [ "https://linmob.net/2020/10/06/reddit-clients-for-mobile-linux.html",]
summary_source_url = "https://open-store.io/app/quickddit"
screenshots = [ "https://fosstodon.org/@linmob/105424788379486065",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "quickddit",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"
+++


