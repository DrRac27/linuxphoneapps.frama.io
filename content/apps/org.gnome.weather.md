+++
title = "GNOME Weather"
description = "Monitor the current weather conditions for your city, or anywhere in the world."
aliases = []
date = 2021-05-30
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "The GNOME Project",]
categories = [ "weather",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = [ "api.met.no",]
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "JavaScript",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-weather"
homepage = ""
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-weather/-/issues/"
donations = ""
translations = ""
more_information = [ "https://apps.gnome.org/app/org.gnome.Weather/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-weather"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Weather"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Weather"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-weather",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-weather/-/raw/master/data/org.gnome.Weather.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++





### Notice

Mobile compliant since version 40
