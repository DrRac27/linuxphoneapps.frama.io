+++
title = "DayKountdown"
description = "A simple date countdown app written for KDE Plasma, using Kirigami."
aliases = []
date = 2021-04-29
updated = 2023-02-15

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Plasma Mobile Developers",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "KDE", "Utility", "Clock",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/utilities/daykountdown"
homepage = ""
bugtracker = "https://invent.kde.org/utilities/daykountdown/-/issues/"
donations = ""
translations = ""
more_information = [ "https://plasma-mobile.org/2021/04/27/plasma-mobile-update-march-april/",]
summary_source_url = "https://invent.kde.org/utilities/daykountdown"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.daykountdown"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "daykountdown",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "linmob"
+++


