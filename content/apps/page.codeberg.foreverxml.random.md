+++
title = "Random"
description = "Make randomization easy"
aliases = []
date = 2022-03-26
updated = 2022-12-19

[taxonomies]
project_licenses = [ "AGPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Forever",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://codeberg.org/foreverxml/random"
homepage = "https://forever.amongtech.cc/random/"
bugtracker = "https://codeberg.org/foreverxml/random/issues/"
donations = ""
translations = ""
more_information = [ "https://stickynote.amongtech.cc/randomapp/", "https://random.amongtech.cc/help.html",]
summary_source_url = "https://codeberg.org/foreverxml/random"
screenshots = [ "https://codeberg.org/foreverxml/random/raw/branch/main/screenshots/coin.png", "https://codeberg.org/foreverxml/random/raw/branch/main/screenshots/number.png", "https://codeberg.org/foreverxml/random/raw/branch/main/screenshots/roulette.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "page.codeberg.foreverxml.Random"
scale_to_fit = ""
flathub = "https://flathub.org/apps/page.codeberg.foreverxml.Random"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "randomgtk",]
appstream_xml_url = "https://codeberg.org/foreverxml/random/raw/branch/main/data/page.codeberg.foreverxml.Random.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Random has three modes that you can use to do nearly any randomizing function you need. With these three modes, you can do a lot. You can pick how many candies to eat, what movie to watch, who goes first in the shower, and more. [Source](https://forever.amongtech.cc/random/)
