+++
title = "MyGNUHealth"
description = "MyGNUHealth is the GNUHealth Personal Health Record application for desktop and mobile devices that integrates with the GNU Health Federation.\" "
aliases = []
date = 2020-10-15
updated = 2022-11-06

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "GNU Solidario",]
categories = [ "health",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "Kirigami",]
backends = []
services = [ "GNU Health",]
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "fedora_38", "fedora_rawhide", "gnuguix", "opensuse_tumbleweed",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "Python", "QML",]
build_systems = [ "custom",]

[extra]
repository = "https://hg.savannah.gnu.org/hgweb/health-mygnuhealth/"
homepage = "https://www.gnuhealth.org/"
bugtracker = "https://lists.gnu.org/mailman/listinfo/health"
donations = ""
translations = ""
more_information = [ "https://www.gnuhealth.org/docs/mygnuhealth/",]
summary_source_url = "https://invent.kde.org/pim/mygnuhealth/"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnuhealth.my"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "mygnuhealth",]
appstream_xml_url = "https://hg.savannah.gnu.org/hgweb/health-mygnuhealth/raw-file/c6b4e9bd3c69/mygnuhealth/org.gnuhealth.mygnuhealth.metainfo.xml"
reported_by = "linmob"
updated_by = "linmob"
+++





### Notice

Can be installed using pip: pip3 install --user --upgrade MyGNUHealth