+++
title = "GPodder Adaptive"
description = "Media aggregator and podcast client for mobile and desktop alike"
aliases = [ "apps/org.gpodder.gpodder-adaptive/",]
date = 2021-02-06
updated = 2023-04-24

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The gPodder Team",]
categories = [ "podcast client",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Feed",]
programming_languages = [ "Python",]
build_systems = [ "make",]

[extra]
repository = "https://github.com/gpodder/gpodder/tree/adaptive"
homepage = "https://gpodder.github.io"
bugtracker = "https://github.com/gpodder/gpodder/issues/"
donations = ""
translations = ""
more_information = [ "https://wiki.mobian.org/doku.php?id=gpodder&s[]=gpodder", "https://wiki.postmarketos.org/wiki/GPodder",]
summary_source_url = "https://raw.githubusercontent.com/gpodder/gpodder/adaptive/share/metainfo/org.gpodder.gpodder-adaptive.appdata.xml"
screenshots = [ "https://raw.githubusercontent.com/flathub/org.gpodder.gpodder-adaptive/master/screenshot1.png", "https://raw.githubusercontent.com/flathub/org.gpodder.gpodder-adaptive/master/screenshot2.png", "https://raw.githubusercontent.com/flathub/org.gpodder.gpodder-adaptive/master/screenshot3.png", "https://raw.githubusercontent.com/flathub/org.gpodder.gpodder-adaptive/master/screenshot4.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = 1
app_id = "org.gpodder.gpodder-adaptive"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gpodder.gpodder-adaptive"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/flathub/org.gpodder.gpodder-adaptive/master/org.gpodder.gpodder-adaptive.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gpodder-adaptive", "gpodder",]
appstream_xml_url = "https://raw.githubusercontent.com/gpodder/gpodder/adaptive/share/metainfo/org.gpodder.gpodder-adaptive.appdata.xml"
reported_by = "linmob"
updated_by = "linmob"
+++



### Description

gPodder lets you manage your Podcast subscriptions, discover new content and download episodes to your devices.

You can also take advantage of the service gpodder.net, which lets you sync subscriptions, playback progress and starred episodes.

This is a version of gPodder with an adaptive interface meant for both touchscreen devices, like phones and tablets, and desktop and laptop computers. [Source](https://flathub.org/apps/org.gpodder.gpodder-adaptive)

### Notice

GPodder adaptive became a branch of the main project, and has been scaling just fine since.
