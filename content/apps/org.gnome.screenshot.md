+++
title = "GNOME Screenshot"
description = "GNOME Screenshot is a small utility that takes a screenshot of the whole desktop, the currently focused window, or an area of the screen."
aliases = []
date = 2021-08-07
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "GNOME Developers",]
categories = [ "utilities",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-screenshot"
homepage = ""
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-screenshot/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-screenshot"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Screenshot"
scale_to_fit = "gnome-screenshots"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-screenshot",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-screenshot/-/raw/master/data/org.gnome.Screenshot.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"
+++





### Notice

Works for "Screen" capture area on Phosh 0.12.1 with release 40. Needs scale-to-fit.