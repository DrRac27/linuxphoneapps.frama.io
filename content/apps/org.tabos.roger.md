+++
title = "Roger Router"
description = "Roger Router is a utility to control and monitor AVM Fritz!Box Routers."
aliases = []
date = 2022-04-12
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Jan-Michael Brummer",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "FRITZ!Box",]
packaged_in = [ "aur", "debian_11", "debian_unstable", "devuan_4_0", "flathub", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Telephony",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/tabos/rogerrouter"
homepage = "https://tabos.gitlab.io/projects/rogerrouter/"
bugtracker = "https://gitlab.com/tabos/rogerrouter/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/tabos/rogerrouter"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/org.tabos.roger/1.png", "https://img.linuxphoneapps.org/org.tabos.roger/2.png", "https://img.linuxphoneapps.org/org.tabos.roger/3.png", "https://img.linuxphoneapps.org/org.tabos.roger/4.png", "https://img.linuxphoneapps.org/org.tabos.roger/5.png", "https://img.linuxphoneapps.org/org.tabos.roger/6.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.tabos.roger"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.tabos.roger"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "roger-router",]
appstream_xml_url = "https://gitlab.com/tabos/rogerrouter/-/raw/master/data/org.tabos.roger.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Roger Router is a clean solution for controlling the FRITZ!Box or compatible router with Linux. It offers a rich feature list, including call monitor, journal, fax support and also has a great integration into several address books. [Source](https://gitlab.com/tabos/rogerrouter/-/raw/master/data/org.tabos.roger.appdata.xml.in)
