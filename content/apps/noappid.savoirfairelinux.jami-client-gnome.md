+++
title = "Jami GNOME"
description = "Jami-client-gnome is a Jami client written in GTK+3."
aliases = []
date = 2020-10-27
updated = 2022-04-26

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-SA-3.0",]
app_author = [ "savoirfairelinux",]
categories = [ "chat", "telephony",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "GTK3", "Clutter",]
backends = []
services = [ "Jami", "SIP",]
packaged_in = [ "archlinuxarm_aarch64", "archlinuxarm_armv7h",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Audio", "Telephony",]
programming_languages = [ "Cpp", "C",]
build_systems = [ "cmake",]

[extra]
repository = "https://git.jami.net/savoirfairelinux/jami-client-gnome"
homepage = "https://jami.net/"
bugtracker = ""
donations = ""
translations = ""
more_information = [ "https://git.jami.net/savoirfairelinux/jami-client-gnome/-/issues/1299",]
summary_source_url = "https://git.jami.net/savoirfairelinux/jami-client-gnome"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "jami-gnome",]
appstream_xml_url = "https://git.jami.net/savoirfairelinux/jami-client-gnome/-/raw/master/jami-gnome.appdata.xml"
reported_by = "linmob"
updated_by = "linmob"
+++





### Notice

Adjusts fairly well, uses Xwayland because of Clutter. [Likely deprecated](https://git.jami.net/savoirfairelinux/jami-client-gnome/-/issues/1299) in favour of [Jami-qt](@/apps/net.jami.jami.md).
