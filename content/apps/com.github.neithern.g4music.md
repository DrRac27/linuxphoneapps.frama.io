+++
title = "G4Music"
description = "Play your music in an elegant way."
aliases = []
date = 2022-06-21
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Nanling",]
categories = [ "music player",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Audio", "Music", "Player",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/neithern/g4music"
homepage = ""
bugtracker = "https://gitlab.gnome.org/neithern/g4music/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/neithern/g4music"
screenshots = [ "https://gitlab.gnome.org/neithern/g4music/-/raw/master/shots/playbar.png", "https://gitlab.gnome.org/neithern/g4music/-/raw/master/shots/playlist.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.neithern.g4music"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.neithern.g4music"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "g4music",]
appstream_xml_url = "https://gitlab.gnome.org/neithern/g4music/-/raw/master/data/app.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

A fast, fluent, light weight music player written in GTK4, with a beautiful, adaptive user interface, so named G4Music. [Source](https://gitlab.gnome.org/neithern/g4music)
