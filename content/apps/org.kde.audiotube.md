+++
title = "AudioTube"
description = "Client for YouTube Music"
aliases = []
date = 2021-03-14
updated = 2023-03-05

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "audio streaming",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Kirigami",]
backends = [ "ytmusicapi", "youtube-dl",]
services = [ "YouTube Music",]
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "fedora_38", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "Qt", "KDE", "Network", "Audio", "Player",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/multimedia/audiotube"
homepage = "https://apps.kde.org/audiotube"
bugtracker = "https://invent.kde.org/multimedia/audiotube/-/issues/"
donations = ""
translations = ""
more_information = [ "https://jbbgameich.github.io/kde/2021/03/13/audiotube.html", "https://plasma-mobile.org/2021/04/27/plasma-mobile-update-march-april/", "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/#audiotube", "https://plasma-mobile.org/2023/01/30/january-blog-post/#audiotube-youtube-music-client",]
summary_source_url = "https://invent.kde.org/multimedia/audiotube"
screenshots = [ "https://jbbgameich.github.io/kde/2021/03/13/audiotube.html",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.audiotube"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.audiotube"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "audiotube",]
appstream_xml_url = "https://invent.kde.org/multimedia/audiotube/-/raw/master/org.kde.audiotube.appdata.xml"
reported_by = "cahfofpai"
updated_by = "linmob"
+++




### Description

AudioTube can search YouTube Music, list albums and artists, play automatically generated playlists, albums and allows to put your own playlist together. [Source](https://invent.kde.org/multimedia/audiotube/-/blob/master/org.kde.audiotube.appdata.xml)
