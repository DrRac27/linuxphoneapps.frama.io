+++
title = "Master Key"
description = "Master Key is a password manager application built with Python 3 and GTK that generates and manages passwords without the need to store them."
aliases = []
date = 2021-02-03
updated = 2023-04-26

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Guillermo Peña",]
categories = [ "password generator",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/guillermop/master-key"
homepage = ""
bugtracker = "https://gitlab.com/guillermop/master-key/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/guillermop/master-key/"
screenshots = [ "https://fosstodon.org/@linmob/105669735264241384",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.gitlab.guillermop.MasterKey"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.gitlab.guillermop.MasterKey"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "master-key",]
appstream_xml_url = "https://gitlab.com/guillermop/master-key/-/raw/master/data/com.gitlab.guillermop.MasterKey.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "linmob"
+++


### Notice

Ported to GTK4 with 1.2.0, used GTK3/libadwaita before.
