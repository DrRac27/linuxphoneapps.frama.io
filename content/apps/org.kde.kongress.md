+++
title = "Kongress"
description = "Companion application for conference attendees"
aliases = []
date = 2020-02-06
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "KDE Community",]
categories = [ "conference companion",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/utilities/kongress"
homepage = ""
bugtracker = "https://invent.kde.org/utilities/kongress/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/utilities/kongress"
screenshots = [ "https://invent.kde.org/utilities/kongress",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.kongress"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "kongress",]
appstream_xml_url = "https://invent.kde.org/utilities/kongress/-/raw/master/org.kde.kongress.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++




### Description

Kongress provides practical information about conferences. It supports conferences that offer their schedule in iCalendar format. In Kongress, the data of the talks are shown in various ways, e.g. in daily views, by talk category, etc. The users can also create a list of favorite conference talks/events as well as they can navigate to the web page of each talk. A map of the conference venue, location information and link to OpenStreetMap can also be added. [Source](https://invent.kde.org/utilities/kongress)
