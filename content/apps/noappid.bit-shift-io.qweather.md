+++
title = "Quick Weather"
description = "Quick Weather QML Qt application"
aliases = []
date = 2022-04-12
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "bit-shift-io",]
categories = [ "weather",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "QtQuick",]
backends = [ "bom.gov.au",]
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/bit-shift-io/qweather"
homepage = ""
bugtracker = "https://github.com/bit-shift-io/qweather/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/bit-shift-io/qweather"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "qweather",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"
+++




### Description

Quick Weather QML Qt application for desktop and mobile. Using BOM weather for Australia. [Source](https://github.com/bit-shift-io/qweather)


### Notice

For Australia only.
