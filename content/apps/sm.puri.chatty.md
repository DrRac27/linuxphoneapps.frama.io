+++
title = "Chats"
description = "Messaging application for mobile and desktop"
aliases = []
date = 2019-02-01
updated = 2023-06-04

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Purism",]
categories = [ "SMS", "chat",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = [ "libpurple", "ModemManager",]
services = [ "SMS", "MMS", "XMPP", "Matrix",]
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Chat",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://source.puri.sm/Librem5/chatty"
homepage = "https://source.puri.sm/Librem5/chatty"
bugtracker = "https://source.puri.sm/Librem5/chatty/-/issues/"
donations = ""
translations = ""
more_information = [ "https://wiki.mobian.org/doku.php?id=chatty",]
summary_source_url = "https://source.puri.sm/Librem5/chatty"
screenshots = [ "https://source.puri.sm/Librem5/chatty/-/raw/master/data/screenshots/screenshot.png?inline=false",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = 1
app_id = "sm.puri.Chatty"
scale_to_fit = ""
flathub = "https://flathub.org/apps/sm.puri.Chatty"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "purism-chatty",]
appstream_xml_url = "https://source.puri.sm/Librem5/chatty/-/raw/master/data/sm.puri.Chatty.metainfo.xml.in"
reported_by = "cahfofpai"
updated_by = "script"
+++

