+++
title = "GNOME Settings"
description = "GNOME's main interface to configure various aspects of the desktop. Fork for librem5 specific settings"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = []
app_author = [ "Purism",]
categories = [ "settings",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Settings", "DesktopSettings",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://source.puri.sm/Librem5/gnome-control-center"
homepage = ""
bugtracker = "https://source.puri.sm/Librem5/gnome-control-center/-/issues/"
donations = ""
translations = ""
more_information = [ "https://apps.gnome.org/app/org.gnome.Settings/",]
summary_source_url = "https://source.puri.sm/Librem5/gnome-control-center"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Settings"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "pureos-gnome-settings", "manjaro-gnome-settings", "gnome-settings-daemon",]
appstream_xml_url = "https://source.puri.sm/Librem5/gnome-control-center/-/raw/librem5-3-24/shell/appdata/gnome-control-center.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "script"
+++

