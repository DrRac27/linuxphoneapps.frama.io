+++
title = "Wike"
description = "Search and read Wikipedia articles"
aliases = []
date = 2021-05-09
updated = 2023-04-24

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Hugo Olabera",]
categories = [ "education",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Wikipedia",]
packaged_in = [ "aur", "debian_12", "debian_unstable", "fedora_38", "fedora_rawhide", "flathub", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "GTK", "Education", "Network",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/hugolabe/Wike"
homepage = "https://hugolabe.github.io/Wike/"
bugtracker = "https://github.com/hugolabe/Wike/issues"
donations = ""
translations = "https://poeditor.com/join/project?hash=kNgJu4MAum"
more_information = [ "https://apps.gnome.org/app/com.github.hugolabe.Wike/",]
summary_source_url = "https://github.com/hugolabe/Wike"
screenshots = [ "https://raw.githubusercontent.com/hugolabe/wike/master/data/screenshots/wike-01.png", "https://raw.githubusercontent.com/hugolabe/wike/master/data/screenshots/wike-02.png", "https://raw.githubusercontent.com/hugolabe/wike/master/data/screenshots/wike-03.png", "https://raw.githubusercontent.com/hugolabe/wike/master/data/screenshots/wike-04.png", "https://raw.githubusercontent.com/hugolabe/wike/master/data/screenshots/wike-05.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/com.github.hugolabe.wike/1.png", "https://img.linuxphoneapps.org/com.github.hugolabe.wike/2.png", "https://img.linuxphoneapps.org/com.github.hugolabe.wike/3.png", "https://img.linuxphoneapps.org/com.github.hugolabe.wike/4.png", "https://img.linuxphoneapps.org/com.github.hugolabe.wike/5.png",]
all_features_touch = 1
intended_for_mobile = 1
app_id = "com.github.hugolabe.Wike"
scale_to_fit = "wike"
flathub = "https://flathub.org/apps/com.github.hugolabe.Wike"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/hugolabe/Wike/master/build-aux/flatpak/com.github.hugolabe.Wike.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "wike",]
appstream_xml_url = "https://raw.githubusercontent.com/hugolabe/Wike/master/data/com.github.hugolabe.Wike.metainfo.xml.in"
reported_by = "linmob"
updated_by = "linmob"
+++



### Description

Wike is a Wikipedia reader for the GNOME Desktop. Provides access to all the content of this online encyclopedia in a native application, with a simpler and distraction-free view of articles.
      
It supports more than 300 languages and integrates with desktop searches, providing suggestions that make it easier to find any content. Other features are: bookmarks with multiple lists, article table of contents, history, text searches and much more… [Source](https://raw.githubusercontent.com/hugolabe/Wike/master/data/com.github.hugolabe.Wike.metainfo.xml.in)

### Notice

GTK4/libadwaita since release 2.0.0, fully adaptive since 2.0.1.

For the previous GTK3/libhandy releases standard min-width was a bit too wide, scale-to-fit wike on can make it usable.

