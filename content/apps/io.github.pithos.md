+++
title = "Pithos"
description = "A Pandora Radio Client"
aliases = []
date = 2020-11-07
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Pithos",]
categories = [ "internet radio",]
mobile_compatibility = [ "3",]
status = []
frameworks = [ "GTK3",]
backends = []
services = [ "Pandora",]
packaged_in = [ "alpine_edge", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "flathub", "gentoo", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Audio", "Player",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/pithos/pithos"
homepage = "https://pithos.github.io/"
bugtracker = "https://github.com/pithos/pithos/issues/"
donations = ""
translations = ""
more_information = [ "https://wiki.mobian-project.org/doku.php?id=pithos",]
summary_source_url = "https://github.com/pithos/pithos"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.github.Pithos"
scale_to_fit = "pithos"
flathub = "https://flathub.org/apps/io.github.Pithos"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "pithos",]
appstream_xml_url = "https://raw.githubusercontent.com/pithos/pithos/master/data/io.github.Pithos.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++





### Notice

could not be tested, service is US only
