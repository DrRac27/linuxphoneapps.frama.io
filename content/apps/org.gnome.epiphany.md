+++
title = "GNOME Web (Epiphany)"
description = "A simple, clean, beautiful view of the Web"
aliases = []
date = 2019-02-01
updated = 2023-04-26

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "The GNOME Project",]
categories = [ "web browser",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "webkit2gtk",]
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing", "snapcraft",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "WebBrowser",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/epiphany"
homepage = "https://wiki.gnome.org/Apps/Web"
bugtracker = "https://gitlab.gnome.org/GNOME/epiphany/-/issues/"
donations = "http://www.gnome.org/friends/"
translations = "https://wiki.gnome.org/TranslationProject"
more_information = [ "https://puri.sm/posts/end-of-year-librem-5-update/", "https://apps.gnome.org/app/org.gnome.Epiphany/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/epiphany"
screenshots = [ "https://gitlab.gnome.org/GNOME/epiphany/raw/HEAD/data/screenshot.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.epiphany/1.png", "https://img.linuxphoneapps.org/org.gnome.epiphany/2.png", "https://img.linuxphoneapps.org/org.gnome.epiphany/3.png", "https://img.linuxphoneapps.org/org.gnome.epiphany/4.png",]
all_features_touch = 1
intended_for_mobile = 1
app_id = "org.gnome.Epiphany"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Epiphany"
flatpak_link = ""
flatpak_recipe = "https://gitlab.gnome.org/GNOME/epiphany/-/raw/master/org.gnome.Epiphany.json"
snapcraft = "https://snapcraft.io/epiphany"
snap_link = ""
snap_recipe = ""
repology = [ "epiphany-browser",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/epiphany/-/raw/master/data/org.gnome.Epiphany.appdata.xml.in.in"
reported_by = "cahfofpai"
updated_by = "linmob"
+++





### Description

GNOME Web (codename: Epiphany) is a GNOME web browser based on the WebKit rendering engine. The codename means "a usually sudden manifestation or perception of the essential nature or meaning of something" (Merriam-Webster). [Source](https://gitlab.gnome.org/GNOME/epiphany)

### Notice

GTK3/libhandy before release 44.
