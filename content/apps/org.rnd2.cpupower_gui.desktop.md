+++
title = "cpupower-gui"
description = "Change the scaling frequency limits of your CPU"
aliases = []
date = 2022-03-13
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Evangelos Rigas",]
categories = [ "system utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur", "debian_11", "debian_unstable", "devuan_4_0", "nix_stable_22_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "System",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/vagnum08/cpupower-gui"
homepage = ""
bugtracker = "https://github.com/vagnum08/cpupower-gui/issues/"
donations = ""
translations = ""
more_information = [ "https://wiki.mobian-project.org/doku.php?id=cpupower-gui",]
summary_source_url = "no quotation"
screenshots = [ "https://github.com/vagnum08/cpupower-gui/blob/master/screenshots/mobile.png?raw=true",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.rnd2.cpupower_gui.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "cpupower-gui",]
appstream_xml_url = "https://raw.githubusercontent.com/vagnum08/cpupower-gui/master/data/org.rnd2.cpupower_gui.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

cpupower-gui is a graphical program that is used to change the scaling frequency limits of the cpu, similar to cpupower. [Source](https://github.com/vagnum08/cpupower-gui)