+++
title = "GNOME Disks"
description = "GNOME Disks, gnome-disk-image-mounter and gsd-disk-utility-notify are libraries and applications for dealing with storage devices."
aliases = []
date = 2020-12-12
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "The GNOME Project",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-disk-utility"
homepage = "https://wiki.gnome.org/Apps/Disks"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-disk-utility/-/issues/"
donations = ""
translations = ""
more_information = [ "https://apps.gnome.org/app/org.gnome.DiskUtility/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-disk-utility/"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.DiskUtility"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-disk-utility",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-disk-utility/-/raw/master/data/org.gnome.DiskUtility.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++

