+++
title = "Flowtime"
description = "Get what motivates you done, without losing concentration."
aliases = []
date = 2021-12-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Diego Iván",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "flathub", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Clock",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/Diego-Ivan/Flowtime"
homepage = ""
bugtracker = "https://github.com/Diego-Ivan/Flowtime/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/Diego-Ivan/Flowtime"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.github.diegoivanme.flowtime"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.diegoivanme.flowtime"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "flowtime",]
appstream_xml_url = "https://raw.githubusercontent.com/Diego-Ivan/Flowtime/main/data/io.github.diegoivanme.flowtime.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++


