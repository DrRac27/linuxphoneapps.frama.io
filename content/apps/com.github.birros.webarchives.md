+++
title = "Web Archives"
description = "A web archives reader"
aliases = []
date = 2021-04-29
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "birros",]
categories = [ "education",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Education", "Humanities",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/birros/web-archives"
homepage = ""
bugtracker = "https://github.com/birros/web-archives/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/birros/web-archives"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.birros.WebArchives"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.birros.WebArchives"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/birros/web-archives/master/data/appdata/com.github.birros.WebArchives.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++



### Description

A web archives reader offering the ability to browse offline millions of articles from large community projects such as Wikipedia or Wikisource. [Source](https://github.com/birros/web-archives)


### Notice

Only supports ZIM files, but works pretty great!
