+++
title = "GNOME Sound Recorder"
description = "A simple and modern sound recorder"
aliases = []
date = 2021-03-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "audio recorder",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Audio", "Recorder",]
programming_languages = [ "JavaScript",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-sound-recorder"
homepage = "https://wiki.gnome.org/Apps/SoundRecorder"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-sound-recorder/-/issues/"
donations = "https://www.gnome.org/donate/"
translations = "https://wiki.gnome.org/TranslationProject"
more_information = []
summary_source_url = "https://wiki.gnome.org/Apps/SoundRecorder"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.SoundRecorder"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.SoundRecorder"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-sound-recorder",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-sound-recorder/-/raw/master/data/org.gnome.SoundRecorder.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++





### Notice

GTK4/libadwaita since release 42.
