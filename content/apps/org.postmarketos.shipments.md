+++
title = "Shipments"
description = "A python library and a GTK3 mobile/desktop application for tracking multiple shipments."
aliases = []
date = 2021-12-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "martijnbraam",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://git.sr.ht/~martijnbraam/shipments"
homepage = ""
bugtracker = "https://todo.sr.ht/~martijnbraam/shipments"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.sr.ht/~martijnbraam/shipments"
screenshots = [ "https://twitter.com/braam_martijn/status/1464973302993149953/photo/1",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.postmarketos.Shipments"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "shipments",]
appstream_xml_url = "https://git.sr.ht/~martijnbraam/shipments/blob/master/data/org.postmarketos.Shipments.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Supported carriers: 4PX, DHL (needs api code), InPost, PostNL, Russian Post, UPS [Source](https://git.sr.ht/~martijnbraam/shipments)