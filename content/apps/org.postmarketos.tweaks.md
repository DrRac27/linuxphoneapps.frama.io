+++
title = "postmarketOS Tweaks"
description = "Application for exposing extra settings easily on mobile platforms"
aliases = []
date = 2021-04-07
updated = 2022-12-19

[taxonomies]
project_licenses = [ "LGPL-3.0-only",]
metadata_licenses = []
app_author = [ "martijnbraam",]
categories = [ "system utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "postmarketos_22.12",]
freedesktop_categories = [ "GTK", "GNOME", "Settings", "DesktopSettings",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/postmarketOS/postmarketos-tweaks"
homepage = ""
bugtracker = "https://gitlab.com/postmarketOS/postmarketos-tweaks/-/issues/"
donations = ""
translations = ""
more_information = [ "https://www.youtube.com/watch?v=X_QuQKhEVRA",]
summary_source_url = "https://gitlab.com/postmarketOS/postmarketos-tweaks"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.postmarketos.Tweaks"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "postmarketos-tweaks",]
appstream_xml_url = "https://gitlab.com/postmarketOS/postmarketos-tweaks/-/raw/master/data/org.postmarketos.Tweaks.metainfo.xml"
reported_by = "linmob"
updated_by = "script"
+++

