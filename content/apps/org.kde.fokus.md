+++
title = "Kirigami Application"
description = "A short summary describing what this software is about"
aliases = []
date = 2021-07-29
updated = 2023-04-22

[taxonomies]
project_licenses = [ "no license specified",]
metadata_licenses = []
app_author = [ "rowdyninja",]
categories = [ "task management",]
mobile_compatibility = [ "5",]
status = [ "inactive", "early",]
frameworks = [ "Kirigami", "QtQuick",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "Office", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/rowdyninja/fokus"
homepage = ""
bugtracker = "https://invent.kde.org/rowdyninja/fokus/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/rowdyninja/fokus"
screenshots = [ "https://twitter.com/linuxphoneapps/status/1420787887801569289",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.fokus"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://invent.kde.org/rowdyninja/fokus/-/raw/master/org.kde.fokus.appdata.xml"
reported_by = "linmob"
updated_by = "linmob"

+++

### Description

Fokus is a convergent task management app built using Kirigami and Qt frameworks. Fokus is mainly being built for plasma-mobile platform, but it can also be used on the desktop. [Source](https://invent.kde.org/rowdyninja/fokus)


### Notice

WIP (still buggy), last commit on 2021-03-30.
