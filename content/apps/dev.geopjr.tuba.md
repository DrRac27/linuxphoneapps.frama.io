+++
title = "Tuba"
description = "Browse the Fediverse"
aliases = [ "apps/dev.geopjr.tooth/",]
date = 2022-11-30
updated = 2023-03-25

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Evangelos \"GeopJr\" Paterakis",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Mastodon",]
packaged_in = [ "alpine_3_18", "alpine_edge", "aur", "flathub", "nix_unstable", "postmarketos_22.12",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Feed", "Chat",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/GeopJr/Tuba"
homepage = "https://tuba.geopjr.dev/"
bugtracker = "https://github.com/GeopJr/Tuba/issues/"
donations = "https://geopjr.dev/donate"
translations = "https://hosted.weblate.org/engage/tuba/"
more_information = []
summary_source_url = "https://github.com/GeopJr/Tuba"
screenshots = [ "https://i.imgur.com/jKmFZou.png", "https://i.imgur.com/3essApP.png", "https://i.imgur.com/jBF85mI.png", "https://i.imgur.com/XBtQsya.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/dev.geopjr.tuba/1.png", "https://img.linuxphoneapps.org/dev.geopjr.tuba/2.png", "https://img.linuxphoneapps.org/dev.geopjr.tuba/3.png", "https://img.linuxphoneapps.org/dev.geopjr.tuba/4.png", "https://img.linuxphoneapps.org/dev.geopjr.tuba/5.png", "https://img.linuxphoneapps.org/dev.geopjr.tuba/6.png", "https://img.linuxphoneapps.org/dev.geopjr.tuba/7.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "dev.geopjr.Tuba"
scale_to_fit = ""
flathub = "https://flathub.org/apps/dev.geopjr.Tuba"
flatpak_link = "https://nightly.link/GeopJr/Tuba/workflows/build/main/dev.geopjr.Tuba.Devel-aarch64.zip"
flatpak_recipe = "https://raw.githubusercontent.com/GeopJr/Tuba/main/build-aux/dev.geopjr.Tuba.Devel.json"
snapcraft = ""
snap_link = "https://nightly.link/GeopJr/Tuba/workflows/build/main/snap-aarch64.zip"
snap_recipe = "https://raw.githubusercontent.com/GeopJr/Tuba/main/build-aux/snap/snapcraft.yaml"
repology = [ "tooth", "tuba",]
appstream_xml_url = "https://raw.githubusercontent.com/GeopJr/Tuba/main/data/dev.geopjr.Tuba.appdata.xml.in"
reported_by = "linmob"
updated_by = "linmob"
+++




### Description

Explore the federated social web with Tuba for GNOME. Stay connected to your favorite communities, family and friends with support for popular Fediverse platforms like Mastodon, GoToSocial, Akkoma & more! The Fediverse is a decentralized social network that consists of multiple interconnected platforms and communities, allowing users to communicate and share content across different networks and servers. It promotes user privacy and data ownership, and offers an alternative to centralized social media platforms. [Source](https://raw.githubusercontent.com/GeopJr/Tuba/main/data/dev.geopjr.Tuba.appdata.xml.in)


### Notice

Continuation of [Tootle](https://linuxphoneapps.org/apps/com.github.bleakgrey.tootle/), was called Tooth before its initial release.
