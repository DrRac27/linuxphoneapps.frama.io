+++
title = "Tokodon"
description = "Tokodon is a Mastodon client for Plasma and Plasma Mobile"
aliases = []
date = 2021-05-29
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The KDE Community",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "Kirigami",]
backends = []
services = [ "Mastodon",]
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Qt", "KDE", "Network",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/network/tokodon"
homepage = "https://apps.kde.org/tokodon/"
bugtracker = "https://invent.kde.org/network/tokodon/-/issues/"
donations = ""
translations = ""
more_information = [ "https://carlschwan.eu/2021/12/18/more-kde-apps/", "https://plasma-mobile.org/2022/04/26/plasma-mobile-gear-22-04/#tokodon", "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/#tokodon",]
summary_source_url = "https://invent.kde.org/network/tokodon"
screenshots = [ "https://cdn.kde.org/screenshots/tokodon/tokodon-home.png", "https://cdn.kde.org/screenshots/tokodon/tokodon-login.png", "https://fosstodon.org/@linmob/106216619777841108",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.tokodon"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.tokodon"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "tokodon",]
appstream_xml_url = "https://invent.kde.org/network/tokodon/-/raw/master/org.kde.tokodon.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++


