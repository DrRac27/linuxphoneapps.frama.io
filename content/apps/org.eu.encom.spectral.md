+++
title = "Spectral"
description = "A glossy cross-platform Matrix client."
aliases = []
date = 2020-03-02
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Black Hat",]
categories = [ "chat",]
mobile_compatibility = [ "3",]
status = []
frameworks = [ "QtQuick",]
backends = [ "libQuotient",]
services = [ "Matrix",]
packaged_in = [ "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "flathub", "pureos_landing",]
freedesktop_categories = [ "Qt", "Network", "Chat", "InstantMessaging",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://gitlab.com/spectral-im/spectral"
homepage = "https://spectral.im/"
bugtracker = "https://gitlab.com/spectral-im/spectral/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/spectral-im/spectral"
screenshots = [ "https://gitlab.com/spectral-im/spectral",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.eu.encom.spectral"
scale_to_fit = "org.eu.encom.spectral"
flathub = "https://flathub.org/apps/org.eu.encom.spectral"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "spectral-matrix",]
appstream_xml_url = "https://gitlab.com/spectral-im/spectral/-/raw/master/linux/org.eu.encom.spectral.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++




### Description

Spectral is a glossy cross-platform client for Matrix, the decentralized communication protocol for instant messaging. [Source](https://gitlab.com/spectral-im/spectral)
