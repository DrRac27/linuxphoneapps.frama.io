+++
title = "pass-manager-compact"
description = "Compact GUI for pass to be useable for smartphones"
aliases = []
date = 2021-02-06
updated = 2023-04-22

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "chrisu281080",]
categories = [ "password manager",]
mobile_compatibility = [ "needs testing",]
status = [ "maturing",]
frameworks = [ "GTK3",]
backends = [ "pass",]
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "none",]

[extra]
repository = "https://gitlab.com/chrisu281080/pass-manager-compact"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/chrisu281080/pass-manager-compact"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = 1
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "linmob"

+++

### Description


This a compact GUI for pass (https://www.passwordstore.org/). It’s designed to be used for the Librem5 (or other Linux Smartphones with GTK). But it can also be used on normal screens.

It supports the extension pass-otp (https://github.com/tadfisher/pass-otp
and the extension pass-tomb (https://github.com/roddhjav/pass-tomb) [Source](https://gitlab.com/chrisu281080/pass-manager-compact/-/blob/master/README.md)

### Notice

Binary for Debian can be downloaded from the [repo](https://gitlab.com/chrisu281080/pass-manager-compact/-/tree/master/deb).

#helpwanted, testing needed
