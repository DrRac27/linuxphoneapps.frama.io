+++
title = "Paper Plane"
description = "Chat over Telegram on a modern and elegant client"
aliases = [ "apps/com.github.melix99.telegrand/",]
date = 2021-04-18
updated = "2023-06-04"

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0",]
app_author = [ "Marco Melorio",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "Telegram",]
services = [ "Telegram",]
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Chat", "InstantMessaging",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/paper-plane-developers/paper-plane"
homepage = "https://github.com/paper-plane-developers/paper-plane"
bugtracker = "https://github.com/paper-plane-developers/paper-plane/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/paper-plane-developers/paper-plane"
screenshots = [ "https://github.com/paper-plane-developers/paper-plane/raw/main/data/resources/screenshots/screenshot1.png",]
screenshots_img = []
all_features_touch = true
intended_for_mobile = true
app_id = "app.drey.PaperPlane"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "paper-plane",]
appstream_xml_url = "https://raw.githubusercontent.com/paper-plane-developers/paper-plane/main/data/app.drey.PaperPlane.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "linmob"

+++

### Notice

WIP, in early beta, read the [readme's API key section](https://github.com/paper-plane-developers/paper-plane#telegram-api-credentials) regarding the risks of getting your account banned.
