+++
title = "GNOME Firmware"
description = "Install firmware on devices"
aliases = []
date = 2022-04-06
updated = 2022-12-23

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Richard Hughes",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "fwupd",]
services = [ "Linux Vendor Firmware Service",]
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "System", "Security",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/World/gnome-firmware"
homepage = ""
bugtracker = "https://gitlab.gnome.org/World/gnome-firmware/-/issues"
donations = "https://www.gnome.org/donate/"
translations = ""
more_information = [ "https://dylanvanassche.be/blog/2022/pinephone-modem-upgrade/", "https://linmob.net/easily-upgrading-pinephone-pro-modem-firmware/",]
summary_source_url = "https://gitlab.gnome.org/World/gnome-firmware"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.firmware/1.png", "https://img.linuxphoneapps.org/org.gnome.firmware/2.png", "https://img.linuxphoneapps.org/org.gnome.firmware/3.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Firmware"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Firmware"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-firmware", "gnome-firmware-updater",]
appstream_xml_url = "https://gitlab.gnome.org/World/gnome-firmware/-/raw/master/data/appdata/org.gnome.Firmware.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

This application can: - Upgrade, Downgrade, & Reinstall firmware on devices supported by fwupd. - Unlock locked fwupd devices Verify firmware on supported devices, - Display all releases for a fwupd device [Source](https://gitlab.gnome.org/World/gnome-firmware)
