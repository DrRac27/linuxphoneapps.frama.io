+++
title = "Sums"
description = "A postfix calculator"
aliases = []
date = 2021-03-31
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Joshua Lee",]
categories = [ "calculator",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Calculator",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/leesonwai/sums"
homepage = ""
bugtracker = "https://gitlab.com/leesonwai/sums/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/leesonwai/sums"
screenshots = [ "https://gitlab.com/leesonwai/sums",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.gitlab.leesonwai.Sums"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.gitlab.leesonwai.Sums"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "sums",]
appstream_xml_url = "https://gitlab.com/leesonwai/sums/-/raw/main/data/io.gitlab.leesonwai.Sums.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Sums is a simple GTK postfix calculator that adheres to GNOME's human-interface guidelines. It is designed to be keyboard-driven and aims to feel natural to interact with by recognising English-language input as mathematical constants and operations. [Source](https://gitlab.com/leesonwai/sums)
