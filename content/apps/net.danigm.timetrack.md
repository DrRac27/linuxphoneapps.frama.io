+++
title = "Timetrack"
description = "Simple timetrack app for GNOME"
aliases = []
date = 2020-08-25
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Daniel Garcia Moreno",]
categories = [ "productivity",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/danigm/timetrack"
homepage = ""
bugtracker = "https://gitlab.gnome.org/danigm/timetrack/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/danigm/timetrack"
screenshots = [ "https://wiki.mobian-project.org/doku.php?id=timetrack",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "net.danigm.timetrack"
scale_to_fit = "Timetrack"
flathub = "https://flathub.org/apps/net.danigm.timetrack"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "timetrack",]
appstream_xml_url = "https://gitlab.gnome.org/danigm/timetrack/-/raw/master/data/net.danigm.timetrack.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++


