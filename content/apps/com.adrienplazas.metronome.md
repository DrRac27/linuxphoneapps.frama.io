+++
title = "Metronome"
description = "Practice music with a regular tempo"
aliases = []
date = 2021-08-11
updated = 2022-12-23

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Adrien Plazas",]
categories = [ "musical tool",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "debian_11", "debian_unstable", "devuan_4_0", "flathub", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Education", "Music",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/World/metronome"
homepage = ""
bugtracker = "https://gitlab.gnome.org/World/metronome/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/World/metronome"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/com.adrienplazas.metronome/1.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.adrienplazas.Metronome"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.adrienplazas.Metronome"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-metronome",]
appstream_xml_url = "https://gitlab.gnome.org/World/metronome/-/raw/master/data/com.adrienplazas.Metronome.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Metronome beats the rhythm for you, you simply need to tell it the required time signature and beats per minutes. [Source](https://gitlab.gnome.org/World/metronome)
