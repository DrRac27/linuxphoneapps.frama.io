+++
title = "Pamac"
description = "Graphical Package Manager for Manjaro Linux with Alpm, AUR, Appstream, Flatpak and Snap support"
aliases = []
date = 2021-04-18
updated = 2023-04-26

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "applications",]
categories = [ "app store",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Settings", "PackageManager",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.manjaro.org/applications/pamac"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.manjaro.org/applications/pamac"
screenshots = [ "https://fosstodon.org/@linmob/106086019748787319", "https://twitter.com/ManjaroLinux/status/1383498747464753153",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.manjaro.pamac.manager"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "pamac",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "linmob"
+++



### Notice

pamac 11 will be based on GTK4.
