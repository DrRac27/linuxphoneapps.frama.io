+++
title = "Paper"
description = "A pretty note-taking app for Gnome"
aliases = []
date = 2022-05-31
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Zagura",]
categories = [ "note taking",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_22_11",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "TextEditor",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/posidon_software/paper"
homepage = "https://posidon.io/paper/"
bugtracker = "https://gitlab.com/posidon_software/paper/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/posidon_software/paper"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.posidon.Paper"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.posidon.Paper"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "paper-note",]
appstream_xml_url = "https://gitlab.com/posidon_software/paper/-/raw/main/data/app.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Create notebooks, take notes in markdown. Features: - Almost WYSIWYG markdown rendering, - Searchable through GNOME search, - Highlight & Strikethrough text formatting, - App recoloring based on notebook color, - Trash can [Source](https://gitlab.com/posidon_software/paper/-/blob/main/data/app.metainfo.xml.in)
