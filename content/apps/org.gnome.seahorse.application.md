+++
title = "Seahorse"
description = "A password and encryption key manager for GNOME."
aliases = []
date = 2021-06-15
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later", "LGPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "The GNOME Project",]
categories = [ "key management",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C", "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/seahorse"
homepage = "https://wiki.gnome.org/Apps/Seahorse/"
bugtracker = "https://gitlab.gnome.org/GNOME/seahorse/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/seahorse"
screenshots = [ "https://twitter.com/linuxphoneapps/status/1404839330712113158",]
screenshots_img = []
all_features_touch = false
intended_for_mobile = ""
app_id = "org.gnome.seahorse.Application"
scale_to_fit = "seahorse"
flathub = "https://flathub.org/apps/org.gnome.seahorse.Application"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "seahorse",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/seahorse/-/raw/main/data/org.gnome.seahorse.Application.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

With seahorse you can create and manage PGP keys, create and manage SSH keys, publish and retrieve keys from key servers, cache your passphrase so you don't have to keep typing it and backup your keys and keyring. [Source](https://flathub.org/apps/org.gnome.seahorse.Application)


### Notice

Almost fits the screen perfectly with release 40, just the "Find Remote Keys" and the "Preferences" page don't work in portrait, but are fine in landscape.
