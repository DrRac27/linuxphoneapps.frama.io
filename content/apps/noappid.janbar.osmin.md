+++
title = "Osmin"
description = "GPS Navigator On-Road/Off-Road for Android, Sailfish and Linux phones"
aliases = []
date = 2022-01-13
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later", "LGPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Jean-Luc Barriere",]
categories = [ "maps and navigation",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick",]
backends = [ "libosmscout",]
services = [ "openstreetmap",]
packaged_in = [ "alpine_3_18", "alpine_edge", "aur",]
freedesktop_categories = [ "Qt", "Utility", "Maps",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/janbar/osmin"
homepage = ""
bugtracker = "https://github.com/janbar/osmin/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/janbar/osmin"
screenshots = [ "https://raw.githubusercontent.com/janbar/osmin/master/screenshots/informations.png", "https://raw.githubusercontent.com/janbar/osmin/master/screenshots/routing.png", "https://raw.githubusercontent.com/janbar/osmin/master/screenshots/tracking.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "osmin",]
appstream_xml_url = "https://raw.githubusercontent.com/janbar/osmin/master/osmin.appdata.xml.in"
reported_by = "Karry"
updated_by = "script"
+++




### Description

Strongly inspired by osmscout by Karry, it uses a fork of libosmscout as backend. It includes features such as generic compass, tracker, GPX reader/writer, road router, POI database. [Source](https://github.com/janbar/osmin)


### Notice

It's packaged in Manjaro. The PKGBUILD (https://gitlab.manjaro.org/manjaro-arm/packages/community/osmin/-/raw/master/PKGBUILD) should also work for other Arch-based distributions.
