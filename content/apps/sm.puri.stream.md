+++
title = "Stream"
description = "A YouTube client written in Python"
aliases = []
date = 2021-05-08
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "todd",]
categories = [ "video player",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = [ "Invidious API",]
services = [ "YouTube",]
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "Network", "Video", "Player",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://source.puri.sm/todd/Stream"
homepage = ""
bugtracker = "https://source.puri.sm/todd/Stream/-/issues/"
donations = "https://puri.sm/fund-your-app/"
translations = ""
more_information = []
summary_source_url = "no quotation"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "sm.puri.Stream"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "purism-stream",]
appstream_xml_url = "https://source.puri.sm/todd/Stream/-/raw/master/data/sm.puri.Stream.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"
+++





### Notice

Pretty nice, will hopefully get accelerated playback soon.