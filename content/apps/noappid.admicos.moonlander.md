+++
title = "Moonlander"
description = "Just another 'fancy' Gemini client."
aliases = []
date = 2021-05-13
updated = 2022-12-23

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "admicos",]
categories = [ "gemini browser",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "aur", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Network",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]

[extra]
repository = "https://sr.ht/~admicos/moonlander/"
homepage = ""
bugtracker = "https://todo.sr.ht/~admicos/moonlander"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://sr.ht/~admicos/moonlander/"
screenshots = [ "https://sr.ht/~admicos/moonlander/",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = "moonlander"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "moonlander",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"
+++





### Notice

Alpha and sadly in maintenance mode, but runs well after scale-to-fit moonlander on.
