+++
title = "Ring-KDE"
description = "Ring-KDE is a Qt based client for the Ring daemon."
aliases = []
date = 2020-02-16
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "chat", "telephony",]
mobile_compatibility = [ "needs testing",]
status = [ "archived",]
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = [ "Jami",]
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "KDE", "Network", "Telephony", "Chat",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/network/ring-kde"
homepage = "https://elv13.wordpress.com/author/elv13/"
bugtracker = "https://invent.kde.org/network/ring-kde/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/network/ring-kde"
screenshots = [ "https://elv13.wordpress.com/author/elv13/",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.ring-kde"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "ring-kde",]
appstream_xml_url = "https://invent.kde.org/network/ring-kde/-/raw/master/data/org.kde.ring-kde.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++


