+++
title = "Loupe"
description = "View images"
aliases = []
date = 2023-04-16

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The Loupe Team",]
categories = [ "image viewer",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Graphics", "Viewer", "GTK", "GNOME",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/Incubator/loupe"
homepage = "https://gitlab.gnome.org/Incubator/loupe"
bugtracker = "https://gitlab.gnome.org/Incubator/loupe/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = ""
screenshots = [ "https://gitlab.gnome.org/Incubator/loupe/uploads/863131c1292cb9f1b32fbef39f266bcf/image.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.loupe/1.png", "https://img.linuxphoneapps.org/org.gnome.loupe/2.png", "https://img.linuxphoneapps.org/org.gnome.loupe/3.png", "https://img.linuxphoneapps.org/org.gnome.loupe/4.png", "https://img.linuxphoneapps.org/org.gnome.loupe/5.png",]
all_features_touch = ""
intended_for_mobile = 1
app_id = "org.gnome.Loupe.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Loupe"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "loupe",]
appstream_xml_url = "https://gitlab.gnome.org/Incubator/loupe/-/raw/main/data/org.gnome.Loupe.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = ""
+++



### Description

A simple image viewer which allows you to browse through your images and inspect their metadata.

The following features are not available yet

*   Applying image color profiles
*   Printing

[Source](https://gitlab.gnome.org/Incubator/loupe/-/raw/main/data/org.gnome.Loupe.metainfo.xml.in.in)

### Notice

The image metadata info menu is not fully shown in portrait orientation and can not be disabled - just use it in landscape.

