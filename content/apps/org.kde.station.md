+++
title = "Station"
description = "This control is based on QMLTermWidget and currently it only works for GNU Linux. Android support might come later on."
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "maui",]
categories = [ "terminal emulator",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "MauiKit", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "manjaro_stable", "manjaro_unstable", "nix_unstable",]
freedesktop_categories = [ "Qt", "System", "TerminalEmulator",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/maui/station"
homepage = "https://mauikit.org/apps/station/"
bugtracker = "https://invent.kde.org/maui/station/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://medium.com/@temisclopeolimac/mauis-monthly-c39bbb2de9d0"
screenshots = [ "https://medium.com/@temisclopeolimac/mauis-monthly-c39bbb2de9d0", "https://medium.com/nitrux/maui-kde-fcdc920138e2",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.station"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "maui-station", "station-terminal-emulator",]
appstream_xml_url = "https://invent.kde.org/maui/station/-/raw/master/org.kde.station.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++


