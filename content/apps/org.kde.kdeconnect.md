+++
title = "KDE Connect"
description = "KDE Connect is a multi-platform app that allows your devices to communicate (eg: your phone and your computer)."
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "KDE Community",]
categories = [ "connectivity",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = [ "KDE Connect",]
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Network", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/network/kdeconnect-kde"
homepage = "https://kdeconnect.kde.org/"
bugtracker = "https://invent.kde.org/network/kdeconnect-kde/-/issues/"
donations = ""
translations = ""
more_information = [ "https://invent.kde.org/network/kdeconnect-android",]
summary_source_url = "https://invent.kde.org/network/kdeconnect-kde"
screenshots = [ "https://nicolasfella.wordpress.com/2018/06/07/kde-connect-on-plasma-mobile/",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.kdeconnect"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "kdeconnect",]
appstream_xml_url = "https://invent.kde.org/network/kdeconnect-kde/-/raw/master/data/org.kde.kdeconnect.metainfo.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++


