+++
title = "Bible"
description = "Bible app that supports multiple translations and languages"
aliases = []
date = 2020-08-25
updated = 2022-12-19

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "FSFAP",]
app_author = [ "Lugsole",]
categories = [ "bible",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Education", "Spirituality",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/Lugsole/net.lugsole.bible_gui"
homepage = "https://lugsole.net/programs/bible/"
bugtracker = "https://github.com/Lugsole/net.lugsole.bible_gui/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/net.lugsole.bible_gui"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/net.lugsole.bible_gui/1.png", "https://img.linuxphoneapps.org/net.lugsole.bible_gui/2.png", "https://img.linuxphoneapps.org/net.lugsole.bible_gui/3.png", "https://img.linuxphoneapps.org/net.lugsole.bible_gui/4.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "net.lugsole.bible_gui"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.lugsole.bible_gui"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/Lugsole/net.lugsole.bible_gui/master/data/net.lugsole.bible_gui.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++


### Description

This is a basic bible app that supports many different translations. This app supports multple different bible file formats. - SoftProjector - spb, - MyBible - SQLite3, - tsv, - Zefania XML bible - xml [Source](https://flathub.org/apps/net.lugsole.bible_gui)


### Notice

Used to be GTK3/libhandy in 0.1.2 and earlier.
