+++
title = "GNOME Contacts"
description = "A contacts manager for GNOME"
aliases = []
date = 2019-02-16
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "contacts",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Office", "ContactManagement",]
programming_languages = [ "Vala", "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-contacts"
homepage = "https://wiki.gnome.org/Apps/Contacts"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-contacts/-/issues/"
donations = "http://www.gnome.org/friends/"
translations = ""
more_information = [ "https://apps.gnome.org/app/org.gnome.Contacts/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-contacts"
screenshots = [ "https://static.gnome.org/appdata/gnome-43/contacts/contacts-edit.png", "https://static.gnome.org/appdata/gnome-43/contacts/contacts-empty.png", "https://static.gnome.org/appdata/gnome-43/contacts/contacts-filled.png", "https://static.gnome.org/appdata/gnome-43/contacts/contacts-selection.png", "https://static.gnome.org/appdata/gnome-43/contacts/contacts-setup.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Contacts"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Contacts"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-contacts",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-contacts/-/raw/main/data/org.gnome.Contacts.appdata.xml.in.in"
reported_by = "cahfofpai"
updated_by = "script"
+++




### Description

Contacts organizes your contacts information from all your online and offline sources, providing a centralized place for managing your contacts. [Source](https://gitlab.gnome.org/GNOME/gnome-contacts)


### Notice

Used GTK3/libhandy before 42, supports .vcf import since 43.
