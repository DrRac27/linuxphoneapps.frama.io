+++
title = "Iridium"
description = "Friendly IRC Client"
aliases = []
date = 2022-06-29
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Andrew Vojak",]
categories = [ "chat",]
mobile_compatibility = [ "4",]
status = [ "mature",]
frameworks = [ "GTK3", "libhandy", "granite",]
backends = []
services = [ "IRC",]
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "Network", "Chat",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/avojak/iridium"
homepage = ""
bugtracker = "https://github.com/avojak/iridium/issues/"
donations = "https://liberapay.com/avojak"
translations = ""
more_information = []
summary_source_url = "https://appcenter.elementary.io/com.github.avojak.iridium/"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/com.github.avojak.iridium/1.png", "https://img.linuxphoneapps.org/com.github.avojak.iridium/2.png", "https://img.linuxphoneapps.org/com.github.avojak.iridium/3.png", "https://img.linuxphoneapps.org/com.github.avojak.iridium/4.png", "https://img.linuxphoneapps.org/com.github.avojak.iridium/5.png", "https://img.linuxphoneapps.org/com.github.avojak.iridium/6.png", "https://img.linuxphoneapps.org/com.github.avojak.iridium/7.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.avojak.iridium"
scale_to_fit = "com.github.avojak.iridium"
flathub = ""
flatpak_link = "https://appcenter.elementary.io/com.github.avojak.iridium/"
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "elementary-iridium",]
appstream_xml_url = "https://raw.githubusercontent.com/avojak/iridium/master/data/com.github.avojak.iridium.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Iridium is a native Linux IRC client built in Vala and Gtk for elementary OS. [Source](https://appcenter.elementary.io/com.github.avojak.iridium/)


### Notice

The main screens work perfectly, but settings work better with scale-to-fit (although they can be somewhat usable without).