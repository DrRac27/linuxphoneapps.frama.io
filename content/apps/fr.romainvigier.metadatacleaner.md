+++
title = "Metadata Cleaner"
description = "Python GTK application to view and clean metadata in files, using mat2."
aliases = []
date = 2021-01-04
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Romain Vigier",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "flathub", "nix_stable_22_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "System", "FileTools",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/rmnvgr/metadata-cleaner"
homepage = ""
bugtracker = "https://gitlab.com/rmnvgr/metadata-cleaner/-/issues/"
donations = ""
translations = ""
more_information = [ "https://apps.gnome.org/app/fr.romainvigier.MetadataCleaner/",]
summary_source_url = "https://gitlab.com/rmnvgr/metadata-cleaner"
screenshots = [ "https://wiki.mobian-project.org/doku.php?id=metadata-cleaner",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "fr.romainvigier.MetadataCleaner"
scale_to_fit = ""
flathub = "https://flathub.org/apps/fr.romainvigier.MetadataCleaner"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "metadata-cleaner", "python:metadata-cleaner",]
appstream_xml_url = "https://gitlab.com/rmnvgr/metadata-cleaner/-/raw/main/application/data/fr.romainvigier.MetadataCleaner.metainfo.xml"
reported_by = "linmob"
updated_by = "script"
+++





### Notice

AUR build works fine.
