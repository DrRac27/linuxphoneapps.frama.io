+++
title = "Telegram Desktop"
description = "Telegram Desktop messaging app"
aliases = []
date = 2019-02-16
updated = 2023-04-23

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "John Preston",]
categories = [ "chat",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "QtWidgets",]
backends = []
services = [ "Telegram",]
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "Network", "Chat", "InstantMessaging",]
programming_languages = [ "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/telegramdesktop/tdesktop"
homepage = "https://desktop.telegram.org/"
bugtracker = "https://github.com/telegramdesktop/tdesktop/issues/"
donations = ""
translations = ""
more_information = [ "https://wiki.postmarketos.org/wiki/Telegram_Desktop", "https://wiki.mobian.org/doku.php?id=telegram-desktop",]
summary_source_url = "https://github.com/telegramdesktop/tdesktop"
screenshots = [ "https://desktop.telegram.org", "https://mastodon.technology/@kde/101557824824695088",]
screenshots_img = []
all_features_touch = 1
intended_for_mobile = ""
app_id = "org.telegram.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.telegram.desktop"
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/flathub/org.telegram.desktop/master/org.telegram.desktop.yml"
snapcraft = ""
snap_link = ""
snap_recipe = "https://raw.githubusercontent.com/telegramdesktop/tdesktop/master/snap/snapcraft.yaml"
repology = [ "telegram-desktop",]
appstream_xml_url = "https://raw.githubusercontent.com/telegramdesktop/tdesktop/dev/lib/xdg/org.telegram.desktop.metainfo.xml"
reported_by = "cahfofpai"
updated_by = "linmob"
+++




### Notice 

Needs tweaking to work well on mobile, see links under "More information" for tips.
