+++
title = "Friendiqa"
description = "Qt/QML App for Friendiqa"
aliases = []
date = 2022-03-29

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "lubuwest",]
categories = [ "social media",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "QtQuick",]
backends = []
services = [ "Friendica",]
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "Network", "Feed",]
programming_languages = [ "QML", "JavaScript", "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://git.friendi.ca/lubuwest/Friendiqa"
homepage = "https://friendiqa.ma-nic.de/"
bugtracker = "https://git.friendi.ca/lubuwest/Friendiqa/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.friendi.ca/lubuwest/Friendiqa"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "de.manic.Friendiqa"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "friendiqa",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = ""
+++




### Description

QML based client for the Friendica Social Network. Tabs for news (incl. Direct Messages), friends, photos and events. OS: currently Linux and Android (4.3 Jelly Bean, 5.1 for background sync). Source code is a QtCreator project. [Source](https://git.friendi.ca/lubuwest/Friendiqa)
