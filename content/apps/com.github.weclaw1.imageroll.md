+++
title = "Image Roll"
description = "Image Roll is a simple and fast GTK image viewer"
aliases = []
date = 2021-06-13
updated = 2022-12-19

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Robert Węcławski",]
categories = [ "image viewer",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "flathub", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Graphics", "Viewer",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]

[extra]
repository = "https://github.com/weclaw1/image-roll"
homepage = ""
bugtracker = "https://github.com/weclaw1/image-roll/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/weclaw1/image-roll"
screenshots = [ "https://twitter.com/linuxphoneapps/status/1404033241452007427",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.weclaw1.ImageRoll"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.weclaw1.ImageRoll"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "image-roll",]
appstream_xml_url = "https://raw.githubusercontent.com/weclaw1/image-roll/main/src/resources/com.github.weclaw1.ImageRoll.metainfo.xml"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Image Roll - simple and fast GTK image viewer with basic image manipulation tools. Written in Rust. [Source](https://github.com/weclaw1/image-roll)


### Notice

Nice image viewer with features like rotation and cropping. GTK3 based until the 2.0.0 release.
