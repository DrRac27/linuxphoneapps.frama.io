+++
title = "Krecorder"
description = "Audio recorder for Plasma Mobile and other platforms"
aliases = []
date = 2020-02-06
updated = 2023-04-26

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Plasma Mobile Developers",]
categories = [ "audio recorder",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "fedora_38", "fedora_rawhide", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Qt", "KDE", "Audio", "Recorder", "Utility",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/utilities/krecorder"
homepage = ""
bugtracker = "https://bugs.kde.org/describecomponents.cgi?product=krecorder"
donations = ""
translations = ""
more_information = [ "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/#recorder",]
summary_source_url = "https://invent.kde.org/utilities/krecorder"
screenshots = [ "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/krecorder-1.png", "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/krecorder-2.png", "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/krecorder-3.png", "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/krecorder-4.png",]
screenshots_img = []
all_features_touch = 1
intended_for_mobile = 1
app_id = "org.kde.krecorder"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://invent.kde.org/utilities/krecorder/-/raw/master/.flatpak-manifest.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "krecorder",]
appstream_xml_url = "https://invent.kde.org/utilities/krecorder/-/raw/master/org.kde.krecorder.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++




### Description

A convergent audio recording application for Plasma. Features: Record audio with a visualizer, and pausing functionality, Ability to select audio sources, Ability to select encoding and container formats, Audio playback with a visualizer. [Source](https://invent.kde.org/utilities/krecorder)
