+++
title = "Lingot"
description = "LINGOT is a musical instrument tuner."
aliases = []
date = 2020-10-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = []
app_author = [ "ibancg",]
categories = [ "musical tool",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "gentoo", "gnuguix", "nix_stable_22_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Education", "Music",]
programming_languages = [ "C",]
build_systems = [ "make",]

[extra]
repository = "https://github.com/ibancg/lingot"
homepage = ""
bugtracker = "https://github.com/ibancg/lingot/issues/"
donations = ""
translations = ""
more_information = [ "https://wiki.mobian-project.org/doku.php?id=lingot",]
summary_source_url = "https://github.com/ibancg/lingot"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.nongnu.lingot"
scale_to_fit = "lingot"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "lingot",]
appstream_xml_url = "https://raw.githubusercontent.com/ibancg/lingot/master/org.nongnu.lingot.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++

