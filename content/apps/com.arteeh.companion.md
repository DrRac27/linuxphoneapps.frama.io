+++
title = "Wasp Companion"
description = "Companion app for wasp-os"
aliases = []
date = 2021-07-18
updated = 2022-12-19

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "Maarten de Jong",]
categories = [ "watch companion",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "custom",]

[extra]
repository = "https://github.com/Siroj42/wasp-companion"
homepage = ""
bugtracker = "https://github.com/Siroj42/wasp-companion/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/Siroj42/wasp-companion"
screenshots = [ "https://twitter.com/linuxphoneapps/status/1416875589038923780",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.arteeh.Companion"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://raw.githubusercontent.com/arteeh/wasp-companion/master/flatpak/com.arteeh.Companion.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++


### Description

This is a Linux companion app for wasp-os, a smartwatch operating system. It's written in Python with GTK and Libhandy. [Source](https://github.com/Siroj42/wasp-companion)


### Notice

WIP, did not work with my WaspOS PineTime in my short, limited-depth testing. Builds as a flatpak.