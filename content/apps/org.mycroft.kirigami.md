+++
title = "Mycroft"
description = "Mycroft Kirigami Application for Plasma Mobile"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "aiix",]
categories = [ "voice assistant",]
mobile_compatibility = [ "5",]
status = [ "inactive",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "Qt", "Utility",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/AIIX/mycroft-kirigami-application"
homepage = ""
bugtracker = "https://github.com/AIIX/mycroft-kirigami-application/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/AIIX/mycroft-kirigami-application"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.mycroft.kirigami"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "mycroft", "plasma-mycroft", "mycroft-gui",]
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "script"
+++





### Notice

Last commit September 2018.
