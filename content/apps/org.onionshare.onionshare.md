+++
title = "OnionShare"
description = "Securely and anonymously share files, host websites, and chat with friends using the Tor network"
aliases = []
date = 2020-11-28
updated = 2022-12-23

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Micah Lee",]
categories = [ "file transfer", "chat",]
mobile_compatibility = [ "3",]
status = []
frameworks = [ "QtQuick",]
backends = []
services = [ "tor",]
packaged_in = [ "archlinuxarm_aarch64", "archlinuxarm_armv7h", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "Network", "FileTransfer", "P2P",]
programming_languages = [ "Python",]
build_systems = [ "custom",]

[extra]
repository = "https://github.com/onionshare/onionshare"
homepage = "https://onionshare.org/"
bugtracker = "https://github.com/onionshare/onionshare/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/onionshare/onionshare"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.onionshare.OnionShare"
scale_to_fit = "python3"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "onionshare",]
appstream_xml_url = "https://raw.githubusercontent.com/onionshare/onionshare/main/desktop/org.onionshare.OnionShare.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++





### Notice

Scaling is almost fine with scale-to-fit, drag and drop is tough to unachievable.