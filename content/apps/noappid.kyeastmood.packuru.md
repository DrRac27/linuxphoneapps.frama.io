+++
title = "Packuru"
description = "Archive manager written in C++/Qt"
aliases = []
date = 2020-11-28
updated = 2022-12-19

[taxonomies]
project_licenses = [ "BSD 2-Clause", "BSD Zero Clause", "MIT",]
metadata_licenses = []
app_author = [ "kyeastmood",]
categories = [ "file management",]
mobile_compatibility = [ "needs testing",]
status = []
frameworks = [ "Kirigami", "QtQuick",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "Utility", "Archiving",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "qmake",]

[extra]
repository = "https://gitlab.com/kyeastmood/packuru"
homepage = ""
bugtracker = "https://gitlab.com/kyeastmood/packuru/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/kyeastmood/packuru"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://gitlab.com/kyeastmood/packuru/-/raw/master/integration/freedesktop.org/org.packuru.PackuruMobile.metainfo.xml"
reported_by = "linmob"
updated_by = "script"

+++
