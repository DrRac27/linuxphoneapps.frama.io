+++
title = "Megapixels"
description = "The GTK camera application"
aliases = []
date = 2020-09-12
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "postmarketOS Developers",]
categories = [ "camera",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "nix_stable_22_11", "nix_unstable", "postmarketos_22.12", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Graphics", "Photography",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/postmarketOS/megapixels"
homepage = ""
bugtracker = "https://gitlab.com/postmarketOS/megapixels/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/postmarketOS/megapixels"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.postmarketos.Megapixels"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "megapixels", "megapixels-gtk3",]
appstream_xml_url = "https://gitlab.com/postmarketOS/megapixels/-/raw/master/data/org.postmarketos.Megapixels.metainfo.xml"
reported_by = "linmob"
updated_by = "script"
+++




### Description

A GTK4 camera application that knows how to deal with the media request api. It uses opengl to debayer the raw sensor data for the preview. [Source](https://gitlab.com/postmarketOS/megapixels)


### Notice

GTK3 branch is still available. Megapixels also scans QR codes.