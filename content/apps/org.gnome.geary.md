+++
title = "Geary"
description = "Geary is an email application built around conversations, for the GNOME 3 desktop."
aliases = []
date = 2020-08-25
updated = 2022-12-19

[taxonomies]
project_licenses = [ "LGPL-2.1-or-later",]
metadata_licenses = []
app_author = [ "Geary Development Team",]
categories = [ "email",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Office", "Email",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/geary"
homepage = "https://wiki.gnome.org/Apps/Geary"
bugtracker = "https://gitlab.gnome.org/GNOME/geary/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/geary"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Geary"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Geary"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "geary",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/geary/-/raw/mainline/desktop/org.gnome.Geary.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++


