+++
title = "Lord Almighty's Modern Bible (LAMB)"
description = "Lord Almighty's Modern Bible (LAMB) is a cross platform Bible application."
aliases = []
date = 2021-01-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "the3dman",]
categories = [ "bible",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "Education", "Spirituality",]
programming_languages = [ "HTML", "QML", "Cpp",]
build_systems = [ "qmake",]

[extra]
repository = "https://gitlab.com/The3DmaN/lord-almightys-modern-bible"
homepage = ""
bugtracker = "https://gitlab.com/The3DmaN/lord-almightys-modern-bible/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/The3DmaN/lord-almightys-modern-bible"
screenshots = [ "https://gitlab.com/The3DmaN/lord-almightys-modern-bible/-/raw/master/img/lightmode1.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "lord-almightys-modern-bible",]
appstream_xml_url = ""
reported_by = "The3DmaN"
updated_by = "script"
+++




### Description

Lord Almighty's Modern Bible (LAMB) is a cross platform Bible application. It was designed with Linux Mobile in mind and provides an offline copy of the Bible in an elegant look and feel. This app was developed to spread scripture to the security minded individuals that need or prefer to read the bible offline for various reasons. The hope is that this app will help to bring scripture to many people across the world who may not have access to stable internet, high-end devices or a safe place to read it online. The Bible text and glossary in this app are based on the 2020 edition of the World English Bible. Neither the scripture nor the translations in Lord Almighty's Modern Bible have been modified from the World English Bible, however, there were major modifications made to the footnotes display and some text from the WEB version. Each footnote has been reviewed for context and placement. They have been put into a more modern and mobile compatable tooltip popup. The word/words/phrase that they apply to have also been underlined. Quotes from other sections of the Bible have also been moved into popups to prevent the reader from having to skip around. This I hope will help the reader gain a better understanding of what they are reading. [Source](https://gitlab.com/The3DmaN/lord-almightys-modern-bible)
