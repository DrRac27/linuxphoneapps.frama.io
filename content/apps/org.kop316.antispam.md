+++
title = "Phosh Antispam"
description = "Phosh Antispam is a program that monitors Gnome calls and automatically hangs up depending on the user's preferences."
aliases = []
date = 2021-10-25
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Chris Talbot",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "debian_12", "debian_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/kop316/phosh-antispam"
homepage = ""
bugtracker = "https://gitlab.com/kop316/phosh-antispam/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/kop316/phosh-antispam"
screenshots = [ "https://gitlab.com/kop316/phosh-antispam/-/raw/master/data/metainfo/screenshot.png?inline=false",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kop316.antispam"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "phosh-antispam",]
appstream_xml_url = "https://gitlab.com/kop316/phosh-antispam/-/raw/master/data/metainfo/org.kop316.antispam.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"
+++





### Notice

Read the README for the required version Gnome Calls needed to make this work.