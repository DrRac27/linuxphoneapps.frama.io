+++
title = "Wordbook"
description = "Lookup definitions for any English term"
aliases = [ "apps/com.github.fushinari.wordbook",]
date = 2021-03-12
updated = "2023-04-23"

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Mufeed Ali",]
categories = [ "dictionary",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4",]
backends = []
services = []
packaged_in = [ "aur", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "TextTools", "Dictionary",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/mufeedali/Wordbook"
homepage = "https://github.com/mufeedali/Wordbook"
bugtracker = "https://github.com/mufeedali/Wordbook/issues"
donations = "https://liberapay.com/fushinari/donate"
translations = ""
more_information = []
summary_source_url = "https://github.com/mufeedali/Wordbook"
screenshots = [ "https://raw.githubusercontent.com/fushinari/Wordbook/main/images/ss.png", "https://raw.githubusercontent.com/fushinari/Wordbook/main/images/ss1.png", "https://raw.githubusercontent.com/fushinari/Wordbook/main/images/ss2.png", "https://raw.githubusercontent.com/fushinari/Wordbook/main/images/ss3.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = 1
app_id = "com.github.fushinari.Wordbook"
scale_to_fit = ""
flathub = "https://flathub.org/apps/details/com.github.fushinari.Wordbook"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "wordbook",]
appstream_xml_url = "https://raw.githubusercontent.com/mufeedali/Wordbook/main/data/dev.mufeed.Wordbook.metainfo.xml.in"
reported_by = "linmob"
updated_by = "linmob"
+++



### Description

**Wordbook** is an offline English-English dictionary application built for GNOME using the [Open English WordNet](https://github.com/globalwordnet/english-wordnet) database for definitions and the reliable eSpeak for pronunciations (both audio and phoneme).

Features

*   Fully offline after initial data download
*   Random Word
*   Live Search
*   Double click to search
*   Custom Definitions feature using Pango Markup or an HTML subset for formatting
*   Support for GNOME Dark Mode and launching app in dark mode.

[Source](https://github.com/mufeedali/Wordbook)

### Notice 

GTK3/libadwaita until 0.2.0, GTK 4 since, will also use libadwaita with next release.
