+++
title = "GNOME Text Editor"
description = "Edit text files"
aliases = []
date = 2023-01-22

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Christian Hergert",]
categories = [ "text editor",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_12", "debian_unstable", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "TextEditor",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-text-editor"
homepage = ""
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-text-editor/-/issues"
donations = "http://www.gnome.org/friends/"
translations = "https://wiki.gnome.org/TranslationProject"
more_information = [ "https://apps.gnome.org/app/org.gnome.TextEditor/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-text-editor/-/raw/main/data/org.gnome.TextEditor.appdata.xml.in.in"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.text-editor/1.png", "https://img.linuxphoneapps.org/org.gnome.text-editor/2.png", "https://img.linuxphoneapps.org/org.gnome.text-editor/3.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.TextEditor"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.TextEditor"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-text-editor",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-text-editor/-/raw/main/data/org.gnome.TextEditor.appdata.xml.in.in"
reported_by = "linmob"
updated_by = ""
+++




### Description

GNOME Text Editor is a simple text editor focused on a pleasing default experience. [Source](https://gitlab.gnome.org/GNOME/gnome-text-editor/-/raw/main/data/org.gnome.TextEditor.appdata.xml.in.in)


### Notice

Mobile friendly since release 43
