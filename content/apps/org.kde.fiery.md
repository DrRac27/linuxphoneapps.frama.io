+++
title = "Fiery"
description = "A convergent web browser"
aliases = []
date = 2021-06-12
updated = 2022-12-23

[taxonomies]
project_licenses = [ "LGPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "maui",]
categories = [ "web browser",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "MauiKit", "Kirigami",]
backends = [ "QtWebEngine",]
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Network", "Qt", "WebBrowser",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/maui/fiery"
homepage = "https://mauikit.org/"
bugtracker = "https://invent.kde.org/maui/fiery/-/issues"
donations = "https://kde.org/community/donations/"
translations = ""
more_information = [ "https://www.youtube.com/watch?v=JS8qZwKoMFk?t=515", "https://mauikit.org/blog/maui-2-2-0-release/",]
summary_source_url = "https://invent.kde.org/maui/fiery"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.fiery"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "maui-sol",]
appstream_xml_url = "https://invent.kde.org/maui/fiery/-/raw/master/org.kde.fiery.metainfo.xml"
reported_by = "linmob"
updated_by = "script"
+++





### Notice

Used to be called Sol before.
