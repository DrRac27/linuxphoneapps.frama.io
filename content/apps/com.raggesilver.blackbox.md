+++
title = "BlackBox"
description = "A beautiful GTK 4 terminal."
aliases = []
date = 2022-06-30
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Paulo Queiroz",]
categories = [ "terminal emulator",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_edge", "aur", "debian_unstable", "flathub", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "System", "TerminalEmulator",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/raggesilver/blackbox"
homepage = ""
bugtracker = "https://gitlab.gnome.org/raggesilver/blackbox/-/issues/"
donations = "https://www.patreon.com/raggesilver"
translations = "https://gitlab.gnome.org/raggesilver/blackbox/-/issues"
more_information = []
summary_source_url = "https://gitlab.gnome.org/raggesilver/blackbox/"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.raggesilver.BlackBox"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.raggesilver.BlackBox"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "blackbox-terminal",]
appstream_xml_url = "https://gitlab.gnome.org/raggesilver/blackbox/-/raw/main/data/com.raggesilver.BlackBox.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Black Box is a native terminal emulator for GNOME that offers superb theming options. [Source](https://gitlab.gnome.org/raggesilver/blackbox/-/raw/main/data/com.raggesilver.BlackBox.appdata.xml.in)
