+++
title = "Fairy Tale"
description = "Read and manage your comics collection."
aliases = []
date = 2020-10-11
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Bilal Elmoussaoui",]
categories = [ "document viewer",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Graphics", "Viewer",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/bilelmoussaoui/fairytale"
homepage = ""
bugtracker = "https://gitlab.gnome.org/bilelmoussaoui/fairytale/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/bilelmoussaoui/fairytale"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.belmoussaoui.FairyTale"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://gitlab.gnome.org/bilelmoussaoui/fairytale/-/raw/master/data/com.belmoussaoui.FairyTale.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"

+++