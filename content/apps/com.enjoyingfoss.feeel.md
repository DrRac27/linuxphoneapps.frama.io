+++
title = "Feeel"
description = "A cross-platform Flutter home workout app that respects your privacy"
aliases = []
date = 2023-01-07

[taxonomies]
project_licenses = [ "AGPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Enjoying FOSS",]
categories = [ "fitness",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Flutter",]
backends = []
services = []
packaged_in = [ "Flathub",]
freedesktop_categories = [ "GTK", "Utility",]
programming_languages = [ "Dart",]
build_systems = [ "flutter",]

[extra]
repository = "https://gitlab.com/enjoyingfoss/feeel"
homepage = ""
bugtracker = "https://gitlab.com/enjoyingfoss/feeel/-/issues"
donations = "https://liberapay.com/Feeel/donate"
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/enjoyingfoss/feeel"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/com.enjoyingfoss.feeel/1.png", "https://img.linuxphoneapps.org/com.enjoyingfoss.feeel/2.png", "https://img.linuxphoneapps.org/com.enjoyingfoss.feeel/3.png", "https://img.linuxphoneapps.org/com.enjoyingfoss.feeel/4.png", "https://img.linuxphoneapps.org/com.enjoyingfoss.feeel/5.png", "https://img.linuxphoneapps.org/com.enjoyingfoss.feeel/6.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.enjoyingfoss.feeel"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.enjoyingfoss.feeel"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://gitlab.com/enjoyingfoss/feeel/-/raw/master/flatpak/com.enjoyingfoss.Feeel.appdata.xml"
reported_by = "linmob"
updated_by = ""

+++


### Description

Feeel is an open-source app for doing simple at-home exercises. It holds the acclaimed full body scientific 7-minute workout regime and allows creating custom workouts as well. While the app currently contains a limited amount of exercises, the plan is to drastically expand the number of both exercises and workouts with the help of the community. [Source](https://gitlab.com/enjoyingfoss/feeel/-/raw/master/flatpak/com.enjoyingfoss.Feeel.appdata.xml)
