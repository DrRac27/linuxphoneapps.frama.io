+++
title = "YouPlay"
description = "Search, download and play music from YouTube."
aliases = [ "apps/noappid.ralfhersel.youplay/",]
date = 2020-12-12
updated = 2023-03-25

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "ralfhersel",]
categories = [ "music player",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "yt-dlp", "mpv",]
services = [ "YouTube",]
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "Audio", "Network", "Player",]
programming_languages = [ "Python",]
build_systems = [ "none",]

[extra]
repository = "https://codeberg.org/ralfhersel/youplay"
homepage = ""
bugtracker = "https://codeberg.org/ralfhersel/youplay/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://codeberg.org/ralfhersel/youplay"
screenshots = [ "https://twitter.com/linmobblog/status/1340339816446222336#m",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.codeberg.ralfhersel.youplay"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "youplay",]
appstream_xml_url = "https://codeberg.org/ralfhersel/youplay/raw/branch/main/org.codeberg.ralfhersel.youplay.metainfo.xml"
reported_by = "linmob"
updated_by = "linmob"
+++

