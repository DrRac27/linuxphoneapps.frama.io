+++
title = "virtboard"
description = "A basic keyboard, blazing the path of modern Wayland keyboards. Sacrificial."
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "X11",]
metadata_licenses = []
app_author = [ "Purism",]
categories = [ "virtual keyboard",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Cairo",]
backends = []
services = []
packaged_in = [ "aur", "pureos_landing",]
freedesktop_categories = [ "Utility", "Accessibility",]
programming_languages = [ "C", "Objective-C",]
build_systems = [ "meson",]

[extra]
repository = "https://source.puri.sm/Librem5/virtboard"
homepage = ""
bugtracker = "https://source.puri.sm/Librem5/virtboard/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://source.puri.sm/Librem5/virtboard"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "sm.puri.OSK0"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "virtboard",]
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "script"
+++





### Notice

replaced with squeekboard