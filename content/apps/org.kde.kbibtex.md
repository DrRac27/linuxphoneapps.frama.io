+++
title = "KBibTeX (Kirigami branch)"
description = "KBibTeX is a bibliography editor (BibTeX and somewhat BibLaTex) used in conjunction with LaTeX and friends"
aliases = []
date = 2019-10-22
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = []
app_author = [ "feature",]
categories = [ "bibliography editor",]
mobile_compatibility = [ "5",]
status = [ "inactive",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/office/kbibtex/-/tree/feature/kirigami"
homepage = ""
bugtracker = "https://invent.kde.org/office/kbibtex/-/tree/feature/kirigami/-/issues/"
donations = ""
translations = ""
more_information = [ "https://t-fischer.dreamwidth.org/9049.html", "https://invent.kde.org/office/kbibtex/-/commit/fd6dc23fab199e769da5e12df5ef99a4d8c9268f",]
summary_source_url = "https://t-fischer.dreamwidth.org/9049.html"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.kbibtex"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "kbibtex",]
appstream_xml_url = "https://invent.kde.org/office/kbibtex/-/raw/master/src/program/org.kde.kbibtex.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++




### Description

This commit is the first implementation of a very basic KBibTeX implementation in Kirigami2. It is modelled after the SailfishOS implementation, but far from being on par feature-wise. There are deficits design-wise which require more expertise in QML and Kirigami in order to be fixed. [Source](https://invent.kde.org/office/kbibtex/-/commit/fd6dc23fab199e769da5e12df5ef99a4d8c9268f)


### Notice

https://invent.kde.org/office/kbibtex/-/tree/master/mobile/sailfishos Sailfish GUI
