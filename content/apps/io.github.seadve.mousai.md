+++
title = "Mousai"
description = "Simple application for identifying songs"
aliases = []
date = 2021-03-31
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Dave Patrick Caberto",]
categories = [ "multimedia",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = [ "audd.io",]
services = []
packaged_in = [ "aur", "flathub", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Audio",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/SeaDve/Mousai"
homepage = ""
bugtracker = "https://github.com/SeaDve/Mousai/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/SeaDve/Mousai"
screenshots = [ "https://fosstodon.org/@linmob/105984721440681060", "https://github.com/SeaDve/Mousai",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.github.seadve.Mousai"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.seadve.Mousai"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "mousai",]
appstream_xml_url = "https://raw.githubusercontent.com/SeaDve/Mousai/main/data/io.github.seadve.Mousai.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++


