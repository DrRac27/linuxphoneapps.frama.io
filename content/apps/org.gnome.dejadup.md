+++
title = "Déjà Dup Backups"
description = "Protect yourself from data loss"
aliases = []
date = 2022-03-06
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Michael Terry",]
categories = [ "system utilities", "backup",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "duplicity",]
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Archiving",]
programming_languages = [ "Vala", "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/World/deja-dup"
homepage = "https://wiki.gnome.org/Apps/DejaDup"
bugtracker = "https://gitlab.gnome.org/World/deja-dup/-/issues/"
donations = "https://wiki.gnome.org/Apps/DejaDup/Donate"
translations = "https://l10n.gnome.org/module/deja-dup/"
more_information = []
summary_source_url = "https://gitlab.gnome.org/World/deja-dup"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.DejaDup"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.DejaDup"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "deja-dup",]
appstream_xml_url = "https://gitlab.gnome.org/World/deja-dup/-/raw/main/data/app.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Déjà Dup is a simple backup tool. It hides the complexity of backing up the Right Way (encrypted, off-site, and regular) and uses duplicity as the backend. [Source](https://gitlab.gnome.org/World/deja-dup)


### Notice

Mobile ready since release 43. For earlier GTK3 based releases, Purism had created a mobile friendly downstream: https://source.puri.sm/Librem5/deja-dup
