+++
title = "Fossil"
description = "A simple GTK Gopher/Gemini client written in Vala"
aliases = []
date = 2021-05-13
updated = 2022-12-19

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "koyuspace",]
categories = [ "gemini browser", "gopher browser",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "GNOME", "Network",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/koyuspace/fossil"
homepage = ""
bugtracker = "https://github.com/koyuspace/fossil/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/koyuspace/fossil"
screenshots = [ "https://github.com/koyuspace/fossil", "https://twitter.com/linuxphoneapps/status/1392929056564383762",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.koyuspace.fossil"
scale_to_fit = "com.github.koyuspace.fossil"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "fossil-gemini",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"
+++





### Notice

Runs fine after scale-to-fit. It's origin/the project it has been forked from, dragonstone, scales perfectly out of the box, so I recommend it over Fossil.