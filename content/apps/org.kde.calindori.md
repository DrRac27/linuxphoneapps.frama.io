+++
title = "Calindori"
description = "Calendar for Plasma Mobile"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Plasma Mobile Developers",]
categories = [ "calendar",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Kirigami",]
backends = [ "icalendar",]
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "fedora_38", "fedora_rawhide", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Qt", "KDE", "Office", "Calendar",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/plasma-mobile/calindori"
homepage = "https://apps.kde.org/calindori/"
bugtracker = "https://invent.kde.org/plasma-mobile/calindori/-/issues/"
donations = "https://kde.org/community/donations/"
translations = "https://l10n.kde.org/stats/gui/trunk-kf5/package/calindori/"
more_information = [ "https://phabricator.kde.org/T6942",]
summary_source_url = "https://invent.kde.org/plasma-mobile/calindori"
screenshots = [ "https://phabricator.kde.org/file/data/avet245njbclujtz6fch/PHID-FILE-ztggnyj75pxxsir6golr/calindori_main.png", "https://phabricator.kde.org/file/data/gy7cakp4mpv7zk3nbjrr/PHID-FILE-ji6d5jggrhvyycggczaz/calindori_todopage.png", "https://phabricator.kde.org/file/data/kzajtylgis4c5hv37mnj/PHID-FILE-2eowzmb72xebw2lnsh6n/calindori_todoview.png", "https://phabricator.kde.org/file/data/t5w3cyzzwjdfffik5lwm/PHID-FILE-if2vua5bagjrwjg42rop/calindori_timepicker.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.kde.calindori/1-monthview.png", "https://img.linuxphoneapps.org/org.kde.calindori/2-dayview.png", "https://img.linuxphoneapps.org/org.kde.calindori/3-eventview.png", "https://img.linuxphoneapps.org/org.kde.calindori/4-taskview.png", "https://img.linuxphoneapps.org/org.kde.calindori/5-sidebar.png", "https://img.linuxphoneapps.org/org.kde.calindori/6-createevent.png", "https://img.linuxphoneapps.org/org.kde.calindori/7-timepicker.png", "https://img.linuxphoneapps.org/org.kde.calindori/8-createtask.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.calindori"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "calindori",]
appstream_xml_url = "https://invent.kde.org/plasma-mobile/calindori/-/raw/master/org.kde.calindori.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++




### Description

Calindori is a touch friendly calendar application. It has been designed for mobile devices but it can also run on desktop environments. Users of Calindori are able to check previous and future dates and manage tasks and events. When executing the application for the first time, a new calendar file is created that follows the ical standard. Alternatively, users may create additional calendars or import existing ones. [Source](https://apps.kde.org/calindori/)


### Notice

Part of the GUI will be cut off if the window is too small (e.g. when the OSK is opened), reference those upstream issues: [1](https://invent.kde.org/plasma-mobile/calindori/-/issues/19) [2](https://bugs.kde.org/show_bug.cgi?id=456406)
