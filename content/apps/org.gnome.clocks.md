+++
title = "GNOME Clocks"
description = "A simple clock application for GNOME"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = []
app_author = [ "The GNOME Project",]
categories = [ "clock",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Clock",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-clocks"
homepage = "https://wiki.gnome.org/Apps/Clocks"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-clocks/-/issues/"
donations = ""
translations = ""
more_information = [ "https://apps.gnome.org/app/org.gnome.clocks/",]
summary_source_url = "https://source.puri.sm/gusnan/gnome-clocks"
screenshots = [ "https://gitlab.gnome.org/GNOME/gnome-clocks/raw/master/data/appdata/alarm.png", "https://gitlab.gnome.org/GNOME/gnome-clocks/raw/master/data/appdata/stopwatch.png", "https://gitlab.gnome.org/GNOME/gnome-clocks/raw/master/data/appdata/timer.png", "https://gitlab.gnome.org/GNOME/gnome-clocks/raw/master/data/appdata/world.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.clocks/1.png", "https://img.linuxphoneapps.org/org.gnome.clocks/2.png", "https://img.linuxphoneapps.org/org.gnome.clocks/3.png", "https://img.linuxphoneapps.org/org.gnome.clocks/4.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Clocks"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.clocks"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-clocks",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-clocks/-/raw/master/data/org.gnome.clocks.metainfo.xml.in.in"
reported_by = "cahfofpai"
updated_by = "script"
+++




### Description

It includes world clocks, alarms, a stopwatch and a timer [Source](https://source.puri.sm/gusnan/gnome-clocks)


### Notice

Was GTK3/libhandy before 42. _Note:_ If your device uses suspend to prolong battery life, timers, stopwatch and alarms will stop working whenever the device is suspended.
