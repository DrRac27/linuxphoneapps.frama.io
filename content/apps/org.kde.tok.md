+++
title = "Tok"
description = "KDE's convergent Telegram client."
aliases = []
date = 2021-05-11
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "network",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = [ "Telegram",]
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "KDE", "Network", "InstantMessaging", "Chat",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "qbs",]

[extra]
repository = "https://invent.kde.org/network/tok"
homepage = ""
bugtracker = "https://invent.kde.org/network/tok/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/network/tok"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.Tok"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "tok",]
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "script"
+++


