+++
title = "Loop"
description = "Simple audio loop machine application to create music"
aliases = []
date = 2021-12-08
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "GNOME Developers",]
categories = [ "musical tool",]
mobile_compatibility = [ "3",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "gstreamer",]
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Audio",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/danigm/loop"
homepage = ""
bugtracker = "https://gitlab.gnome.org/danigm/loop/-/issues/"
donations = ""
translations = ""
more_information = [ "https://danigm.net/loop.html",]
summary_source_url = "https://gitlab.gnome.org/danigm/loop"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "net.danigm.loop"
scale_to_fit = "net.danigm.loop"
flathub = "https://flathub.org/apps/net.danigm.loop"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-loop",]
appstream_xml_url = "https://gitlab.gnome.org/danigm/loop/-/raw/main/data/net.danigm.loop.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++





### Notice

A bit slow on PinePhone. With release 1.0 this app does not fit the screen anymore, still works okay-ish with scale-to-fit in landscape.
