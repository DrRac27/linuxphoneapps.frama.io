+++
title = "Confy"
description = "Conferences schedule viewer"
aliases = []
date = 2020-09-28
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Fabio Comuni",]
categories = [ "conference companion",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "debian_12", "debian_unstable", "flathub", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://git.sr.ht/~fabrixxm/confy"
homepage = "https://confy.kirgroup.net/"
bugtracker = "https://todo.sr.ht/~fabrixxm/confy"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://confy.kirgroup.net/"
screenshots = [ "https://git.sr.ht/~fabrixxm/confy-website/blob/master/appdata/confy3.jpg",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "net.kirgroup.confy"
scale_to_fit = ""
flathub = "https://flathub.org/apps/net.kirgroup.confy"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "confy",]
appstream_xml_url = "https://git.sr.ht/~fabrixxm/confy/blob/master/data/net.kirgroup.confy.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"
+++


