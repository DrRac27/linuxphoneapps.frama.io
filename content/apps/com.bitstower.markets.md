+++
title = "Markets"
description = "Keep track of your investments"
aliases = []
date = 2020-08-25
updated = 2023-04-22

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Tomasz Oponowicz",]
categories = [ "stocks",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur", "fedora_38", "fedora_rawhide", "gnuguix", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "News",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/tomasz-oponowicz/markets"
homepage = "https://github.com/tomasz-oponowicz/markets"
bugtracker = "https://github.com/tomasz-oponowicz/markets/issues"
donations = ""
translations = ""
more_information = [ "http://web.archive.org/web/20230128222300/https://apps.gnome.org/app/com.bitstower.Markets/",]
summary_source_url = "https://raw.githubusercontent.com/tomasz-oponowicz/markets/master/data/biz.zaxo.Markets.appdata.xml.in"
screenshots = [ "https://raw.githubusercontent.com/tomasz-oponowicz/markets/master/preview.png",]
screenshots_img = []
all_features_touch = 1
intended_for_mobile = 1
app_id = "biz.zaxo.Markets"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://raw.githubusercontent.com/tomasz-oponowicz/markets/master/biz.zaxo.Markets.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "markets",]
appstream_xml_url = "https://raw.githubusercontent.com/tomasz-oponowicz/markets/master/data/biz.zaxo.Markets.appdata.xml.in"
reported_by = "linmob"
updated_by = "linmob"
+++




### Description

The Markets application delivers financial data to your fingertips. Track stocks, currencies and cryptocurrencies. Stay on top of the market and never miss an investment opportunity! [Source](https://github.com/tomasz-oponowicz/markets)


### Notice

Archived in late 2022, app ID changed to `biz.zaxo.Markets`, last release and EOL'd/archived on 2023-03-03. 
