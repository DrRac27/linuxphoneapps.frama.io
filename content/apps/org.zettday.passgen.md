+++
title = "Passgen"
description = "Password generator written using Qt Quick Controls 2"
aliases = []
date = 2019-03-19
updated = 2022-12-19

[taxonomies]
project_licenses = [ "MIT and others",]
metadata_licenses = []
app_author = [ "zettdaymond",]
categories = [ "password generator",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "Qt", "Utility",]
programming_languages = [ "C", "QML", "Cpp",]
build_systems = [ "qmake",]

[extra]
repository = "https://github.com/zettdaymond/passgen"
homepage = ""
bugtracker = "https://github.com/zettdaymond/passgen/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/zettdaymond/passgen"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.zettday.passgen"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "nheko",]
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "script"
+++




### Description

Passgen - unique password generator written using Qt Quick Controls 2. Generates password based on (web) service name and master-password. Any inputted data or generated password doesn't be stored. [Source](https://github.com/zettdaymond/passgen)
