+++
title = "Keepassk"
description = "A password manager for KDBX password databases."
aliases = []
date = 2021-05-04
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "KDE Community",]
categories = [ "password manager",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick", "Kirigami",]
backends = [ "KeePass",]
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "Cpp", "QML", "Rust",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/tfella/keepassk"
homepage = ""
bugtracker = "https://invent.kde.org/tfella/keepassk/-/issues/"
donations = ""
translations = ""
more_information = [ "https://plasma-mobile.org/2021/04/27/plasma-mobile-update-march-april/",]
summary_source_url = "https://invent.kde.org/tfella/keepassk"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.keepassk"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "keepassk",]
appstream_xml_url = "https://invent.kde.org/tfella/keepassk/-/raw/master/org.kde.keepassk.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++





### Notice

WIP, read only, only for password-only encrypted databases currently.
