+++
title = "Nheko"
description = "Desktop client for the Matrix protocol"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Nheko Reborn",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "QtQuick",]
backends = []
services = [ "Matrix",]
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "Qt", "Network", "Chat",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/Nheko-Reborn/nheko"
homepage = "https://nheko-reborn.github.io/"
bugtracker = "https://github.com/Nheko-Reborn/nheko/issues/"
donations = ""
translations = ""
more_information = [ "https://wiki.mobian.org/doku.php?id=nheko",]
summary_source_url = "https://github.com/mujx/nheko"
screenshots = [ "https://fosstodon.org/@linmob/105686343450550320", "https://nheko-reborn.github.io/images/screenshots/mobile.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/io.github.nhekoreborn.nheko/1.png", "https://img.linuxphoneapps.org/io.github.nhekoreborn.nheko/2.png", "https://img.linuxphoneapps.org/io.github.nhekoreborn.nheko/3.png", "https://img.linuxphoneapps.org/io.github.nhekoreborn.nheko/4.png", "https://img.linuxphoneapps.org/io.github.nhekoreborn.nheko/5.png", "https://img.linuxphoneapps.org/io.github.nhekoreborn.nheko/6.png", "https://img.linuxphoneapps.org/io.github.nhekoreborn.nheko/7.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.github.NhekoReborn.Nheko"
scale_to_fit = "nheko"
flathub = "https://flathub.org/apps/io.github.NhekoReborn.Nheko"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "nheko",]
appstream_xml_url = "https://raw.githubusercontent.com/Nheko-Reborn/nheko/master/resources/nheko.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "script"
+++




### Description

The motivation behind the project is to provide a native desktop app for Matrix that feels more like a mainstream chat app (Element, Telegram etc) and less like an IRC client. [Source](https://nheko-reborn.github.io/)
