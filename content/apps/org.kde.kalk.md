+++
title = "Kalk"
description = "Kalk is a powerful cross-platfrom calculator application built with the Kirigami framework."
aliases = []
date = 2020-03-02
updated = 2023-02-16

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "calculator",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Kirigami",]
backends = [ "Math.js",]
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "fedora_38", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Qt", "KDE", "Utility", "Calculator",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake", "ninja",]

[extra]
repository = "https://invent.kde.org/utilities/kalk"
homepage = ""
bugtracker = "https://bugs.kde.org/describecomponents.cgi?product=kalk"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/utilities/kalk"
screenshots = [ "https://cdn.kde.org/screenshots/kalk/kalk.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.kalk"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.kalk"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "kalk",]
appstream_xml_url = "https://invent.kde.org/utilities/kalk/-/raw/master/org.kde.kalk.appdata.xml"
reported_by = "cahfofpai"
updated_by = "linmob"
+++




### Description

Kalk is a convergent calculator application built with the Kirigami framework. Although it is mainly targeted for mobile platforms, it can also be used on the desktop. Originally starting as a fork of Liri calculator, Kalk has gone through heavy development, and no longer shares the same codebase with Liri calculator. [Source](https://invent.kde.org/utilities/kalk)
