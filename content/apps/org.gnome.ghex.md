+++
title = "GHex"
description = "Inspect and edit binary files"
aliases = []
date = 2020-10-21
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Logan Rathbone",]
categories = [ "development",]
mobile_compatibility = [ "4",]
status = [ "mature",]
frameworks = [ "GTK4",]
backends = [ "libgtkhex",]
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Development",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/ghex"
homepage = "https://wiki.gnome.org/Apps/Ghex"
bugtracker = "https://gitlab.gnome.org/GNOME/ghex/-/issues/"
donations = "http://www.gnome.org/friends/"
translations = ""
more_information = [ "https://help.gnome.org/users/ghex/stable/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/ghex/-/raw/master/data/org.gnome.GHex.appdata.xml.in.in"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.ghex/1.png", "https://img.linuxphoneapps.org/org.gnome.ghex/2.png", "https://img.linuxphoneapps.org/org.gnome.ghex/3.png", "https://img.linuxphoneapps.org/org.gnome.ghex/4.png", "https://img.linuxphoneapps.org/org.gnome.ghex/5.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.GHex"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.GHex"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "ghex",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/ghex/-/raw/master/data/org.gnome.GHex.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

GHex can load raw data from binary files and display them for editing in a traditional hex editor view. The display is split in two columns, with hexadecimal values in one column and the ASCII representation in the other. GHex is a useful tool for working with raw data. [Source](https://gitlab.gnome.org/GNOME/ghex)


### Notice

Mostly mobile friendly with GHex 42 for simple browsing, options menu (bottom right) does not fit the screen.
