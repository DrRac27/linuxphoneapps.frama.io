+++
title = "White Noise Mobian"
description = "Basic white noise app for Mobian"
aliases = []
date = 2021-09-12
updated = 2022-12-19

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "greenbeast",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "SDL",]
backends = [ "pygame",]
services = []
packaged_in = [ "Debian",]
freedesktop_categories = [ "GTK", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "none",]

[extra]
repository = "https://gitlab.com/greenbeast/white_noise_mobian"
homepage = ""
bugtracker = "https://gitlab.com/greenbeast/white_noise_mobian/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/greenbeast/white_noise_mobian"
screenshots = [ "https://gitlab.com/greenbeast/white_noise_mobian/-/blob/master/screenshot/white_noise_screenshot.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "greenbeast"
updated_by = "script"

+++



### Notice

Works on other distributions too, install it via icon_install.py. Adding env SDL_VIDEODRIVER=wayland to the launchers Exec= line can improve things on distributions with new enough SDL (2.0.16 or newer).