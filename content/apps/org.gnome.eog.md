+++
title = "Eye of Gnome"
description = "This is the Eye of GNOME, an image viewer program.  It is meant to be a fast and functional image viewer."
aliases = []
date = 2020-12-12
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "GNOME Developers",]
categories = [ "image viewer",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Graphics", "Viewer",]
programming_languages = [ "C", "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/eog"
homepage = "https://wiki.gnome.org/Apps/EyeOfGnome"
bugtracker = "https://gitlab.gnome.org/GNOME/eog/-/issues/"
donations = ""
translations = ""
more_information = [ "https://apps.gnome.org/app/org.gnome.eog/",]
summary_source_url = "https://wiki.gnome.org/Apps/EyeOfGnome"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Eog"
scale_to_fit = "eog"
flathub = "https://flathub.org/apps/org.gnome.eog"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "eog",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/eog/-/raw/master/data/eog.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++





### Notice

Scales fine out of the box since the Gnome 3.38 release
