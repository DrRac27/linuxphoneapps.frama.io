+++
title = "Mt"
description = "A terminal written in Rust and gtk-rs"
aliases = []
date = 2021-01-17
updated = 2022-12-19

[taxonomies]
project_licenses = [ "BSD-3-Clause",]
metadata_licenses = []
app_author = [ "miridyan",]
categories = [ "terminal emulator",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "GNOME", "System", "TerminalEmulator",]
programming_languages = [ "Rust",]
build_systems = [ "make",]

[extra]
repository = "https://gitlab.com/Miridyan/mt"
homepage = ""
bugtracker = "https://gitlab.com/Miridyan/mt/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "no quotation"
screenshots = [ "https://fosstodon.org/@linmob/105572758025488468",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.gitlab.miridyan.Terminal"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "mt",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"
+++

