+++
title = "Gajim"
description = "A fully-featured XMPP client"
aliases = []
date = 2020-10-21
updated = 2023-03-10

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC-BY-SA-3.0",]
app_author = [ "Gajim Team",]
categories = [ "chat",]
mobile_compatibility = [ "2",]
status = [ "mature",]
frameworks = [ "GTK3",]
backends = []
services = [ "XMPP",]
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Chat", "InstantMessaging",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]

[extra]
repository = "https://dev.gajim.org/gajim/gajim"
homepage = "https://gajim.org/"
bugtracker = "https://dev.gajim.org/gajim/gajim/-/issues/"
donations = "https://gajim.org/development/#donations"
translations = "https://dev.gajim.org/gajim/gajim/-/wikis/development/devtranslate"
more_information = []
summary_source_url = "https://gajim.org/"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/org.gajim.gajim/1.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gajim.Gajim"
scale_to_fit = "org.gajim.Gajim"
flathub = "https://flathub.org/apps/org.gajim.Gajim"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gajim",]
appstream_xml_url = "https://dev.gajim.org/gajim/gajim/-/raw/master/data/org.gajim.Gajim.appdata.xml.in"
reported_by = "linmob"
updated_by = "linmob"
+++




### Description

Gajim aims to be an easy to use and fully-featured XMPP client. Just chat with your friends or family, easily share pictures and thoughts or discuss the news with your groups. Chat securely with End-to-End encryption via OMEMO or OpenPGP. [Source](https://dev.gajim.org/gajim/gajim/-/raw/master/data/org.gajim.Gajim.appdata.xml.in)


### Notice

Used to be great after scale-to-fit pre Gajim 1.4.0 and single window mode, now it’s ... just check the screenshot (with scale-to-fit enabled) below.
