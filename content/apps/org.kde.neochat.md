+++
title = "NeoChat"
description = "A client for matrix, the decentralized communication protocol."
aliases = []
date = 2020-11-03
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "The KDE Community",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami", "QtQuick",]
backends = []
services = [ "Matrix",]
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_12", "debian_unstable", "fedora_38", "fedora_rawhide", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Qt", "KDE", "Network", "Chat", "InstantMessaging",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/network/neochat"
homepage = ""
bugtracker = "https://invent.kde.org/network/neochat/-/issues/"
donations = ""
translations = ""
more_information = [ "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/#neochat",]
summary_source_url = "https://invent.kde.org/network/neochat"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.neochat"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "neochat",]
appstream_xml_url = "https://invent.kde.org/network/neochat/-/raw/master/org.kde.neochat.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Neochat is a client for Matrix, the decentralized communication protocol for instant messaging. It is a fork of Spectral, using KDE frameworks (Kirigami and KI18n). [Source](https://invent.kde.org/network/neochat)


### Notice

No end-to-end encryption yet.
