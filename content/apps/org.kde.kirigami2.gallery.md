+++
title = "Kirigami Gallery"
description = "Example application which uses all features from kirigami"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = []
app_author = [ "Marco Martin <mart@kde.org>",]
categories = [ "development",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Development",]
programming_languages = [ "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/sdk/kirigami-gallery"
homepage = ""
bugtracker = "https://invent.kde.org/sdk/kirigami-gallery/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/sdk/kirigami-gallery"
screenshots = [ "https://play.google.com/store/apps/details?id=org.kde.kirigamigallery",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.kirigami2.gallery"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "kirigami-gallery",]
appstream_xml_url = "https://invent.kde.org/sdk/kirigami-gallery/-/raw/master/org.kde.kirigami2.gallery.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++

