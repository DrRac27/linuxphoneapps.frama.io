+++
title = "Elisa"
description = "Beautiful no-nonsense music player with online radio support"
aliases = []
date = 2021-03-15
updated = 2022-12-19

[taxonomies]
project_licenses = [ "LGPL-3.0-only",]
metadata_licenses = []
app_author = [ "multimedia",]
categories = [ "music player",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Audio", "Player",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/multimedia/elisa"
homepage = "https://elisa.kde.org"
bugtracker = "https://invent.kde.org/multimedia/elisa/-/issues/"
donations = ""
translations = ""
more_information = [ "https://plasma-mobile.org/2021/04/27/plasma-mobile-update-march-april/",]
summary_source_url = "https://invent.kde.org/multimedia/elisa/-/blob/master/org.kde.elisa.appdata.xml"
screenshots = [ "https://invent.kde.org/multimedia/elisa/-/merge_requests/205",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.elisa"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.elisa"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "elisa",]
appstream_xml_url = "https://invent.kde.org/multimedia/elisa/-/raw/master/org.kde.elisa.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++




### Description

A simple music player that should be easy to use. It does not require any configuration before one can use it. [Source](https://invent.kde.org/multimedia/elisa/-/blob/master/org.kde.elisa.appdata.xml)
