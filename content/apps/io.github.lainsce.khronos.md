+++
title = "Khronos"
description = "Track each task's time in a simple inobtrusive way"
aliases = []
date = 2021-04-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Lains",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "flathub", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Clock",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/lainsce/khronos/"
homepage = ""
bugtracker = "https://github.com/lainsce/khronos/issues/"
donations = ""
translations = "https://github.com/lainsce/khronos/blob/main/po/README.md"
more_information = [ "https://apps.gnome.org/app/io.github.lainsce.Khronos/",]
summary_source_url = "https://github.com/lainsce/khronos"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/io.github.lainsce.khronos/1.png", "https://img.linuxphoneapps.org/io.github.lainsce.khronos/2.png", "https://img.linuxphoneapps.org/io.github.lainsce.khronos/3.png", "https://img.linuxphoneapps.org/io.github.lainsce.khronos/4.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.github.lainsce.Khronos"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.lainsce.Khronos"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "khronos",]
appstream_xml_url = "https://raw.githubusercontent.com/lainsce/khronos/main/data/io.github.lainsce.Khronos.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"
+++





### Notice

Sadly deep sleep interferes with time tracking on the PinePhone, so make sure that it does not suspend while you track a task.
