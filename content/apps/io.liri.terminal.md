+++
title = "Liri Terminal"
description = "A Material Design terminal."
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Liri",]
categories = [ "terminal emulator",]
mobile_compatibility = [ "3",]
status = []
frameworks = [ "QtQuick", "fluid",]
backends = []
services = []
packaged_in = [ "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "Qt", "System", "TerminalEmulator",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/lirios/terminal"
homepage = ""
bugtracker = "https://github.com/lirios/terminal/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/lirios/terminal"
screenshots = [ "https://liri.io/apps/",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.liri.Terminal"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "liri-terminal",]
appstream_xml_url = "https://raw.githubusercontent.com/lirios/terminal/develop/src/app/io.liri.Terminal.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++


