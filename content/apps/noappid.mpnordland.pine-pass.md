+++
title = "Pine Pass"
description = "GUI for password-store.org written with Python and GTK. Originally written for the PinePhone but should be compatible with most Linux distros."
aliases = []
date = 2021-06-04
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "mpnordland",]
categories = [ "password manager",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = [ "pass",]
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "flit",]

[extra]
repository = "https://github.com/mpnordland/pine_pass"
homepage = ""
bugtracker = "https://github.com/mpnordland/pine_pass/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/mpnordland/pine_pass"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++