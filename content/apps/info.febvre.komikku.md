+++
title = "Komikku"
description = "An online/offline manga reader for GNOME, developed with the aim of being used with the Librem 5 phone."
aliases = []
date = 2020-08-25
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "valos",]
categories = [ "document viewer",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur", "fedora_38", "fedora_rawhide", "flathub", "gnuguix", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Graphics", "Viewer",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/valos/Komikku"
homepage = "https://valos.gitlab.io/Komikku/"
bugtracker = "https://gitlab.com/valos/Komikku/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/valos/Komikku"
screenshots = [ "https://gitlab.com/valos/Komikku",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "info.febvre.Komikku"
scale_to_fit = ""
flathub = "https://flathub.org/apps/info.febvre.Komikku"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "komikku",]
appstream_xml_url = "https://gitlab.com/valos/Komikku/-/raw/main/data/info.febvre.Komikku.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++


