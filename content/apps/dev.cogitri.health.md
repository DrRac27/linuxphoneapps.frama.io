+++
title = "Health"
description = "A health tracking app for the GNOME desktop."
aliases = []
date = 2021-01-04
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Rasmus 'Cogitri' Thomsen",]
categories = [ "health",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Google Health",]
packaged_in = [ "aur", "flathub", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Science", "MedicalSoftware",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/World/Health"
homepage = "https://world.pages.gitlab.gnome.org/Health/libhealth/index.html"
bugtracker = "https://gitlab.gnome.org/World/Health/-/issues/"
donations = ""
translations = ""
more_information = [ "https://apps.gnome.org/app/dev.Cogitri.Health/",]
summary_source_url = "https://gitlab.gnome.org/World/Health"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "dev.Cogitri.Health"
scale_to_fit = ""
flathub = "https://flathub.org/apps/dev.Cogitri.Health"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "health", "gnome-health",]
appstream_xml_url = "https://gitlab.gnome.org/World/Health/-/raw/master/data/dev.Cogitri.Health.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++




### Notice

Only supports Google Fit – therefore the rating is based on the setup dialog
