+++
title = "Password"
description = "Strong Password for Maximum Security"
aliases = []
date = 2020-11-28
updated = 2023-04-22

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Emilien Lescoute",]
categories = [ "password generator",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/elescoute/password-for-gnome"
homepage = "https://gitlab.com/elescoute/password-for-gnome-vala"
bugtracker = "https://gitlab.com/elescoute/password-for-gnome/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/org.emilien.Password"
screenshots = [ "https://gitlab.com/elescoute/password-for-gnome-vala/raw/master/data/appdata/screenshot01.png", "https://gitlab.com/elescoute/password-for-gnome-vala/raw/master/data/appdata/screenshot02.png", "https://gitlab.com/elescoute/password-for-gnome-vala/raw/master/data/appdata/screenshot03.png", "https://gitlab.com/elescoute/password-for-gnome-vala/raw/master/data/appdata/screenshot04.png", "https://gitlab.com/elescoute/password-for-gnome-vala/raw/master/data/appdata/screenshot05.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = 1
app_id = "org.emilien.Password.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.emilien.Password"
flatpak_link = ""
flatpak_recipe = "https://gitlab.com/elescoute/password-for-gnome-vala/-/raw/master/build-aux/flatpak/org.emilien.Password.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "password-for-gnome-vala",]
appstream_xml_url = "https://gitlab.com/elescoute/password-for-gnome-vala/-/raw/master/data/org.emilien.Password.metainfo.xml.in"
reported_by = "linmob"
updated_by = "linmob"
+++



### Description

Password is a password calculator and random generator for Gnome written in Gtk/Vala.

This app calculates strong unique passwords for each alias and passphrase combination. No need to remember dozens of passwords any longer and no need for a password manager!

The calculator can use MD5, SHA1, SHA256 and SHA512 hash algorithms in combination with HMAC.

Compatible with Linux Phone using Phosh (PinePhone, Librem 5). [Source](https://gitlab.com/elescoute/password-for-gnome-vala/-/raw/master/data/org.emilien.Password.metainfo.xml.in)

### Notice

Was GTK3/libhandy before release 1.4.0.
