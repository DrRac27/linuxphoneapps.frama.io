+++
title = "GNOME Calculator"
description = "Calculator for solving mathematical equations"
aliases = []
date = 2021-09-28
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "The GNOME Project",]
categories = [ "calculator",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Calculator",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-calculator"
homepage = "https://wiki.gnome.org/Apps/Calculator"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-calculator/-/issues/"
donations = "https://www.gnome.org/donate/"
translations = ""
more_information = [ "https://apps.gnome.org/app/org.gnome.Calculator/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-calculator"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Calculator"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Calculator"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-calculator",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-calculator/-/raw/master/data/org.gnome.Calculator.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Calculator is an application that solves mathematical equations and is suitable as a default application in a Desktop environment. [Source](https://gitlab.gnome.org/GNOME/gnome-calculator)


### Notice

Adaptive upstream since release 41, GTK4/libadwaita since release 42.
