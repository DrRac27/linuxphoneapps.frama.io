+++
title = "Gnote"
description = "Gnote started as a C++ port of Tomboy and has moved its own path later."
aliases = []
date = 2021-06-21
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "GNOME Developers",]
categories = [ "note taking",]
mobile_compatibility = [ "3",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "TextEditor",]
programming_languages = [ "Cpp",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnote"
homepage = "https://wiki.gnome.org/Apps/Gnote"
bugtracker = "https://gitlab.gnome.org/GNOME/gnote/-/issues/"
donations = ""
translations = ""
more_information = [ "https://wiki.mobian-project.org/doku.php?id=gnote",]
summary_source_url = "https://gitlab.gnome.org/GNOME/gnote"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Gnote"
scale_to_fit = "gnote"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnote",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnote/-/raw/master/data/org.gnome.Gnote.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Gnote is a simple note-taking application for GNOME desktop environment. It allows you to capture your ideas, link them together using WikiWiki-style links, group together in notebooks and some extra features for everyday use. [Source](https://wiki.gnome.org/Apps/Gnote)


### Notice

Scales well (reviewed release 40.1), only the settings menu does not fit the screen properly. But: Opening notes is tedious and requires the use of the enter key on the software keyboard.
