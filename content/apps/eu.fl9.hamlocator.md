+++
title = "hamlocator"
description = "Display a maidenhead locator"
aliases = []
date = 2023-04-17

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Michał Rudowicz",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "libgeoclue",]
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://git.sr.ht/~michalr/hamlocator"
homepage = "https://sr.ht/~michalr/hamlocator/"
bugtracker = "https://todo.sr.ht/~michalr/hamlocator"
donations = ""
translations = ""
more_information = []
summary_source_url = ""
screenshots = [ "https://git.sr.ht/~michalr/hamlocator/blob/main/media/screenshot.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/eu.fl9.hamlocator/1.png", "https://img.linuxphoneapps.org/eu.fl9.hamlocator/2.png"]
all_features_touch = 1
intended_for_mobile = 1
app_id = "eu.fl9.hamlocator.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = "https://git.sr.ht/~michalr/hamlocator/blob/main/eu.fl9.hamlocator.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://git.sr.ht/~michalr/hamlocator/blob/main/data/eu.fl9.hamlocator.appdata.xml.in"
reported_by = "linmob"
updated_by = ""

+++

### Description

Simple application for displaying a Maidenhead locator written in GTK4 with mobile phones in mind. [Source](https://git.sr.ht/~michalr/hamlocator)


