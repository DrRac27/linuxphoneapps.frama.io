+++
title = "Solanum"
description = "A pomodoro timer for the GNOME desktop"
aliases = []
date = 2020-10-21
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Christopher Davis",]
categories = [ "productivity",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "flathub", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Clock",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/World/Solanum"
homepage = ""
bugtracker = "https://gitlab.gnome.org/World/Solanum/-/issues/"
donations = ""
translations = ""
more_information = [ "https://apps.gnome.org/app/org.gnome.Solanum/",]
summary_source_url = "https://gitlab.gnome.org/World/Solanum"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Solanum"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Solanum"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "solanum-pomodoro",]
appstream_xml_url = "https://gitlab.gnome.org/World/Solanum/-/raw/main/data/org.gnome.Solanum.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++


