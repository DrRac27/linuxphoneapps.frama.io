+++
title = "Workflow"
description = "A screen time monitor app for Linux. Needs ActivityWatch to be installed"
aliases = []
date = 2020-09-23
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Raffaele T.",]
categories = [ "health",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "Flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/cunidev/workflow"
homepage = ""
bugtracker = "https://gitlab.com/cunidev/workflow/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/cunidev/workflow"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.gitlab.cunidev.Workflow"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.gitlab.cunidev.Workflow"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://gitlab.com/cunidev/workflow/-/raw/master/data/com.gitlab.cunidev.Workflow.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"

+++
