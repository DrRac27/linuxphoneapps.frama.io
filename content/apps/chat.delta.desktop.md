+++
title = "Delta Chat Desktop"
description = "Email-based instant messaging for Desktop."
aliases = []
date = 2022-04-11
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "deltachat",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Electron",]
backends = [ "libdeltachat",]
services = [ "deltachat",]
packaged_in = [ "aur", "flathub", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "Network", "Chat", "InstantMessaging",]
programming_languages = [ "TypeScript",]
build_systems = [ "npm",]

[extra]
repository = "https://github.com/deltachat/deltachat-desktop"
homepage = "/en/"
bugtracker = "https://github.com/deltachat/deltachat-desktop/issues/"
donations = "https://delta.chat/en/contribute#donate-money"
translations = "https://www.transifex.com/delta-chat/public/"
more_information = [ "/en/help",]
summary_source_url = "https://github.com/deltachat/deltachat-desktop"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/chat.delta.desktop/1.png", "https://img.linuxphoneapps.org/chat.delta.desktop/2.png", "https://img.linuxphoneapps.org/chat.delta.desktop/3.png", "https://img.linuxphoneapps.org/chat.delta.desktop/4.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "chat.delta.desktop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/chat.delta.desktop"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "deltachat-desktop",]
appstream_xml_url = "https://raw.githubusercontent.com/flathub/chat.delta.desktop/master/chat.delta.desktop.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Delta Chat is like Telegram or Whatsapp but without the tracking or central control. Check out our GDPR compliancy statement.  Delta Chat doesn't have their own servers but uses the most massive and diverse open messaging system ever: the existing e-mail server network.  Chat with anyone if you know their e-mail address, no need for them to install DeltaChat! All you need is a standard e-mail account. [Source](https://flathub.org/apps/chat.delta.desktop)
