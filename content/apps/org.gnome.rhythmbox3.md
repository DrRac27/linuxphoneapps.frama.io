+++
title = "Rhythmbox"
description = "Rhythmbox is a music playing application for GNOME."
aliases = []
date = 2020-11-28
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "GNOME Developers",]
categories = [ "music player",]
mobile_compatibility = [ "3",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Audio", "Network", "Feed", "Player",]
programming_languages = [ "C", "Vala",]
build_systems = [ "make",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/rhythmbox"
homepage = "https://wiki.gnome.org/Apps/Rhythmbox"
bugtracker = "https://gitlab.gnome.org/GNOME/rhythmbox/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/rhythmbox/"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Rhythmbox3"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "rhythmbox",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/rhythmbox/-/raw/master/data/org.gnome.Rhythmbox3.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++

