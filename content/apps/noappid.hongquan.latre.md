+++
title = "LaTre"
description = "Assistant tool for GNOME Contacts. Do import contacts from VCard files, delete contacts."
aliases = []
date = 2021-07-29
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "hongquan",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "setup.py",]

[extra]
repository = "https://github.com/hongquan/LaTre"
homepage = ""
bugtracker = "https://github.com/hongquan/LaTre/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/hongquan/LaTre"
screenshots = [ "https://twitter.com/linuxphoneapps/status/1420811750065295362",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++



### Notice

Can be used to import vcard files. License according to https://github.com/hongquan/LaTre/blob/23d98b3011c2b1baec01ca61038c9c3822ffc389/debian/copyright.