+++
title = "Games"
description = "Simple game launcher for GNOME"
aliases = []
date = 2019-02-16
updated = 2022-12-23

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "The GNOME Project",]
categories = [ "game launcher",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "archlinuxarm_aarch64", "archlinuxarm_armv7h", "fedora_38", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Game", "Emulator",]
programming_languages = [ "Vala", "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/Archive/gnome-games"
homepage = "https://wiki.gnome.org/Design/Playground/Games"
bugtracker = "https://gitlab.gnome.org/Archive/gnome-games/-/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/Archive/gnome-games"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Games"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Games"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-games",]
appstream_xml_url = "https://gitlab.gnome.org/Archive/gnome-games/-/raw/master/data/org.gnome.Games.appdata.xml.in.in"
reported_by = "cahfofpai"
updated_by = "script"
+++




### Description

Games is a game manager application for GNOME. [Source](https://gitlab.gnome.org/Archive/gnome-games)


### Notice

Repo is archived, project seems to continue at https://gitlab.gnome.org/World/highscore.
