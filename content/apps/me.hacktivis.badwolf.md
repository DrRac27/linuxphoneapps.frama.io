+++
title = "BadWolf"
description = "Minimalist and privacy-oriented web browser based on WebKitGTK"
aliases = []
date = 2022-08-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "BSD-3-Clause", "CC-BY-SA-4.0",]
metadata_licenses = []
app_author = [ "git",]
categories = [ "web browser",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "fedora_38", "fedora_rawhide", "nix_unstable",]
freedesktop_categories = [ "Network", "WebBrowser",]
programming_languages = [ "C",]
build_systems = [ "make",]

[extra]
repository = "https://hacktivis.me/git/badwolf/"
homepage = "https://hacktivis.me/projects/badwolf"
bugtracker = "https://todo.sr.ht/~lanodan/badwolf"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://hacktivis.me/projects/badwolf"
screenshots = [ "https://hacktivis.me/projects/badwolf",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "me.hacktivis.badwolf"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "badwolf",]
appstream_xml_url = ""
reported_by = "lanodan"
updated_by = "script"
+++





### Notice

Note on mobile compatibility: Fits nicely in on the pinephone with a custom [GTK stylesheet](https://hacktivis.me/git/dotfiles/file/.config/gtk-3.0/gtk.css.html) allowing to go down to 306×97 px instead of 399×111 px with the defaults provided by GTK.