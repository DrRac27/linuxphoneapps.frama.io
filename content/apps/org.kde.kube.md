+++
title = "Kube"
description = "Kube is a personal information and communication application."
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "pim",]
categories = [ "email",]
mobile_compatibility = [ "2",]
status = []
frameworks = [ "QtQuick",]
backends = [ "Sink",]
services = []
packaged_in = [ "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "gentoo", "manjaro_stable", "manjaro_unstable",]
freedesktop_categories = [ "Qt", "KDE", "Office", "Email",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/pim/kube"
homepage = "https://kube-project.com/"
bugtracker = "https://invent.kde.org/pim/kube/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/pim/kube"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.kube"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "kube", "kube-develop",]
appstream_xml_url = "https://invent.kde.org/pim/kube/-/raw/master/applications/kube/kube.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "script"
+++


