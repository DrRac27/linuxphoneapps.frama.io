+++
title = "Cozy"
description = "Listen to audio books on Linux"
aliases = []
date = 2020-10-15
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Julian Geywitz",]
categories = [ "audiobook player",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "aur", "fedora_38", "fedora_rawhide", "flathub", "gnuguix", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "GTK", "GNOME", "Audio", "Player",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/geigi/cozy"
homepage = "https://cozy.sh/"
bugtracker = "https://github.com/geigi/cozy/issues/"
donations = "https://www.patreon.com/geigi"
translations = "https://www.transifex.com/geigi/cozy/"
more_information = [ "https://wiki.mobian-project.org/doku.php?id=cozy", "https://apps.gnome.org/app/com.github.geigi.cozy/",]
summary_source_url = "https://github.com/geigi/cozy"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/com.github.geigi.cozy/1.png", "https://img.linuxphoneapps.org/com.github.geigi.cozy/2.png", "https://img.linuxphoneapps.org/com.github.geigi.cozy/3.png", "https://img.linuxphoneapps.org/com.github.geigi.cozy/4.png", "https://img.linuxphoneapps.org/com.github.geigi.cozy/5.png", "https://img.linuxphoneapps.org/com.github.geigi.cozy/6.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.geigi.cozy"
scale_to_fit = "com.github.geigi.cozy"
flathub = "https://flathub.org/apps/com.github.geigi.cozy"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "cozy-audiobooks",]
appstream_xml_url = "https://raw.githubusercontent.com/geigi/cozy/master/data/com.github.geigi.cozy.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Cozy is a modern audiobook player for Linux. Here are some of the current features: Import your audiobooks into Cozy to browse them comfortably, Sort your audio books by author, reader & name, Remembers your playback position, Sleep timer, Playback speed control, Search your library, Offline Mode! This allows you to keep an audio book on your internal storage if you store your audiobooks on an external or network drive. Perfect for listening on the go!, Add mulitple storage locations, Drag & Drop to import new audio books, Support for DRM free mp3, m4a (aac, ALAC, …), flac, ogg, opus, wav files, Mpris integration (Media keys & playback info for desktop environment) [Source](https://raw.githubusercontent.com/geigi/cozy/master/data/com.github.geigi.cozy.appdata.xml)


### Notice

The app works great out of the box with release 1.20.
