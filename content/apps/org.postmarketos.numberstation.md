+++
title = "Numberstation"
description = "A Gnome Authenticator clone."
aliases = []
date = 2021-03-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "LGPL-3.0-only",]
metadata_licenses = []
app_author = [ "martijnbraam",]
categories = [ "multi-factor authentication",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "debian_12", "debian_unstable", "fedora_38", "fedora_rawhide", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://git.sr.ht/~martijnbraam/numberstation"
homepage = ""
bugtracker = "https://todo.sr.ht/~martijnbraam/numberstation"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.sr.ht/~martijnbraam/numberstation"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.postmarketos.Numberstation"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "numberstation",]
appstream_xml_url = "https://git.sr.ht/~martijnbraam/numberstation/blob/master/data/org.postmarketos.Numberstation.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++




### Description

A Gnome Authenticator clone. A Gnome Authenticator clone. This generates 2fa tokens based on secrets installed. It registers as uri-handler for otpauth:// urls so they can be added from Megapixels. [Source](https://git.sr.ht/~martijnbraam/numberstation)