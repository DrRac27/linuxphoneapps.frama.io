+++
title = "GNOME Calendar"
description = "Calendar application for GNOME"
aliases = []
date = 2022-09-28
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "GNOME Developers",]
categories = [ "calendar",]
mobile_compatibility = [ "4",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Office", "Calendar",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/gnome-calendar/"
homepage = "https://wiki.gnome.org/Apps/Calendar"
bugtracker = "https://gitlab.gnome.org/GNOME/gnome-calendar/-/issues/"
donations = ""
translations = ""
more_information = [ "https://apps.gnome.org/app/org.gnome.Calendar/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/gnome-calendar/"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.calendar/1.png", "https://img.linuxphoneapps.org/org.gnome.calendar/2.png", "https://img.linuxphoneapps.org/org.gnome.calendar/3.png", "https://img.linuxphoneapps.org/org.gnome.calendar/4.png", "https://img.linuxphoneapps.org/org.gnome.calendar/5.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Calendar"
scale_to_fit = "org.gnome.Calendar"
flathub = "https://flathub.org/apps/org.gnome.Calendar"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-calendar",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/gnome-calendar/-/raw/main/data/appdata/org.gnome.Calendar.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

GNOME Calendar is a simple and beautiful calendar application designed to perfectly fit the GNOME desktop. By reusing the components which the GNOME desktop is built on, Calendar nicely integrates with the GNOME ecosystem. We aim to find the perfect balance between nicely crafted features and user-centred usability. No excess, nothing missing. You'll feel comfortable using Calendar, like you've been using it for ages! [Source](https://gitlab.gnome.org/GNOME/gnome-calendar/-/raw/main/data/appdata/org.gnome.Calendar.appdata.xml.in.in)


### Notice

With release 43, this app has been redesigned to be mobile friendly. In initial testing (flathub), it misses this goal by a few pixels (see first screenshot). Previously, [Purism had created an adaptive fork of GNOME Calendar 41](https://source.puri.sm/Librem5/debs/gnome-calendar) for the Librem 5.
