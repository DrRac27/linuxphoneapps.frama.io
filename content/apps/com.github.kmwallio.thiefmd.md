+++
title = "ThiefMD"
description = "The markdown editor worth stealing. Inspired by Ulysses, based on code from Quilter"
aliases = []
date = 2020-11-03
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "kmwallio",]
categories = [ "text editor",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "TextEditor",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/kmwallio/ThiefMD/"
homepage = "https://thiefmd.com/"
bugtracker = "https://github.com/kmwallio/ThiefMD/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/kmwallio/ThiefMD/"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.kmwallio.thiefmd"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.kmwallio.thiefmd"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "thiefmd",]
appstream_xml_url = "https://raw.githubusercontent.com/kmwallio/ThiefMD/master/data/com.github.kmwallio.thiefmd.appdata.xml"
reported_by = "linmob"
updated_by = "script"
+++


