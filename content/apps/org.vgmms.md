+++
title = "vgmms"
description = "vgtk-based SMS+MMS client"
aliases = []
date = 2020-09-12
updated = 2022-12-19

[taxonomies]
project_licenses = [ "not specified",]
metadata_licenses = []
app_author = [ "anteater",]
categories = [ "chat",]
mobile_compatibility = [ "needs testing",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "MMS",]
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Network", "InstantMessaging",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]

[extra]
repository = "https://git.sr.ht/~anteater/vgmms"
homepage = ""
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.sr.ht/~anteater/vgmms"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.vgmms"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++


### Description

The existing messaging stacks for linux are lacking in support for MMS, from unimplemented features to antiquated frameworks. vgmms exists to do only SMS+MMS and to have feature parity with messaging clients for Android and iOS. [Source](https://git.sr.ht/~anteater/vgmms)