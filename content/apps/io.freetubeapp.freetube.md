+++
title = "FreeTube"
description = "An Open Source YouTube app for privacy"
aliases = []
date = 2020-11-28
updated = 2022-12-19

[taxonomies]
project_licenses = [ "AGPL-3.0-only",]
metadata_licenses = []
app_author = [ "freetubeapp",]
categories = [ "video player",]
mobile_compatibility = [ "4",]
status = []
frameworks = [ "Electron",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "flathub", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "Video", "Network", "Player",]
programming_languages = [ "JavaScript",]
build_systems = [ "yarn",]

[extra]
repository = "https://github.com/FreeTubeApp/FreeTube"
homepage = "https://freetubeapp.io/"
bugtracker = "https://github.com/FreeTubeApp/FreeTube/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://freetubeapp.io/"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.freetubeapp.FreeTube"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.freetubeapp.FreeTube"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "freetube",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"
+++


