+++
title = "Feeds"
description = "Feeds is a minimal RSS/Atom feed reader built with speed and simplicity in mind."
aliases = []
date = 2020-08-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Gabriele Musco",]
categories = [ "feed reader",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "RSS",]
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Feed", "News",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/World/gfeeds"
homepage = "https://gfeeds.gabmus.org/"
bugtracker = "https://gitlab.gnome.org/World/gfeeds/-/issues/"
donations = "https://liberapay.com/gabmus/donate"
translations = "https://gitlab.gnome.org/World/gfeeds/-/tree/master/po"
more_information = []
summary_source_url = "https://flathub.org/apps/org.gabmus.gfeeds"
screenshots = [ "https://gitlab.gnome.org/World/gfeeds/raw/website/static/screenshots/preferences.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gabmus.gfeeds"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gabmus.gfeeds"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-feeds",]
appstream_xml_url = "https://gitlab.gnome.org/World/gfeeds/-/raw/master/data/org.gabmus.gfeeds.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Feeds is a minimal RSS/Atom feed reader built with speed and simplicity in mind.  It offers a simple user interface that only shows the latest news from your subscriptions.  Articles are shown in a web view by default, with javascript disabled for a faster and less intrusive user experience. There's also a reader mode included, built from the one GNOME Web/Epiphany uses. [Source](https://flathub.org/apps/org.gabmus.gfeeds)


### Notice

GTK3/libhandy before 1.0
