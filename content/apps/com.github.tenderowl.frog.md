+++
title = "Frog"
description = "Extract Text From Anywhere"
aliases = []
date = 2022-11-30
updated = 2022-12-19

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "TenderOwl",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "tesseract",]
services = []
packaged_in = [ "flathub", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/tenderowl/frog"
homepage = "https://getfrog.app/"
bugtracker = "https://github.com/tenderowl/frog/issues/"
donations = "https://www.buymeacoffee.com/tenderowl"
translations = "https://hosted.weblate.org/projects/frog/default/"
more_information = []
summary_source_url = "https://getfrog.app/"
screenshots = [ "https://raw.githubusercontent.com/tenderowl/frog/master/data/screenshots/frog-config.png", "https://raw.githubusercontent.com/tenderowl/frog/master/data/screenshots/frog-window.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/com.github.tenderowl.frog/1.png", "https://img.linuxphoneapps.org/com.github.tenderowl.frog/2.png", "https://img.linuxphoneapps.org/com.github.tenderowl.frog/3.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.tenderowl.frog"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.github.tenderowl.frog"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-frog",]
appstream_xml_url = "https://raw.githubusercontent.com/TenderOwl/Frog/master/data/com.github.tenderowl.frog.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Quickly extract text from any source such as images, websites, even video - everywhere you can grab an image shot. Even if it is encoded in a QR code! [Source](https://getfrog.app/)
