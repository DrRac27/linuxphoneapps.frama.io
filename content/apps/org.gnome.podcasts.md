+++
title = "Podcasts"
description = "Podcast app for GNOME"
aliases = []
date = 2019-02-16
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Julian Hofer",]
categories = [ "podcast client",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Feed", "Audio", "Player",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/World/podcasts"
homepage = "https://wiki.gnome.org/Apps/Podcasts"
bugtracker = "https://gitlab.gnome.org/World/podcasts/-/issues/"
donations = ""
translations = ""
more_information = [ "https://apps.gnome.org/app/org.gnome.Podcasts/",]
summary_source_url = "https://gitlab.gnome.org/World/podcasts"
screenshots = [ "https://gitlab.gnome.org/World/podcasts",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Podcasts"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gnome-podcasts",]
appstream_xml_url = "https://gitlab.gnome.org/World/podcasts/-/raw/master/podcasts-gtk/resources/org.gnome.Podcasts.appdata.xml.in.in"
reported_by = "cahfofpai"
updated_by = "script"
+++




### Description

A Podcast application for GNOME. Listen to your favorite podcasts, right from your desktop. [Source](https://gitlab.gnome.org/World/podcasts)