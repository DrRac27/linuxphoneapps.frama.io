+++
title = "Monophony"
description = "Stream music from YouTube"
aliases = []
date = 2023-02-19

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "MIT",]
app_author = [ "zehkira",]
categories = [ "music player",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = [ "yt-dlp", "ytmusicapi",]
services = [ "YouTube",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "Audio", "Network", "Player",]
programming_languages = [ "Python",]
build_systems = [ "make",]

[extra]
repository = "https://gitlab.com/zehkira/monophony"
homepage = ""
bugtracker = "https://gitlab.com/zehkira/monophony/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/zehkira/monophony/-/raw/master/source/data/metainfo.xml"
screenshots = [ "https://gitlab.com/zehkira/monophony/-/raw/master/assets/screenshot1.png", "https://gitlab.com/zehkira/monophony/-/raw/master/assets/screenshot2.png",]
screenshots_img = [ "https://img.linuxphoneapps.org/io.gitlab.zehkira.monophony/1.png", "https://img.linuxphoneapps.org/io.gitlab.zehkira.monophony/2.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.gitlab.zehkira.Monophony"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.gitlab.zehkira.Monophony"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "monophony",]
appstream_xml_url = "https://gitlab.com/zehkira/monophony/-/raw/master/source/data/metainfo.xml"
reported_by = "linmob"
updated_by = ""
+++




### Description

Listen to your favorite music without using a browser. [Source](https://gitlab.com/zehkira/monophony/-/raw/master/source/data/metainfo.xml)


### Notice

This app is the successor to [Myuzi](https://linuxphoneapps.org/apps/com.gitlab.zehkira.myuzi/).
