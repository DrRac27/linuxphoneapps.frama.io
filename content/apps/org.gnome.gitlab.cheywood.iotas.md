+++
title = "Iotas"
description = "Simple note taking"
aliases = []
date = 2022-04-06
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Chris Heywood",]
categories = [ "note taking",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Nextcloud Notes",]
packaged_in = [ "aur", "flathub", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "TextEditor",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/cheywood/iotas"
homepage = ""
bugtracker = "https://gitlab.gnome.org/cheywood/iotas/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/cheywood/iotas"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.gitlab.cheywood.iotas/1.png", "https://img.linuxphoneapps.org/org.gnome.gitlab.cheywood.iotas/2.png", "https://img.linuxphoneapps.org/org.gnome.gitlab.cheywood.iotas/3.png", "https://img.linuxphoneapps.org/org.gnome.gitlab.cheywood.iotas/4.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.gitlab.cheywood.Iotas"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.gitlab.cheywood.Iotas"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "iotas",]
appstream_xml_url = "https://gitlab.gnome.org/cheywood/iotas/-/raw/main/data/org.gnome.gitlab.cheywood.Iotas.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Simple note taking with mobile-first design and a focus on Nextcloud Notes sync. [Source](https://gitlab.gnome.org/cheywood/iotas)
