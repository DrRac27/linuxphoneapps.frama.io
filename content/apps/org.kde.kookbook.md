+++
title = "Kookbook"
description = "Simple application viewer based on markdown formatted recipes."
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = []
app_author = [ "Sune Vuorela",]
categories = [ "recipe management",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/utilities/kookbook"
homepage = ""
bugtracker = "https://invent.kde.org/utilities/kookbook/-/issues/"
donations = ""
translations = ""
more_information = [ "https://pusling.com/blog/?p=499",]
summary_source_url = "https://invent.kde.org/utilities/kookbook"
screenshots = [ "https://pusling.com/blog/?p=499",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.kookbook"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "kookbook",]
appstream_xml_url = "https://invent.kde.org/utilities/kookbook/-/raw/master/src/desktop/org.kde.kookbook.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++


