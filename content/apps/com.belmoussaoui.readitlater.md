+++
title = "Read It Later"
description = "A Wallabag Client"
aliases = []
date = 2020-08-25
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Bilal Elmoussaoui",]
categories = [ "document viewer",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = [ "Wallabag",]
services = [ "Wallabag",]
packaged_in = [ "alpine_edge", "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "Education", "Literature",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/World/read-it-later"
homepage = ""
bugtracker = "https://gitlab.gnome.org/World/read-it-later/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/World/read-it-later"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.belmoussaoui.ReadItLater"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.belmoussaoui.ReadItLater"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "read-it-later",]
appstream_xml_url = "https://gitlab.gnome.org/World/read-it-later/-/raw/master/data/com.belmoussaoui.ReadItLater.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++


