+++
title = "gtkcord4"
description = "GTK4 Discord client in Go."
aliases = [ "apps/noappid.diamondburned.gtkcord4/",]
date = 2022-04-11
updated = 2023-03-25

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "diamondburned",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "Discord",]
packaged_in = [ "aur", "flathub", "nix_unstable",]
freedesktop_categories = [ "GTK", "Network", "Chat",]
programming_languages = [ "Go",]
build_systems = [ "go",]

[extra]
repository = "https://github.com/diamondburned/gtkcord4"
homepage = "https://libdb.so/gtkcord4"
bugtracker = "https://github.com/diamondburned/gtkcord4/issues/"
donations = "https://github.com/sponsors/diamondburned"
translations = ""
more_information = []
summary_source_url = "https://github.com/diamondburned/gtkcord4"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/noappid.diamondburned.gtkcord4/1.png", "https://img.linuxphoneapps.org/noappid.diamondburned.gtkcord4/2.png", "https://img.linuxphoneapps.org/noappid.diamondburned.gtkcord4/3.png", "https://img.linuxphoneapps.org/noappid.diamondburned.gtkcord4/4.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "xyz.diamondb.gtkcord4"
scale_to_fit = ""
flathub = "https://flathub.org/apps/xyz.diamondb.gtkcord4"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "gtkcord4",]
appstream_xml_url = "https://raw.githubusercontent.com/diamondburned/gtkcord4/main/xyz.diamondb.gtkcord4.metainfo.xml"
reported_by = "linmob"
updated_by = "linmob"
+++





### Notice

Replaces gtkcord3, early, but pretty good already. Opening channels takes a double tap/click. To get a token check the [gtkcord3 page](https://github.com/diamondburned/gtkcord3#logging-in).
