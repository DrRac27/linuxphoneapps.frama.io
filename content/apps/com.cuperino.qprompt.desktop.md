+++
title = "QPrompt"
description = "Personal teleprompter software for all video makers"
aliases = []
date = 2021-11-09
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Javier O. Cordero Pérez",]
categories = [ "Teleprompter software",]
mobile_compatibility = [ "3",]
status = []
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "AudioVideo", "Video",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "CMake",]

[extra]
repository = "https://github.com/Cuperino/QPrompt"
homepage = "https://qprompt.app"
bugtracker = "https://github.com/Cuperino/QPrompt/issues/"
donations = ""
translations = ""
more_information = [ "Downloads:", "https://sourceforge.net/projects/qprompt/",]
summary_source_url = "https://github.com/Cuperino/QPrompt/"
screenshots = [ "https://a.fsdn.com/con/app/proj/qprompt/screenshots/Screenshot_20211101_041917.png/max/max/1",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.cuperino.qprompt.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "qprompt",]
appstream_xml_url = "https://raw.githubusercontent.com/Cuperino/QPrompt/main/com.cuperino.qprompt.appdata.xml"
reported_by = "Cuperino"
updated_by = "script"
+++




### Description

Open source personal teleprompter software for all video makers. Built with ease of use, productivity, fast performance, and cross-platform support in mind. QPrompt's convergent user interface can be built to run on Linux, Windows, macOS, and Android. [Source](https://github.com/Cuperino/QPrompt/)


### Notice

It's workable, but does not fit the screen perfectly. I suppose currently preparing the text on a larger screen device or in a convergent setting is best. (Not sure if I set the correct compile flags for the mobile UI yet, though)
