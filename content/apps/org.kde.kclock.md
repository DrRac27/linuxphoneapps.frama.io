+++
title = "KClock"
description = "Set alarms and timers, use a stopwatch, and manage world clocks"
aliases = []
date = 2020-08-25
updated = 2023-02-16

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "clock",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Qt", "KDE", "Utility", "Clock",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/utilities/kclock"
homepage = "https://apps.kde.org/kclock/"
bugtracker = "https://bugs.kde.org/describecomponents.cgi?product=KClock"
donations = ""
translations = ""
more_information = [ "https://plasma-mobile.org/2021/04/27/plasma-mobile-update-march-april/", "https://apps.kde.org/kclock/",]
summary_source_url = "https://invent.kde.org/utilities/kclock/-/raw/master/org.kde.kclock.appdata.xml"
screenshots = [ "https://cdn.kde.org/screenshots/kclock/kclock-mobile-alarms.png", "https://cdn.kde.org/screenshots/kclock/kclock-mobile-stopwatch.png", "https://cdn.kde.org/screenshots/kclock/kclock-mobile-timers.png", "https://cdn.kde.org/screenshots/kclock/kclock-mobile-timezones.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.kclock"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.kclock"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "kclock", "kde5-kclock",]
appstream_xml_url = "https://invent.kde.org/utilities/kclock/-/raw/master/org.kde.kclock.appdata.xml"
reported_by = "linmob"
updated_by = "linmob"
+++




### Description

A universal clock application for desktop and mobile. [Source](https://invent.kde.org/utilities/kclock/-/raw/master/org.kde.kclock.appdata.xml)
