+++
title = "Subsurface"
description = "Manage and display dive computer data"
aliases = []
date = 2019-02-01
updated = 2022-04-23

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = [ "CC-BY-SA-3.0",]
app_author = [ "The Subsurface development team",]
categories = [ "sports",]
mobile_compatibility = [ "needs testing",]
status = [ "mature",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "Qt", "Education", "Geography",]
programming_languages = [ "Cpp", "C",]
build_systems = [ "cmake",]

[extra]
repository = "https://github.com/subsurface/subsurface"
homepage = "https://subsurface-divelog.org"
bugtracker = "https://github.com/Subsurface/subsurface/issues"
donations = ""
translations = "https://explore.transifex.com/subsurface/subsurface/"
more_information = []
summary_source_url = "https://subsurface-divelog.org/"
screenshots = [ "https://raw.githubusercontent.com/Subsurface/subsurface/master/appdata/main.png", "https://raw.githubusercontent.com/Subsurface/subsurface/master/appdata/diveplanner.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.subsurface_divelog.Subsurface"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.subsurface_divelog.Subsurface"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "subsurface",]
appstream_xml_url = "https://raw.githubusercontent.com/subsurface/subsurface/master/appdata/subsurface.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "linmob"
+++



### Notice

Packaging on AUR and Flathub contains the QWidget-based desktop app, that still somewhat fits the screen and does somewhat work. The mobile, Kirigami-based interface (created for the Android app) needs to be build manually and needs evaluation on Mobile Linux.
