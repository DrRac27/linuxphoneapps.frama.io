+++
title = "Pika Backup"
description = "Simple backups based on borg"
aliases = []
date = 2020-11-28
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Sophie Herold",]
categories = [ "backup", "system utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "borg",]
packaged_in = [ "aur", "flathub", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Archiving",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/World/pika-backup"
homepage = ""
bugtracker = "https://gitlab.gnome.org/World/pika-backup/-/issues/"
donations = ""
translations = ""
more_information = [ "https://apps.gnome.org/app/org.gnome.World.PikaBackup/",]
summary_source_url = "https://gitlab.gnome.org/World/pika-backup"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.World.PikaBackup"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.World.PikaBackup"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "pika-backup",]
appstream_xml_url = "https://gitlab.gnome.org/World/pika-backup/-/raw/main/data/app.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"
+++


