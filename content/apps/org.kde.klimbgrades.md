+++
title = "Klimbgrades"
description = "Conversion utility for the difficulty rating of rock climbs"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "KDE Community",]
categories = [ "sports",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "QML", "Cpp", "JS",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/utilities/klimbgrades"
homepage = ""
bugtracker = "https://invent.kde.org/utilities/klimbgrades/-/issues/"
donations = ""
translations = ""
more_information = [ "https://notmart.org/blog/2017/03/climbing-grades-a-kirigami-example-app/",]
summary_source_url = "https://invent.kde.org/utilities/klimbgrades"
screenshots = [ "http://web.archive.org/web/20191011093525/https://play.google.com/store/apps/details?id=org.kde.klimbgrades",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.klimbgrades"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://invent.kde.org/utilities/klimbgrades/-/raw/master/org.kde.klimbgrades.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"

+++
