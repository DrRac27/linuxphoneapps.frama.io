+++
title = "OptiImage"
description = "An image optimizer based on optipng."
aliases = []
date = 2021-04-29
updated = 2022-12-19

[taxonomies]
project_licenses = [ "No license", "all rights reserved.",]
metadata_licenses = []
app_author = [ "KDE Community",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick", "Kirigami",]
backends = []
services = []
packaged_in = []
freedesktop_categories = [ "Qt", "KDE", "Utility", "Graphics",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/carlschwan/optiimage"
homepage = ""
bugtracker = "https://invent.kde.org/carlschwan/optiimage/-/issues/"
donations = ""
translations = ""
more_information = [ "https://plasma-mobile.org/2021/04/27/plasma-mobile-update-march-april/",]
summary_source_url = "no quotation"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.optiimage"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = "https://invent.kde.org/carlschwan/optiimage/-/raw/master/org.kde.optiimage.appdata.xml"
reported_by = "linmob"
updated_by = "script"

+++



### Notice

WIP
