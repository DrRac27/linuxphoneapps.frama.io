+++
title = "Banking"
description = "Banking application for small screens"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Jan-Michael Brummer",]
categories = [ "banking",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = [ "aqbanking",]
services = []
packaged_in = [ "flathub", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Office", "Finance",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/tabos/banking"
homepage = "https://tabos.gitlab.io/projects/banking/"
bugtracker = "https://gitlab.com/tabos/banking/-/issues/"
donations = "https://www.paypal.com/paypalme/tabos/10/"
translations = ""
more_information = []
summary_source_url = "https://tabos.gitlab.io/projects/banking/"
screenshots = [ "https://tabos.gitlab.io/projects/banking/",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.tabos.Banking"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.tabos.banking"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "banking",]
appstream_xml_url = "https://gitlab.com/tabos/banking/-/raw/master/data/org.tabos.banking.appdata.xml.in.in"
reported_by = "cahfofpai"
updated_by = "script"
+++




### Description

An easy way to access your online banking information. Show your balance and transaction based on FinTS online banking information. FinTS online banking application designed for Linux smartphones. [Source](https://gitlab.com/tabos/banking/-/raw/master/data/org.tabos.banking.appdata.xml.in.in)
