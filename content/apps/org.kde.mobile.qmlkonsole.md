+++
title = "QMLKonsole"
description = "Terminal app for Plasma Mobile"
aliases = []
date = 2019-02-01
updated = 2023-02-23

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "terminal emulator",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_12", "debian_unstable", "fedora_38", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Qt", "System", "TerminalEmulator",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/plasma-mobile/qmlkonsole"
homepage = ""
bugtracker = "https://invent.kde.org/plasma-mobile/qmlkonsole/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/plasma-mobile/qmlkonsole"
screenshots = [ "https://cdn.kde.org/screenshots/qmlkonsole/main.png", "https://cdn.kde.org/screenshots/qmlkonsole/settings.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.mobile.qmlkonsole"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.qmlkonsole"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "qmlkonsole", "kde5-qmlkonsole",]
appstream_xml_url = "https://invent.kde.org/plasma-mobile/qmlkonsole/-/raw/master/org.kde.qmlkonsole.appdata.xml"
reported_by = "cahfofpai"
updated_by = "linmob"
+++




### Description

Terminal application offering additional keyboard buttons useful on touch devices. [Source](https://invent.kde.org/plasma-mobile/qmlkonsole/-/raw/master/org.kde.qmlkonsole.appdata.xml)


### Notice

App ID is now org.kde.qmlkonsole, listing will be fixed once we have aliases implemented.
