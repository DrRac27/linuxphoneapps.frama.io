+++
title = "Dukto"
description = "A simple multi-platform file transfer application especially designed for LAN users and which supports sending text, files or folders. It supports Windows, OS X, Linux, iOS, Android and more."
aliases = []
date = 2019-03-04
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = []
app_author = [ "ttys3",]
categories = [ "file transfer",]
mobile_compatibility = [ "5",]
status = [ "archived",]
frameworks = [ "QtQuick",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "Qt", "System", "FileTools", "P2P",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "qmake",]

[extra]
repository = "https://github.com/ttys3/dukto"
homepage = "https://www.msec.it/blog/dukto/"
bugtracker = "https://github.com/ttys3/dukto/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "http://web.archive.org/web/20180822053309/https://play.google.com/store/apps/details?id=it.msec.dukto"
screenshots = [ "http://web.archive.org/web/20180822053309/https://play.google.com/store/apps/details?id=it.msec.dukto",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "dukto",]
appstream_xml_url = ""
reported_by = "cahfofpai"
updated_by = "script"
+++





### Notice

No longer maintained, used to have n900 variant at http://talk.maemo.org/showpost.php?p=900004&postcount=44. To be moved to archive.
