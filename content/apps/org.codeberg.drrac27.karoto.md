+++
title = "Karoto Shopping List"
description = "A shopping list app for Linux mobile"
aliases = []
date = 2023-04-03

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "MIT",]
app_author = [ "DrRac27",]
categories = [ "productivity", "utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "PyQt6",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "Utility", "Qt",]
programming_languages = [ "Python",]
build_systems = [ "custom",]

[extra]
repository = "https://codeberg.org/DrRac27/karoto"
homepage = "https://codeberg.org/DrRac27/karoto"
bugtracker = "https://codeberg.org/DrRac27/karoto/issues"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://codeberg.org/DrRac27/karoto/raw/branch/main/packaging/org.codeberg.DrRac27.Karoto.metainfo.xml"
screenshots = [ "https://codeberg.org/DrRac27/karoto/src/branch/main/screenshots",]
screenshots_img = [ "https://codeberg.org/DrRac27/karoto/raw/branch/main/screenshots/storage_feed.png", "https://codeberg.org/DrRac27/karoto/raw/branch/main/screenshots/shopping_feed.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.codeberg.DrRac27.Karoto"
scale_to_fit = ""
flathub = "https://flathub.org/apps/page.codeberg.DrRac27.Karoto"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "karoto",]
appstream_xml_url = "https://codeberg.org/DrRac27/karoto/raw/branch/main/packaging/org.codeberg.DrRac27.Karoto.metainfo.xml"
reported_by = "DrRac27"
updated_by = ""
+++




### Description

A Linux mobile app that helps you with your groceries. It is not just a fancy replacement for your notes app but instead works a bit like a stock management software. The heart of the app is a list with things you always want to have at home. Before you go to the grocery store or whereever you get your stuff from you can go through this list and check for every item how much you have in your storage. The app then generates the shopping list for you which makes it impossible to forget anything. If you need something like a new knive or some new plates e.g. there is the "only once" option where the item automatically gets deleted as soon as you got it once. [Source](https://codeberg.org/DrRac27/karoto/raw/branch/main/packaging/org.codeberg.DrRac27.Karoto.metainfo.xml)
