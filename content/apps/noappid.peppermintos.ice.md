+++
title = "ICE SSB"
description = "Tool to create Chromium/Chrome/Firefox/Vivaldi SSBs in Peppermint OS."
aliases = []
date = 2020-10-21
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-only",]
metadata_licenses = []
app_author = [ "peppermintos",]
categories = [ "web app laucher",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = []
packaged_in = [ "aur",]
freedesktop_categories = [ "GTK", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "none",]

[extra]
repository = "https://github.com/peppermintos/ice"
homepage = ""
bugtracker = "https://github.com/peppermintos/ice/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/peppermintos/ice"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = "ice"
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "ice-ssb",]
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"
+++





### Notice

Only tested with Firefox, Android User Agent makes this a lot less useful