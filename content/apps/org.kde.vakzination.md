+++
title = "Vakzination"
description = "Vakzination manages your health certificates like vaccination, test, and recovery certificates."
aliases = []
date = 2021-07-14
updated = 2023-02-15

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "FSFAP",]
app_author = [ "KDE Community",]
categories = [ "health",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "fedora_38", "fedora_rawhide",]
freedesktop_categories = [ "Qt", "Utility",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/pim/vakzination"
homepage = ""
bugtracker = "https://invent.kde.org/pim/vakzination/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/pim/vakzination"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.vakzination"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "vakzination",]
appstream_xml_url = "https://invent.kde.org/pim/vakzination/-/raw/master/org.kde.vakzination.metainfo.xml"
reported_by = "myxor"
updated_by = "linmob"
+++


