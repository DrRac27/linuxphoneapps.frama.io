+++
title = "Wiremapper"
description = "GTK3 frontend for the Pockethernet"
aliases = []
date = 2020-10-21
updated = 2022-12-19

[taxonomies]
project_licenses = [ "No license", "all rights reserved.",]
metadata_licenses = []
app_author = [ "martijnbraam",]
categories = [ "network",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "PocketEthernet",]
packaged_in = [ "alpine_edge", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.com/MartijnBraam/wiremapper"
homepage = ""
bugtracker = "https://gitlab.com/MartijnBraam/wiremapper/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.com/MartijnBraam/wiremapper"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "nl.brixit.wiremapper"
scale_to_fit = ""
flathub = "https://flathub.org/apps/nl.brixit.wiremapper"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "wiremapper",]
appstream_xml_url = "https://gitlab.com/MartijnBraam/wiremapper/-/raw/master/data/nl.brixit.wiremapper.metainfo.xml"
reported_by = "linmob"
updated_by = "script"
+++




### Description

This is a python GTK3 application that uses the pockethernet library to run various network tests using the Pockethernet hardware (a bluetooth connected network diagnostics tool) [Source](https://gitlab.com/MartijnBraam/wiremapper)
