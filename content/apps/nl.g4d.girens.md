+++
title = "Girens for Plex"
description = "Girens is a Plex GTK client for playing movies, TV shows and music from your Plex library."
aliases = []
date = 2019-09-30
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Gerben Droogers",]
categories = [ "media",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "Plex",]
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "AudioVideo", "Player",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/tijder/girens"
homepage = ""
bugtracker = "https://gitlab.gnome.org/tijder/girens/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/tijder/girens"
screenshots = [ "https://flathub.org/apps/nl.g4d.Girens",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "nl.g4d.Girens"
scale_to_fit = ""
flathub = "https://flathub.org/apps/nl.g4d.Girens"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "girens",]
appstream_xml_url = "https://gitlab.gnome.org/tijder/girens/-/raw/master/data/nl.g4d.Girens.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "script"
+++


