+++
title = "Plasma Mobile Phonebook"
description = "Phone book for Plasma Mobile."
aliases = []
date = 2019-09-30
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "contacts",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = [ "kpeople", "kpeoplevcard", "Kcontacts",]
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "debian_12", "debian_unstable", "fedora_38", "fedora_rawhide", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "Qt", "KDE", "Office", "ContactManagement",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/plasma-mobile/plasma-phonebook"
homepage = ""
bugtracker = "https://invent.kde.org/plasma-mobile/plasma-phonebook/-/issues/"
donations = ""
translations = ""
more_information = [ "https://phabricator.kde.org/T6937",]
summary_source_url = "https://invent.kde.org/plasma-mobile/plasma-phonebook"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.phonebook"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "plasma-phonebook",]
appstream_xml_url = "https://invent.kde.org/plasma-mobile/plasma-phonebook/-/raw/master/org.kde.phonebook.metainfo.xml"
reported_by = "nicolasfella"
updated_by = "script"
+++




### Description

Contacts application which allows adding, modifying and removing contacts. [Source](https://invent.kde.org/plasma-mobile/plasma-phonebook)
