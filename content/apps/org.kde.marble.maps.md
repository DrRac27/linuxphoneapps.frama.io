+++
title = "Marble Maps"
description = "Marble is a Virtual Globe and World Atlas that you can use to learn more about Earth"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "LGPL-2.1-or-later",]
metadata_licenses = []
app_author = [ "KDE Community",]
categories = [ "maps and navigation", "geography",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Education", "Geography",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/education/marble"
homepage = "https://marble.kde.org/features.php"
bugtracker = "https://invent.kde.org/education/marble/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/education/marble"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.marble.maps"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "marble",]
appstream_xml_url = "https://invent.kde.org/education/marble/-/raw/master/src/apps/marble-maps/org.kde.marble.maps.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++






### Notice

Marble maps is sadly not working well, at least on wayland.
