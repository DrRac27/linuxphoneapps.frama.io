+++
title = "Thumbdrives"
description = "Thumbdrive and ISO emulator for phones"
aliases = []
date = 2021-03-24
updated = 2023-04-23

[taxonomies]
project_licenses = [ "MIT",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "martijnbraam",]
categories = [ "system utilities",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK3", "libhandy",]
backends = []
services = [ "USB mass storage",]
packaged_in = [ "alpine_edge", "aur", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://git.sr.ht/~martijnbraam/thumbdrives"
homepage = ""
bugtracker = "https://todo.sr.ht/~martijnbraam/thumbdrives"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://git.sr.ht/~martijnbraam/thumbdrives"
screenshots = [ "http://brixitcdn.net/metainfo/thumbdrives.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "nl.brixit.Thumbdrives.desktop"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "thumbdrives",]
appstream_xml_url = "https://git.sr.ht/~martijnbraam/thumbdrives/blob/master/data/nl.brixit.Thumbdrives.metainfo.xml.in"
reported_by = "linmob"
updated_by = "linmob"
+++



### Description

Thumbdrives is a USB mass storage emulator for Linux handhelds. It uses the
USB gadget configurations in the kernel to present a virtual USB CD drive or
thumbdrive to the attached device. This makes it possible to install linux
from the phone on the target device.

CD emulation above cdrom sizes is not supported by the kernel and those .iso
files will be emulated as usb mass storage instead. Files ending in .img will
always be emulated as usb mass storage and will be writable. [Source](https://git.sr.ht/~martijnbraam/thumbdrives/blob/master/README.md)

