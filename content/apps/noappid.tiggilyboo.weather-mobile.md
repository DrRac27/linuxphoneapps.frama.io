+++
title = "Weather Mobile"
description = "A simple GTK weather app to play with Linux Mobile development in GTK4 and OpenWeather API."
aliases = []
date = 2021-02-06
updated = 2022-12-19

[taxonomies]
project_licenses = [ "not specified",]
metadata_licenses = []
app_author = [ "tiggilyboo",]
categories = [ "weather",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = [ "OpenWeather API",]
services = []
packaged_in = []
freedesktop_categories = [ "GTK", "GNOME", "Utility",]
programming_languages = [ "Rust",]
build_systems = [ "cargo",]

[extra]
repository = "https://github.com/Tiggilyboo/weather-mobile"
homepage = ""
bugtracker = "https://github.com/Tiggilyboo/weather-mobile/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/Tiggilyboo/weather-mobile"
screenshots = [ "https://fosstodon.org/@linmob/105703922646357593",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = ""
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = []
appstream_xml_url = ""
reported_by = "linmob"
updated_by = "script"

+++



### Notice

WIP, lacks desktop file and PKGBUILD does not work.