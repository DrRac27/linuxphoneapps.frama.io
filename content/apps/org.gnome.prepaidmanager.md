+++
title = "Prepaid manager"
description = "Prepaid-manager-applet (ppm) is an applet for the GNOME Desktop that allows you to check and top up the balance of GSM mobile prepaid SIM cards."
aliases = []
date = 2020-10-15
updated = 2023-04-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC-BY-SA-3.0",]
app_author = [ "Guido Günther",]
categories = [ "mobile service utility",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK3",]
backends = []
services = [ "USSD",]
packaged_in = [ "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "TelephonyTools",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://git.sigxcpu.org/cgit/ppm"
homepage = "https://honk.sigxcpu.org/piki/projects/ppm/"
bugtracker = ""
donations = ""
translations = ""
more_information = []
summary_source_url = "https://honk.sigxcpu.org/piki/projects/ppm/#index2h3"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/org.gnome.prepaidmanager/1.png", "https://img.linuxphoneapps.org/org.gnome.prepaidmanager/2.png", "https://img.linuxphoneapps.org/org.gnome.prepaidmanager/3.png", "https://img.linuxphoneapps.org/org.gnome.prepaidmanager/4.png",]
all_features_touch = 1
intended_for_mobile = 1
app_id = "org.gnome.PrepaidManager"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "prepaid-manager-applet",]
appstream_xml_url = "https://git.sigxcpu.org/cgit/ppm/plain/data/org.gnome.PrepaidManager.appdata.xml.in"
reported_by = "linmob"
updated_by = "linmob"
+++



### Description

GNOME Prepaid-manager-applet is an applet for the GNOME Desktop that allows you to check and top up the balance of GSM mobile prepaid SIM cards.

It uses ModemManager to talk to the modem and mobile-broadband-provider-info to determine the necessary USSD codes for your provider. [Source](https://git.sigxcpu.org/cgit/ppm/plain/data/org.gnome.PrepaidManager.appdata.xml.in)
