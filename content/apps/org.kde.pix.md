+++
title = "Pix"
description = "Convergent image and gallery viewer."
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "LGPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "maui",]
categories = [ "image viewer",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "MauiKit", "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Qt", "Graphics", "Viewer",]
programming_languages = [ "QML", "Cpp", "C",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/maui/pix"
homepage = "https://mauikit.org/apps/pix/"
bugtracker = "https://invent.kde.org/maui/pix/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://flathub.org/apps/org.kde.pix"
screenshots = [ "https://medium.com/nitrux/maui-apps-apk-packages-5c966f185f0c", "https://medium.com/nitrux/maui-kde-fcdc920138e2", "https://medium.com/nitrux/maui-plasma-mobile-sprint-2019-c20031700b3b",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.pix"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.pix"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "maui-pix", "pix",]
appstream_xml_url = "https://invent.kde.org/maui/pix/-/raw/master/org.kde.pix.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++




### Description

Pix is an image gallery and image viewer. Pix can be used to open images with other applications like an image editor, add tags to the files, add annotations to pictures, rotate and share them. [Source](https://apps.kde.org/pix/)
