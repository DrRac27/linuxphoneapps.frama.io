+++
title = "Color Palette"
description = "Colour Palette tool"
aliases = []
date = 2020-02-06
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Zander Brown",]
categories = [ "development",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4",]
backends = []
services = []
packaged_in = [ "aur", "flathub", "opensuse_tumbleweed",]
freedesktop_categories = [ "GTK", "GNOME", "Development", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/World/design/palette"
homepage = ""
bugtracker = "https://gitlab.gnome.org/World/design/palette/-/issues/"
donations = ""
translations = ""
more_information = [ "https://apps.gnome.org/app/org.gnome.design.Palette/",]
summary_source_url = "https://gitlab.gnome.org/World/design/palette"
screenshots = [ "https://gitlab.gnome.org/World/design/palette",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.design.Palette"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.design.Palette"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "palette",]
appstream_xml_url = "https://gitlab.gnome.org/World/design/palette/-/raw/master/data/org.gnome.design.Palette.metainfo.xml.in"
reported_by = "cahfofpai"
updated_by = "script"
+++


