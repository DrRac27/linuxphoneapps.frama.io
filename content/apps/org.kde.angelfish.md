+++
title = "Angelfish Webbrowser"
description = "Web browser for mobile devices"
aliases = []
date = 2019-02-01
updated = 2023-04-23

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "web browser",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_12", "debian_unstable", "fedora_38", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Network", "KDE", "Qt", "WebBrowser",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/network/angelfish"
homepage = "https://apps.kde.org/angelfish"
bugtracker = "https://bugs.kde.org/describecomponents.cgi?product=angelfish"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/network/angelfish"
screenshots = [ "https://cdn.kde.org/screenshots/plasma-angelfish/homepage.png", "https://cdn.kde.org/screenshots/plasma-angelfish/actions.png", "https://cdn.kde.org/screenshots/plasma-angelfish/tabs.png", "https://cdn.kde.org/screenshots/plasma-angelfish/desktop.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = 1
app_id = "org.kde.angelfish"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.angelfish"
flatpak_link = ""
flatpak_recipe = "https://invent.kde.org/network/angelfish/-/raw/master/org.kde.angelfish.json"
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "angelfish", "angelfish",]
appstream_xml_url = "https://invent.kde.org/network/angelfish/-/raw/master/org.kde.angelfish.metainfo.xml"
reported_by = "cahfofpai"
updated_by = "linmob"
+++



### Description

Angelfish is a modern mobile webbrowser. [Source](https://invent.kde.org/network/angelfish/-/raw/master/org.kde.angelfish.metainfo.xml)
