+++
title = "KDE Minuet"
description = "A KDE Software for Music Education"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "KDE Community",]
categories = [ "education",]
mobile_compatibility = [ "2",]
status = []
frameworks = [ "Kirigami",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Education", "Music",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/education/minuet"
homepage = ""
bugtracker = "https://invent.kde.org/education/minuet/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://invent.kde.org/education/minuet"
screenshots = [ "https://sandroandrade.org/minuet-0-2-massive-refactoring-and-android-version-available/",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.minuet"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.minuet"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "minuet",]
appstream_xml_url = "https://invent.kde.org/education/minuet/-/raw/master/org.kde.minuet.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++





### Notice

Does not really work
