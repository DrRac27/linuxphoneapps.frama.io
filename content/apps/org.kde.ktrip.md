+++
title = "KTrip"
description = "KTrip helps you navigate in public transport. It allows you to query journeys between specified locations."
aliases = []
date = 2020-02-04
updated = 2022-12-23

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "KDE Community",]
categories = [ "public transport",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = [ "KPublicTransport",]
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "debian_12", "debian_unstable", "fedora_38", "fedora_rawhide", "flathub", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "QML", "Cpp", "Java",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/utilities/ktrip"
homepage = "https://apps.kde.org/utilities/org.kde.ktrip/"
bugtracker = "https://invent.kde.org/utilities/ktrip/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://apps.kde.org/utilities/org.kde.ktrip/"
screenshots = [ "https://apps.kde.org/utilities/org.kde.ktrip/",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.ktrip"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.ktrip"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "ktrip",]
appstream_xml_url = "https://invent.kde.org/utilities/ktrip/-/raw/master/org.kde.ktrip.appdata.xml"
reported_by = "nicolasfella"
updated_by = "script"
+++




### Description

KTrip is a public transport assistant targeted towards mobile Linux and Android. It allows to query journeys for a wide range of countries/public transport providers by leveraging KPublicTransport. [Source](https://invent.kde.org/utilities/ktrip)
