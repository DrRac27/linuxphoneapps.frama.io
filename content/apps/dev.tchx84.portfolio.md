+++
title = "Portfolio"
description = "A minimalist file manager for those who want to use Linux mobile devices."
aliases = []
date = 2020-12-22
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Martin Abente Lahaye",]
categories = [ "file management",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "debian_12", "debian_unstable", "flathub", "gnuguix", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "System", "FileManager",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/tchx84/Portfolio"
homepage = ""
bugtracker = "https://github.com/tchx84/Portfolio/issues/"
donations = ""
translations = ""
more_information = [ "https://blogs.gnome.org/tchx84/2020/12/21/portfolio-manage-files-in-your-phone/",]
summary_source_url = "https://github.com/tchx84/Portfolio"
screenshots = [ "https://fosstodon.org/@linmob/105420507280474714",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "dev.tchx84.Portfolio"
scale_to_fit = ""
flathub = "https://flathub.org/apps/dev.tchx84.Portfolio"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "portfolio-file-manager",]
appstream_xml_url = "https://raw.githubusercontent.com/tchx84/Portfolio/master/data/dev.tchx84.Portfolio.metainfo.xml.in"
reported_by = "linmob"
updated_by = "script"
+++





### Notice

Minimal feature set, works fine and fast
