+++
title = "Audio Sharing"
description = "Share your computer audio"
aliases = []
date = 2021-12-05
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Felix Häcker",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Audio", "Network",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/World/AudioSharing"
homepage = ""
bugtracker = "https://gitlab.gnome.org/World/AudioSharing/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/World/AudioSharing/-/raw/main/data/de.haeckerfelix.AudioSharing.metainfo.xml.in.in"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "de.haeckerfelix.AudioSharing"
scale_to_fit = ""
flathub = "https://flathub.org/apps/de.haeckerfelix.AudioSharing"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "audio-sharing",]
appstream_xml_url = "https://gitlab.gnome.org/World/AudioSharing/-/raw/main/data/de.haeckerfelix.AudioSharing.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

With Audio Sharing you can share your current computer audio playback in the form of an RTSP stream. This stream can then be played back by other devices, for example using VLC.  By sharing the audio as a network stream, you can also use common devices that are not intended to be used as audio sinks (eg. smartphones) to receive it. For example, there are audio accessories that are not compatible with desktop computers (e.g. because the computer does not have a Bluetooth module installed). With the help of this app, the computer audio can be played back on a smartphone, which is then connected to the Bluetooth accessory. [Source](https://gitlab.gnome.org/World/AudioSharing/-/raw/main/data/de.haeckerfelix.AudioSharing.metainfo.xml.in.in)


### Notice

It works, but no idea how useful this is on a phone that has Bluetooth.
