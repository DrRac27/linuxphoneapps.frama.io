+++
title = "Frogr"
description = "A Flickr Remote Organizer for GNOME"
aliases = []
date = 2020-10-15
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Mario Sanchez Prada",]
categories = [ "photo management",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3",]
backends = []
services = [ "Flickr",]
packaged_in = [ "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "FileTransfer",]
programming_languages = [ "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/frogr/"
homepage = "https://wiki.gnome.org/Apps/Frogr"
bugtracker = "https://gitlab.gnome.org/GNOME/frogr/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://gitlab.gnome.org/GNOME/frogr/"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.frogr"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.frogr"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "frogr",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/frogr/-/raw/master/data/org.gnome.frogr.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++


