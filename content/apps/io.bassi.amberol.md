+++
title = "Amberol"
description = "A small and simple sound and music player that is well integrated with GNOME."
aliases = []
date = 2022-04-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Emmanuele Bassi",]
categories = [ "music player",]
mobile_compatibility = [ "5",]
status = [ "early",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "flathub", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Audio", "Player",]
programming_languages = [ "Rust",]
build_systems = [ "meson", "cargo",]

[extra]
repository = "https://gitlab.gnome.org/World/amberol"
homepage = ""
bugtracker = "https://gitlab.gnome.org/World/amberol/-/issues/"
donations = "https://ko-fi.com/emmanueleb"
translations = ""
more_information = [ "https://apps.gnome.org/app/io.bassi.Amberol/", "https://www.omgubuntu.co.uk/2022/04/amberol-simple-gtk-music-player/amp", "https://www.youtube.com/watch?v=P5YMekBLOns", "https://www.youtube.com/watch?v=J_GyS6zhyZk",]
summary_source_url = "https://gitlab.gnome.org/World/amberol"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/io.bassi.amberol/1.png", "https://img.linuxphoneapps.org/io.bassi.amberol/2.png", "https://img.linuxphoneapps.org/io.bassi.amberol/3.png", "https://img.linuxphoneapps.org/io.bassi.amberol/4.png", "https://img.linuxphoneapps.org/io.bassi.amberol/5.png", "https://img.linuxphoneapps.org/io.bassi.amberol/6.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.bassi.Amberol"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.bassi.Amberol"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "amberol",]
appstream_xml_url = "https://gitlab.gnome.org/World/amberol/-/raw/main/data/io.bassi.Amberol.appdata.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Amberol is a music player with no delusions of grandeur. If you just want to play music available on your local system then Amberol is the music player you are looking for. Current features: - adaptive UI, - UI recoloring using the album art, - drag and drop support to queue songs, - shuffle and repeat, - MPRIS integration [Source](https://gitlab.gnome.org/World/amberol/-/raw/main/data/io.bassi.Amberol.appdata.xml.in.in)
