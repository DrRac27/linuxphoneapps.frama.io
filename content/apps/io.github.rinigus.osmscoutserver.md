+++
title = "OSM Scout Server"
description = "Maps server providing tiles, geocoder, and router"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "rinigus",]
categories = [ "maps server",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick", "Kirigami", "Silica",]
backends = []
services = [ "openstreetmap",]
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "flathub", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "Qt", "Utility", "Maps",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "qmake",]

[extra]
repository = "https://github.com/rinigus/osmscout-server"
homepage = "https://rinigus.github.io/osmscout-server/en/"
bugtracker = "https://github.com/rinigus/osmscout-server/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/rinigus/osmscout-server"
screenshots = [ "https://openrepos.net/content/rinigus/osm-scout-server",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "io.github.rinigus.OSMScoutServer"
scale_to_fit = ""
flathub = "https://flathub.org/apps/io.github.rinigus.OSMScoutServer"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "osmscout-server",]
appstream_xml_url = "https://raw.githubusercontent.com/rinigus/osmscout-server/master/packaging/osmscout-server.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++




### Description

OSM Scout Server can be used to add offline operation mode for navigation or any other application requiring map data access. Maps in vector and raster formats are supported as well as providing geocoding, routing, and map matching. [Source](https://github.com/rinigus/osmscout-server)
