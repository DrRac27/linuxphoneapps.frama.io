+++
title = "Lollypop"
description = "Lollypop is a modern music player for GNOME"
aliases = []
date = 2019-02-01
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "GNOME Developers",]
categories = [ "music player",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK3", "libhandy",]
backends = []
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Audio", "Music", "Player",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/World/lollypop"
homepage = "https://wiki.gnome.org/Apps/Lollypop"
bugtracker = "https://gitlab.gnome.org/World/lollypop/-/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://wiki.gnome.org/Apps/Lollypop"
screenshots = [ "https://wiki.gnome.org/Apps/Lollypop",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Lollypop"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Lollypop"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "lollypop", "lollypop-next", "lollypop-stable",]
appstream_xml_url = "https://gitlab.gnome.org/World/lollypop/-/raw/master/data/org.gnome.Lollypop.appdata.xml.in"
reported_by = "cahfofpai"
updated_by = "script"
+++


