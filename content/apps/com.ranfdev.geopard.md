+++
title = "Geopard"
description = "Gemini browser in gtk4"
aliases = []
date = 2022-05-10
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Lorenzo Miglietta",]
categories = [ "gemini browser",]
mobile_compatibility = [ "5",]
status = [ "maturing",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_3_18", "alpine_edge", "aur", "flathub",]
freedesktop_categories = [ "GTK", "GNOME", "Network",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/ranfdev/Geopard"
homepage = "https://ranfdev.com/projects/geopard/"
bugtracker = "https://github.com/ranfdev/Geopard/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/ranfdev/Geopard"
screenshots = []
screenshots_img = [ "https://img.linuxphoneapps.org/com.ranfdev.geopard/1.png", "https://img.linuxphoneapps.org/com.ranfdev.geopard/2.png", "https://img.linuxphoneapps.org/com.ranfdev.geopard/3.png", "https://img.linuxphoneapps.org/com.ranfdev.geopard/4.png",]
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.ranfdev.Geopard"
scale_to_fit = ""
flathub = "https://flathub.org/apps/com.ranfdev.Geopard"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "geopard",]
appstream_xml_url = "https://raw.githubusercontent.com/ranfdev/Geopard/master/data/com.ranfdev.Geopard.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Geopard is a gemini client written in rust, using the gtk4 toolkit [Source](https://ranfdev.com/projects/geopard/)
