+++
title = "KWeather"
description = "A convergent weather application for Plasma."
aliases = []
date = 2020-08-25
updated = 2023-02-16

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "KDE Community",]
categories = [ "weather",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "Kirigami",]
backends = [ "api.met.no", "geonames.org", "geoip.ubuntu.com", "openweathermap.org",]
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "Qt", "KDE", "Utility",]
programming_languages = [ "QML", "Cpp",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/utilities/kweather"
homepage = ""
bugtracker = "https://bugs.kde.org/describecomponents.cgi?product=kweather"
donations = ""
translations = ""
more_information = [ "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/#weather",]
summary_source_url = "https://invent.kde.org/utilities/kweather"
screenshots = [ "https://cdn.kde.org/screenshots/kweather/kweather-mobile-dynamic2.png", "https://cdn.kde.org/screenshots/kweather/kweather-mobile-flat.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.kweather"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.kde.kweather"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "kweather",]
appstream_xml_url = "https://invent.kde.org/utilities/kweather/-/raw/master/org.kde.kweather.appdata.xml"
reported_by = "linmob"
updated_by = "linmob"
+++




### Description

A convergent weather application for Plasma. Has flat and dynamic/animated views for showing forecasts and other information. [Source](https://invent.kde.org/utilities/kweather/-/raw/master/org.kde.kweather.appdata.xml)
