+++
title = "Kamoso"
description = "Kamoso is a simple and friendly program to use your camera. Use it to take pictures and make videos to share."
aliases = []
date = 2020-02-06
updated = 2022-12-23

[taxonomies]
project_licenses = [ "GPL-2.0-or-later",]
metadata_licenses = []
app_author = [ "KDE Community",]
categories = [ "camera",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "Kirigami",]
backends = [ "gstreamer",]
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "Qt", "KDE", "Graphics", "Photography",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/multimedia/kamoso"
homepage = "https://apps.kde.org/multimedia/org.kde.kamoso/"
bugtracker = "https://invent.kde.org/multimedia/kamoso/-/issues/"
donations = ""
translations = ""
more_information = [ "https://userbase.kde.org/Kamoso",]
summary_source_url = "https://apps.kde.org/multimedia/org.kde.kamoso/"
screenshots = [ "https://apps.kde.org/multimedia/org.kde.kamoso/",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.kamoso"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "kamoso",]
appstream_xml_url = "https://invent.kde.org/multimedia/kamoso/-/raw/master/org.kde.kamoso.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++


