+++
title = "Yishu"
description = "[DEPRECATED] A bespoke and simple Todo.txt client."
aliases = []
date = 2020-12-05
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "Lains",]
categories = [ "productivity",]
mobile_compatibility = [ "3",]
status = [ "archived",]
frameworks = [ "GTK3", "libhandy", "granite",]
backends = [ "todo.txt",]
services = []
packaged_in = [ "aur", "opensuse_tumbleweed",]
freedesktop_categories = [ "GTK", "Utility",]
programming_languages = [ "Vala",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/lainsce/yishu"
homepage = ""
bugtracker = "https://github.com/lainsce/yishu/issues/"
donations = ""
translations = ""
more_information = []
summary_source_url = "https://github.com/lainsce/yishu"
screenshots = [ "https://cdn.fosstodon.org/media_attachments/files/105/323/999/114/351/249/original/43140ce0bbf61454.png", "https://cdn.fosstodon.org/media_attachments/files/105/323/999/277/208/479/original/6a482df569e27111.png", "https://cdn.fosstodon.org/media_attachments/files/105/323/999/411/124/095/original/dd66b67be120654b.png",]
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.lainsce.yishu"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "yishu",]
appstream_xml_url = "https://raw.githubusercontent.com/lainsce/yishu/master/data/com.github.lainsce.yishu.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++





### Notice

Works fine after scale-to-fit, repo has been archived.