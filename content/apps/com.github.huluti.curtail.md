+++
title = "Curtail"
description = "Simple & useful image compressor."
aliases = []
date = 2021-05-24
updated = 2023-04-13

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Hugo Posnic",]
categories = [ "utilities",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "flathub", "gentoo", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed",]
freedesktop_categories = [ "GTK", "GNOME", "Utility", "Compression",]
programming_languages = [ "Python",]
build_systems = [ "meson",]

[extra]
repository = "https://github.com/Huluti/Curtail"
homepage = ""
bugtracker = "https://github.com/Huluti/Curtail/issues/"
donations = ""
translations = ""
more_information = [ "https://apps.gnome.org/app/com.github.huluti.Curtail/",]
summary_source_url = "https://github.com/Huluti/Curtail"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "com.github.huluti.Curtail"
scale_to_fit = "curtail"
flathub = "https://flathub.org/apps/com.github.huluti.Curtail"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "curtail",]
appstream_xml_url = "https://raw.githubusercontent.com/Huluti/Curtail/master/data/com.github.huluti.Curtail.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++


 


