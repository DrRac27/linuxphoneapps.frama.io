+++
title = "Polari"
description = "Polari is a simple IRC Client that is designed to integrate seamlessly with GNOME"
aliases = []
date = 2022-03-24
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-2.0-or-later", "LGPL-2.1-or-later",]
metadata_licenses = [ "CC0-1.0",]
app_author = [ "GNOME Developers",]
categories = [ "chat",]
mobile_compatibility = [ "5",]
status = [ "mature",]
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = [ "IRC",]
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "archlinuxarm_aarch64", "archlinuxarm_armv7h", "aur", "debian_11", "debian_12", "debian_unstable", "devuan_4_0", "fedora_38", "fedora_rawhide", "flathub", "gentoo", "gnuguix", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable", "opensuse_tumbleweed", "pureos_landing",]
freedesktop_categories = [ "GTK", "GNOME", "Network", "IRCClient",]
programming_languages = [ "JavaScript", "C",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/GNOME/polari"
homepage = "https://wiki.gnome.org/Apps/Polari"
bugtracker = "https://gitlab.gnome.org/GNOME/polari/-/issues/"
donations = "http://www.gnome.org/friends/"
translations = "https://wiki.gnome.org/TranslationProject"
more_information = [ "https://apps.gnome.org/app/org.gnome.Polari/",]
summary_source_url = "https://gitlab.gnome.org/GNOME/polari"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.Polari"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.Polari"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "polari",]
appstream_xml_url = "https://gitlab.gnome.org/GNOME/polari/-/raw/main/data/appdata/org.gnome.Polari.appdata.xml.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

A simple Internet Relay Chat (IRC) client that is designed to integrate seamlessly with GNOME, it features a simple and beautiful interface which allows you to focus on your conversations. [Source](https://apps.gnome.org/app/org.gnome.Polari/)


### Notice

Works on mobile since release 42. It's still a bit imperfect, e.g. changing nicknames after tapping the nickname does not work.
