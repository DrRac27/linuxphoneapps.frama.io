+++
title = "Dialer"
description = "Dialer for Plasma Mobile."
aliases = []
date = 2019-09-30
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-only",]
metadata_licenses = []
app_author = [ "Plasma Mobile Developers",]
categories = [ "telephony",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "QtQuick", "Kirigami",]
backends = [ "ofono",]
services = []
packaged_in = [ "alpine_3_17", "alpine_3_18", "alpine_edge", "aur", "debian_12", "debian_unstable", "fedora_38", "fedora_rawhide", "manjaro_stable", "manjaro_unstable", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "Qt", "KDE", "Utility", "TelephonyTools",]
programming_languages = [ "Cpp", "QML",]
build_systems = [ "cmake",]

[extra]
repository = "https://invent.kde.org/plasma-mobile/plasma-dialer"
homepage = ""
bugtracker = "https://invent.kde.org/plasma-mobile/plasma-dialer/-/issues/"
donations = ""
translations = ""
more_information = [ "https://phabricator.kde.org/T6935", "https://plasma-mobile.org/2022/11/30/plasma-mobile-gear-22-11/#dialer",]
summary_source_url = "https://invent.kde.org/plasma-mobile/plasma-dialer"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.kde.phone.dialer"
scale_to_fit = ""
flathub = ""
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "plasma-dialer",]
appstream_xml_url = "https://invent.kde.org/plasma-mobile/plasma-dialer/-/raw/master/plasma-dialer/org.kde.phone.dialer.appdata.xml"
reported_by = "cahfofpai"
updated_by = "script"
+++


