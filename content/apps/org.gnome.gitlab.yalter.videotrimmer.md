+++
title = "Video Trimmer"
description = "Trim videos quickly"
aliases = []
date = 2020-10-21
updated = 2022-12-19

[taxonomies]
project_licenses = [ "GPL-3.0-or-later",]
metadata_licenses = []
app_author = [ "Ivan Molodetskikh",]
categories = [ "video editing",]
mobile_compatibility = [ "5",]
status = []
frameworks = [ "GTK4", "libadwaita",]
backends = []
services = []
packaged_in = [ "alpine_edge", "aur", "flathub", "nix_stable_22_11", "nix_unstable",]
freedesktop_categories = [ "GTK", "GNOME", "Video", "AudioVideoEditing",]
programming_languages = [ "Rust",]
build_systems = [ "meson",]

[extra]
repository = "https://gitlab.gnome.org/YaLTeR/video-trimmer"
homepage = ""
bugtracker = "https://gitlab.gnome.org/YaLTeR/video-trimmer/-/issues/"
donations = ""
translations = ""
more_information = [ "https://apps.gnome.org/app/org.gnome.gitlab.YaLTeR.VideoTrimmer/",]
summary_source_url = "https://gitlab.gnome.org/YaLTeR/video-trimmer"
screenshots = []
screenshots_img = []
all_features_touch = ""
intended_for_mobile = ""
app_id = "org.gnome.gitlab.YaLTeR.VideoTrimmer"
scale_to_fit = ""
flathub = "https://flathub.org/apps/org.gnome.gitlab.YaLTeR.VideoTrimmer"
flatpak_link = ""
flatpak_recipe = ""
snapcraft = ""
snap_link = ""
snap_recipe = ""
repology = [ "video-trimmer",]
appstream_xml_url = "https://gitlab.gnome.org/YaLTeR/video-trimmer/-/raw/master/data/org.gnome.gitlab.YaLTeR.VideoTrimmer.metainfo.xml.in.in"
reported_by = "linmob"
updated_by = "script"
+++




### Description

Video Trimmer cuts out a fragment of a video given the start and end timestamps. The video is never re-encoded, so the process is very fast and does not reduce the video quality. [Source](https://gitlab.gnome.org/YaLTeR/video-trimmer)
