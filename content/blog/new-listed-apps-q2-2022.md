+++
title = "New apps of LinuxPhoneApps.org, Q2/2022 🎉"
date = 2022-07-07T22:10:00+02:00
template = "blog/page.html"
draft = false

[taxonomies]
authors = ["linmob"]

+++

Let's have a quick and dirty blog post listing all the additions to our app list in the second quarter of 2022!
<!-- more -->

### April

* [Moment](https://linuxphoneapps.org/apps/xyz.mx_moment.moment/),a ustomizable and keyboard-operable Matrix client, written in Qt/QML and Python. _This is the continuation of [Mirage](https://linuxphoneapps.org/apps/io.github.mirukana.mirage/), which has been inactive for a bit._
* [GNOME Firmware](https://linuxphoneapps.org/apps/org.gnome.firmware/), GNOME's mobile friendly fwupd frontend.
* [Iotas](https://linuxphoneapps.org/apps/org.gnome.gitlab.cheywood.iotas/), a simple note taking with mobile-first design and a focus on Nextcloud Notes sync.
* [gtkcord4](https://linuxphoneapps.org/apps/com.github.diamondburned.gtkcord4/), a GTK4 Discord client in Go.
* [Delta Chat Desktop](https://linuxphoneapps.org/apps/chat.delta.desktop/), an Email-based instant messaging client for Desktop. _And phones, duh!_
* [Quick Weather](https://linuxphoneapps.org/apps/noappid.bit-shift-io.qweather/), a QML Qt application for desktop and mobile, that uses BOM weather for Australia.
* [Roger Router](https://linuxphoneapps.org/apps/org.tabos.roger/), a utility to control and monitor AVM Fritz!Box Routers.
* [Clans](https://linuxphoneapps.org/apps/io.gitlab.caveman250.clans/), a WIP Gtk4 / libAdwaita Discord client. _Warning: Use at your own risk. Using a third party Discord client may result in your account being banned._
* [Signal Desktop](https://linuxphoneapps.org/apps/org.signal.signal/), the official Signal app for desktops, works pretty well - except for suffering from the common Electron issues: Blurriness, or non-working virtual keyboards (partially on Phosh, totally on Plasma Mobile) when enabling the ozone wayland backend. _Sadly, Signal does not provide official ARM64/aarch64 binaries, so that unofficial builds must do: Builds for Debian are listed on the [Mobian Wiki](https://wiki.mobian.org/doku.php?id=signaldesktop), builds for Arch are available on [privacy-shark](https://privacyshark.zero-credibility.net/#howto) and there's also a distribution-independent [flatpak build](https://elagost.com/flatpak/) that should work on every distro._ 
* [Amberol](https://linuxphoneapps.org/apps/io.bassi.amberol/), a small and simple sound and music player that is well integrated with GNOME.
* [Jami-qt](https://linuxphoneapps.org/apps/net.jami.jami/) is the cross platform client for Jami, a privacy-oriented voice, video, chat, and conference platform. 
* [Coasts](https://linuxphoneapps.org/apps/org.kde.coasts/), a nice and simple map viewer.
* [Tasks](https://linuxphoneapps.org/apps/org.kde.tasks/), a simple task list application.

### May

* [Geopard](https://linuxphoneapps.org/apps/com.ranfdev.geopard/), a Gemini client written in Rust, using the GTK4 toolkit
* [Warp](https://linuxphoneapps.org/apps/app.drey.warp/) allows you to securely send files to each other via the internet or local network by exchanging a word-based code.
* [Smile](https://linuxphoneapps.org/apps/it.mijorus.smile/), a simple emoji picker for Linux with custom tags support.
* [Paper](https://linuxphoneapps.org/apps/io.posidon.paper/), a pretty note-taking app for GNOME.
* [Shopping List](https://linuxphoneapps.org/apps/ro.hume.cosmin.shoppinglist/), a shopping list application for GNU/Linux mobile devices.

### June

* [Songrec](https://linuxphoneapps.org/apps/com.github.marinm.songrec/), an open-source, unofficial Shazam client for Linux, written in Rust.
* [G4Music](https://linuxphoneapps.org/apps/com.github.neithern.g4music/), a fast, fluent, light weight music player written in GTK4, with a beautiful, adaptive user interface, so named G4Music.
* [Valent](https://linuxphoneapps.org/apps/ca.andyholmes.valent/), securely connect your devices to open files and links where you need them, get notifications when you need them, stay in control of your media and more. _Valent is an implementation of the KDE Connect protocol, built on GNOME platform libraries._
* [Iridium](https://linuxphoneapps.org/apps/com.github.avojak.iridium/), a native Linux IRC client built in Vala and Gtk for elementary OS.
* [Phosh Mobile Settings](https://linuxphoneapps.org/apps/org.sigxcpu.mobilesettings/), a settings app for Phosh and related components. _Really neat tool that tackles everything I always wanted to add to [postmarketOS Tweaks](https://linuxphoneapps.org/apps/org.postmarketos.tweaks/)._
* [Goguma](https://linuxphoneapps.org/apps/noappid.emersion.goguma/), an IRC client written in Flutter for mobile devices.
* [BlackBox](https://linuxphoneapps.org/apps/com.raggesilver.blackbox/), a beautiful GTK4 terminal.

Thanks to everyone involved in developing and submitting these apps!👏

### Other noteworthy changes

There also have been a some nice updates to already included listings - shout out to BigB, Maeve and alpabrz for their contributions!

Overall, I would have hoped to get more done in the past three months. My day job has been challenging at times, so maybe more progress was just not possible. That said, especially progress on site features like [app authorship](https://framagit.org/linuxphoneapps/linuxphoneapps.frama.io/-/issues/29)[^1] or others that you can find in the [issue tracker](https://framagit.org/groups/linuxphoneapps/-/issues) (or in my head) has been way to slow for my liking.

So if you think you can help out, please do get in touch!


### Feedback, thoughts?

Thank you for reading this post! If you enjoyed it, please spread the word about this post and LinuxPhoneApps.org!

If you want to contribute an/your app or work on the website itself, please check the [FAQ](@/docs/help/faq.md#join-the-effort)![^2] 

And if you want to discuss apps with like minded persons, make sure to join [our Matrix group](https://matrix.to/#/#linuxphoneapps:matrix.org)!


[^1]: This is the [current state](https://linuxphoneapps.org/app-author/), in case you were curious.

[^2]: If you are not aware of any new apps, check out [apps-to-be-added](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/apps-to-be-added.md), test the app and add it!
