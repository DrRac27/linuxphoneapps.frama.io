+++
title = "Help us achieve self-maintaining App Listings by adding AppStream Metadata!"
date = 2022-12-24T16:45:00+01:00
template = "blog/page.html"
draft = false 

[taxonomies]
authors = ["linmob"]

+++

With the festive season upon us, and the end of 2022 approaching, we have a big ask: Help us keep app listings up to date by working with upstream projects!

<!-- more -->

In the past months, not many apps were evaluated or added to LinuxPhoneApps.org. But still, things were done: The technical stack was much improved. While most of this work is not directly user-facing, as it's about restructuring the underlying .csv files and adding more and more automatic content checkers, we've now arrived at a stage where we can update listings without a lot of the time-intense churn this was.

As the main contributor of these efforts[^1] put it in the Matrix group:

> __Help wanted: upstreaming metadata__
>
> The last weeks, I created a script which allows us to automatically update outdated information in the apps.csv file based on AppStream metadata: [https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/check_via_appstream.py](https://framagit.org/linmobapps/linmobapps.frama.io/-/blob/master/check_via_appstream.py).
>
> This massively decreases manual maintenance overhead and allows us to instead focus on adding additional value like testing the apps, storing mobile compatibility scores and so on.
>
> In order for that workflow to work out, we need your help:
>
> - _Make sure each repository has an [AppStream metadata file](https://www.freedesktop.org/software/appstream/docs/chap-Metadata.html)._ [This job](https://framagit.org/darkdragon-001/linmobapps.frama.io/-/jobs/1819530) will list you the missing ones.
> -  _Add missing values:_ [This job](https://framagit.org/darkdragon-001/linmobapps.frama.io/-/jobs/1819529) will show you where we have more information that reported upstream -> just earch for "missing in upstream AppStream file. Consider contributing it upstream"
>
> See [this issue](https://framagit.org/linmobapps/linmobapps.frama.io/-/issues/27) for further info. You can also comment links to upstream issues and merge requests there.
>

### Want to help? Great! Please be polite and helpful to upstream!

__As always: Please don't bomb upstream here, be polite, don't just go around and create a ton of issues on upstrean projects!__ 

Try to be helpful instead: If you've ever dealt with HTML or other XML things, you'll be able to understand AppStream files well enough without much effort to create a Merge Request (MR) or Pull Request (PR), to take the initial load of this task from the individual developer. Be kind in the description of your PR or MR, after all it's a request you are making and politeness does not cost you money.

In order to reduce duplicate effort, make sure to briefly mention your MR or issue to [the issue that tracks progress](https://framagit.org/linmobapps/linmobapps.frama.io/-/issues/27). If you don't want to sign up with Framagit for that purpose, sending an email to upstream-appstream@linuxphoneapps.org will do the same once I (linmob) get to it.

### More on AppStream Metadata

If this is the first time you've heard of AppStream Metadata, you may wonder: Ok, what is this good for? Is all this XML not a little much just to support this dumb LinuxPhoneApps.org website? Yes, it would be, you're not mistaken. But this is a well established specification, it makes listings in GNOME Software, KDE's Discover and other 'app store'-ish apps.look have nice content. 

Still not convinced? Well, it also provides the data for websites like

- [Flathub](https://flathub.org/),
- [Apps for GNOME](https://apps.gnome.org/),
- [KDE Applications](https://apps.kde.org/).

### If you don't like XML, here are other ways to help

Pretty cool, huh? If you still don't think so, and don't want to help us this way, you can help in other ways:

- [Submitting apps by email](https://linuxphoneapps.org/docs/contributing/submit-app-by-email/),
- [Submit apps on GitLab](https://linuxphoneapps.org/docs/contributing/submit-app-on-gitlab/),
- or just by writing a nice 'Thank you' mail/toot/tweet to the developers of your favorite apps!

And if you don't want to help at all, that's fine too. ;-) And even if we are a day late: Happy [Festivus](https://en.wikipedia.org/wiki/Festivus)!


[^1]: Who doea not even want to be named - still: Thank you so much for your work on LinuxPhoneApps, and thanks for putting up with me (linmob) being difficult!
