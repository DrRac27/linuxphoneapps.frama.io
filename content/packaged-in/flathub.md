+++
title = "Flathub"
date = 2021-08-15T08:50:45+00:00
draft = false
+++

[Flathub](https://flathub.org) is an universal app store for desktop Linux and also Linux Phones. To enable Flathub on your distribution, see [flatpak.org](https://flatpak.org/). 
