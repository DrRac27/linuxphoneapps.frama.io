#!/usr/bin/env python3

from dataclasses import dataclass, field
from dataclass_csv import dateformat
from datetime import date


@dataclass()
@dateformat('%Y-%m-%d')
class App:
    title: str = ""
    description: str = ""
    description_long: str = ""
    notice: str = ""
    taxonomies_project_licenses: list[str] = field(default_factory=list)
    taxonomies_metadata_licenses: list[str] = field(default_factory=list)
    taxonomies_app_author: list[str] = field(default_factory=list)
    taxonomies_categories: list[str] = field(default_factory=list)
    taxonomies_status: list[str] = field(default_factory=list)
    taxonomies_mobile_compatibility: list[str] = field(default_factory=list)
    taxonomies_frameworks: list[str] = field(default_factory=list)
    taxonomies_backends: list[str] = field(default_factory=list)
    taxonomies_services: list[str] = field(default_factory=list)
    taxonomies_packaged_in: list[str] = field(default_factory=list)
    taxonomies_freedesktop_categories: list[str] = field(default_factory=list)
    taxonomies_programming_languages: list[str] = field(default_factory=list)
    taxonomies_build_systems: list[str] = field(default_factory=list)
    extra_repository: str = ""
    extra_homepage: str = ""
    extra_bugtracker: str = ""
    extra_donations: str = ""
    extra_translations: str = ""
    extra_more_information: list[str] = field(default_factory=list)
    extra_summary_source_url: str = ""
    extra_screenshots: list[str] = field(default_factory=list)
    extra_app_id: str = ""
    extra_scale_to_fit: str = ""
    extra_appstream_xml_url: str = ""
    extra_repology: list[str] = field(default_factory=list)
    extra_screenshots_img: list[str] = field(default_factory=list)
    extra_reported_by: str = ""
    extra_updated_by: str = ""
    date: date = None
    updated: date = None

    def validate(self) -> bool:
        allowed = {
            'taxonomies_categories': [
                'accessibility',
                'alarm clock',
                'app store',
                'audio recorder',
                'audio streaming',
                'audiobook player',
                'backup',
                'banking',
                'bible',
                'bibliography editor',
                'bitcoin wallet',
                'bittorrent client',
                'calculator',
                'calendar',
                'camera',
                'camera utility',
                'chat',
                'clock',
                'cloud syncing',
                'compass',
                'conference companion',
                'connectivity',
                'contacts',
                'demo',
                'development',
                'diary',
                'dictionary',
                'document and ebook manager',
                'document viewer',
                'drawing',
                'drone control',
                'education',
                'educational game',
                'email',
                'emoji picker',
                'entertainment',
                'feed reader',
                'file management',
                'file transfer',
                'fitness',
                'flight navigation',
                'font downloader',
                'food rescue',
                'game',
                'game engine',
                'game launcher',
                'gemini browser',
                'geography',
                'geology',
                'gopher browser',
                'health',
                'image and video comparison',
                'image viewer',
                'internet radio',
                'key management',
                'loyalty cards',
                'maps and navigation',
                'maps server',
                'media',
                'mobile service utility',
                'multi-factor authentication',
                'multimedia',
                'music player',
                'musical tool',
                'network',
                'news',
                'note taking',
                'office',
                'parental controls',
                'password generator',
                'password manager',
                'pdf viewer',
                'photo management',
                'podcast client',
                'privacy',
                'productivity',
                'project management tool',
                'public transport',
                'qr code',
                'recipe management',
                'settings',
                'smart home',
                'SMS',
                'social media',
                'software center',
                'sports',
                'stocks',
                'system utilities',
                'task management',
                'telephony',
                'Teleprompter software',
                'terminal emulator',
                'text editor',
                'timer',
                'translation tool',
                'travel',
                'tv guide',
                'utilities',
                'video downloader',
                'video editing',
                'video player',
                'virtual assistant',
                'virtual input device',
                'virtual keyboard',
                'visual voicemail',
                'voice assistant',
                'voice chat',
                'watch companion',
                'weather',
                'web app laucher',
                'web browser',
                'writing'
            ],
            'taxonomies_status': [
                'archived',
                'early',
                'gone',
                'inactive',
                'mature',
                'maturing',
                'unmaintained'
            ],
            'taxonomies_mobile_compatibility': [
                '1',
                '2',
                '3',
                '4',
                '5',
                'needs testing'
            ],
            'taxonomies_frameworks': [
                'Cairo',
                'Clutter',
                'Electron',
                'fluid',
                'Flutter',
                'granite',
                'GTK3',
                'GTK4',
                'Kirigami',
                'iced',
                'libadwaita',
                'libhandy',
                'MauiKit',
                'ReactXP',
                'SDL',
                'Silica',
                'Tk',
                'QtQuick',
                'QtQuick 6',
                'QtWidgets',
                'UbuntuComponents',
                'WxWidgets',
                'xapps'
            ],
            'taxonomies_packaged_in': [
                'AUR',
                'Debian',
                'Flathub',
                'postmarketOS'
            ],
            'taxonomies_programming_languages': [
                'C',
                'C++',
                'C++. QML',  # TODO: fix
                'Clojure',
                'Crystal',
                'Dart',
                'go',  # TODO: fix
                'Go',
                'HTML',
                'Java',
                'JS',  # TODO: fix
                'Javascript',  # TODO: fix
                'JavaScript',
                'Objective-C',
                'Python',
                'QML',
                'Rust',
                'Shell',
                'Tcl',
                'TypeScript',
                'Vala',
                'Vue',
                'XSLT',
                'Zig'
            ],
            'taxonomies_build_systems': [
                'cargo',
                'clickable',
                'cmake',  # TODO: fix
                'CMake',
                'custom',
                'flit',
                'flutter',
                'go',
                'make',
                'meson',
                'ninja',
                'nix',
                'none',
                'npm',
                'qbs',
                'qmake',
                'setup.py',  # TODO: ?
                'various',
                'yarn',
                'zig'
            ]
        }

        print("validating: '" + self.title + "'")
        for v in self.taxonomies_categories:
            if v not in allowed['taxonomies_categories']:
                print("'" + v + "' is not allowed for taxonomies_categories")
                return False
        for v in self.taxonomies_status:
            if v not in allowed['taxonomies_status']:
                print("'" + v + "' is not allowed for taxonomies_status")
                return False
        for v in self.taxonomies_mobile_compatibility:
            if v not in allowed['taxonomies_mobile_compatibility']:
                print("'" + v + "' is not allowed for taxonomies_mobile_compatibility")
                return False
        for v in self.taxonomies_frameworks:
            if v not in allowed['taxonomies_frameworks']:
                print("'" + v + "' is not allowed for taxonomies_frameworks")
                return False
        for v in self.taxonomies_packaged_in:
            if v not in allowed['taxonomies_packaged_in']:
                print("'" + v + "' is not allowed for taxonomies_packaged_in")
                return False
        for v in self.taxonomies_programming_languages:
            if v not in allowed['taxonomies_programming_languages']:
                print("'" + v + "' is not allowed for taxonomies_programming_languages")
                return False
        for v in self.taxonomies_build_systems:
            if v not in allowed['taxonomies_build_systems']:
                print("'" + v + "' is not allowed for taxonomies_build_systems")
                return False
        return True
